<?php get_header(); ?>

	<section class="page-header usau-block">
		<div class="headline blue align-center underline">
			<h2>Play Local</h2>
		</div>

		<div class="copy p2">
			<p>Find local organizations and opportunities in your state to learn, play, and compete.</p>
		</div>			
	</section>

	<section class="map usau-block">
		<?php get_template_part('template-parts/local/map'); ?>
	</section>

	<?php get_template_part('template-parts/local/states-list'); ?>

<?php get_footer(); ?>