<?php 

$sections = get_field('sections', 959);


get_header(); ?>


	<?php  $s = 0; foreach($sections as $section): ?>
		<?php
			$columns = $section['columns'];
			$sheet = urlencode($section['title']);
		?>

		<section class="usau-block" style="padding-top: 100px;">
			<table class="contact-table">
				<thead>
					<tr>
						<?php foreach($columns as $column): ?>
							
							<th class="<?php echo $column['slug']; ?>"><?php echo $column['name']; ?></th>

						<?php endforeach; ?>										
					</tr>
				</thead>


				<tbody>
					<?php foreach ($values as $value): ?>

						<?php
							$trClassName = "contacts-row";                        
							if($entry['gsx$name']['$t'] == 'Vacant') {
								$trClassName .= ' vacant';
							}
						?>

						<tr class="<?php echo $trClassName; ?>">
							<?php $i = 0; foreach($columns as $column): ?>
								<?php
									$slug = $column['slug'];
									$datum = $value[$i];
									$tdClassName = $slug;          
								?>
								
								<?php if($slug == "email"): ?>
									<td class="<?php echo $tdClassName; ?>"><a href="mailto:<?php echo $datum; ?>"><?php echo $datum; ?></a></td>
								<?php else: ?>
									<td class="<?php echo $tdClassName; ?>"><?php echo $datum; ?></td>
								<?php endif; ?>
								
							<?php $i++; endforeach ?>													
						</tr>					

					<?php endforeach; ?>

				</tbody>


			</table>

		</section>

	<?php $s++; endforeach; ?>

<?php get_footer(); ?>