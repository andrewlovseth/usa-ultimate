<?php

	get_header();

?>

	<section class="page-header usau-block">
		<h1>Video Library</h1>
	</section>

	<section class="video-archive usau-block">

		<div class="filters-container">
			<h2 class="filters-header">Filters</h2>

			<div class="filters-wrapper">
				<?php echo do_shortcode('[ajax_load_more_filters id="division_filter" target="video_archive"]'); ?>

				<div class="filters-clear">
					<a href="#" class="clear-btn">Clear</a>
				</div>
			</div>
		</div>

		<?php echo do_shortcode('[ajax_load_more id="video_archive" target="division_filter" filters="true" theme_repeater="video.php" post_type="videos" posts_per_page="16" scroll="false" button_label="Load More" no_results_text="<aside>Sorry, nothing found.</aside>"]'); ?>

	</section>

<?php get_footer(); ?>