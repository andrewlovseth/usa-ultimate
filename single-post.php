<?php get_header(); ?>

	<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

		<article class="post news-post">

			<?php get_template_part('template-parts/news/featured-image'); ?>

			<section class="article-header">
				<div class="headline">
					<h1><?php the_title(); ?></h1>
				</div>

				<aside class="meta desktop">
					<?php get_template_part('template-parts/news/date'); ?>

					<?php get_template_part('template-parts/news/authors'); ?>

					<?php
						$args = ['color' => 'red'];
						get_template_part('template-parts/news/categories-list', null, $args);
					?>

					<?php get_template_part('template-parts/news/social-share'); ?>
				</aside>

			</section>

			<section class="article-body copy p2">
				<?php the_content(); ?>				
			</section>

			<aside class="meta mobile">
				<?php get_template_part('template-parts/news/date'); ?>

				<?php get_template_part('template-parts/news/authors'); ?>

				<?php get_template_part('template-parts/news/categories-list'); ?>

				<?php get_template_part('template-parts/news/social-share'); ?>
			</aside>

			<section class="article-footer">
				<div class="footer-container">

					<?php get_template_part('template-parts/news/related-posts'); ?>

				</div>
			</section>
			
		</article>

	<?php endwhile; endif; ?>

<?php get_footer(); ?>