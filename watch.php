<?php

/*
	Template Name: Watch

*/

get_header(); ?>

	<?php get_template_part('template-parts/watch/banner'); ?>

	<?php get_template_part('template-parts/watch/hero'); ?>

	<?php get_template_part('template-parts/watch/playlists'); ?>

	<?php get_template_part('template-parts/watch/library-cta'); ?>

<?php get_footer(); ?>