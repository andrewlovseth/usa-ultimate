<?php get_header(); ?>

	<?php get_template_part('template-parts/local/page-header'); ?>

	<?php get_template_part('template-parts/local/state-map'); ?>

	<?php get_template_part('template-parts/local/locations'); ?>

	<?php get_template_part('template-parts/local/states-list'); ?>

<?php get_footer(); ?>