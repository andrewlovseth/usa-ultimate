<section class="hero">
	<div class="video">
		<div class="content">
			<iframe src="https://player.vimeo.com/video/<?php echo get_field('hero_video_id'); ?>?muted=1&autoplay=1&loop=1&controls=0&byline=0&playsinline=1&quality=1080p" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>		
		</div>
	</div>

	<div class="info usau-block">
		<div class="headline">
			<h1><?php echo get_field('hero_headline'); ?></h1>
		</div>
	</div>
</section>