<?php
	$video = get_field('this_is_ultimate_video');
	$thumbnail = wp_get_attachment_url($video['thumbnail']);
	$background = get_field('this_is_ultimate_background');
?>


<section class="this-is-ultimate usau-block" style="background-image: url(<?php echo $background['url']; ?>);">
	<div class="video">
		<a href="#" data-video-id="<?php echo $video['youtube_id']; ?>" class="video-trigger discover-play-btn">
			<span class="icon"></span>
			<span class="duration"><?php echo $video['time']; ?></span>
			<span class="thumbnail">
				<img src="<?php echo $thumbnail; ?>" alt="This is Ultimate.">

			</span>		
		</a>
	</div>

	<div class="info section-header">
		<div class="headline">
			<h5><?php echo get_field('this_is_ultimate_sub_headline'); ?></h5>
			<h2><?php echo get_field('this_is_ultimate_headline'); ?></h2>
		</div>

		<div class="copy p1">
			<?php echo get_field('this_is_ultimate_deck'); ?>
		</div>

		<div class="cta">
			<?php 
			$link = get_field('this_is_ultimate_cta');
			if( $link ): 
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self';
				?>
				<a class="btn white" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
			<?php endif; ?>

		</div>
	</div>
</section>
