<?php

	$background = get_field('how_to_play_background');

?>

<section class="how-to-play usau-block" style="background-image: url(<?php echo $background['url']; ?>);">		
	<div class="section-header">
		<div class="headline">
			<h5><?php echo get_field('how_to_play_sub_headline'); ?></h5>
			<h2><?php echo get_field('how_to_play_headline'); ?></h2>					
		</div>
	</div>

	<div class="tiles">
		<?php if(have_rows('how_to_play_tiles')): while(have_rows('how_to_play_tiles')): the_row(); ?>
			<div class="tile">
				<div class="content">
					<img src="<?php $image = get_sub_field('graphic'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>
			</div>
		<?php endwhile; endif; ?>			
	</div>

	<?php 
		$link = get_field('how_to_play_cta');
		if( $link ): 
		$link_url = $link['url'];
		$link_title = $link['title'];
		$link_target = $link['target'] ? $link['target'] : '_self';
	?>
		<div class="cta align-center">
			<a class="btn white" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
		</div>
	<?php endif; ?>
</section>