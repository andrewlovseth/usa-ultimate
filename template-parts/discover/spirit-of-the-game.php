<?php

	$background = get_field('spirit_of_the_game_background');
?>

<section class="spirit-of-the-game usau-block" style="background-image: url(<?php echo $background['url']; ?>);">	
	<div class="section-header">
		<div class="headline">
			<h5><?php echo get_field('spirit_of_the_game_sub_headline'); ?></h5>
			<h2><?php echo get_field('spirit_of_the_game_headline'); ?></h2>					
		</div>
	</div>

	<div class="info">
		<div class="copy p2">
			<?php echo get_field('spirit_of_the_game_copy'); ?>
		</div>

		<?php 
			$link = get_field('spirit_of_the_game_cta');
			if( $link ): 
			$link_url = $link['url'];
			$link_title = $link['title'];
			$link_target = $link['target'] ? $link['target'] : '_self';
		?>
			<div class="cta">
				<a class="btn white small" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
			</div>
		<?php endif; ?>
	</div>
</section>