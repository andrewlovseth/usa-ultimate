<section class="history usau-block">	
	<div class="section-header">
		<div class="headline">
			<h5><?php echo get_field('history_sub_headline'); ?></h5>
			<h2><?php echo get_field('history_headline'); ?></h2>					
		</div>
	</div>

	<div class="timeline">
		<?php if(have_rows('history_timeline')): while(have_rows('history_timeline')) : the_row(); ?>

			<?php if( get_row_layout() == 'event' ): $info = get_sub_field('info'); ?>
				<div class="event">
					<div class="year">
						<span class="label"><?php echo get_sub_field('year'); ?></span>			    		
					</div>

					<div class="photo">
						<img src="<?php echo $info['photo']['url']; ?>" alt="<?php echo $image['photo']['alt']; ?>" />
					</div>

					<div class="info">
						<div class="headline">
							<h3><?php echo $info['headline']; ?></h3>
						</div>

						<div class="copy p2">
							<?php echo $info['deck']; ?>
						</div>
					</div>				       
				</div>
			<?php endif; ?>

			<?php if( get_row_layout() == 'caption' ): ?>
				<div class="event event-small">
					<div class="year">
						<span class="label"><?php echo get_sub_field('year'); ?></span>			    		
					</div>

					<div class="info caption">
						<div class="copy p2">
							<p><?php echo get_sub_field('caption'); ?></p>
						</div>
					</div>				       
				</div>
			<?php endif; ?>			
		<?php endwhile; endif; ?>
	</div>
</section>