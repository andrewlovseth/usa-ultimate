<section class="where-we-play">
	<div class="section-header">
		<div class="headline">
			<h5><?php echo get_field('where_we_play_sub_headline'); ?></h5>
			<h2><?php echo get_field('where_we_play_headline'); ?></h2>					
		</div>
	</div>

	<div class="locations">
		<?php if(have_rows('locations')): while(have_rows('locations')): the_row(); ?>
		 
		    <div class="location">
		    	<div class="photo">
		    		<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		    	</div>

		    	<div class="caption">
		    		<div class="caption-wrapper copy p3">
						<?php echo get_sub_field('caption'); ?>
		    		</div>			    		
		    	</div>			    		
		    </div>

		<?php endwhile; endif; ?>				
	</div>
</section>