<section class="why-we-play">	
	<div class="section-header">
		<div class="headline">
			<h5><?php echo get_field('why_we_play_sub_headline'); ?></h5>
			<h2><?php echo get_field('why_we_play_sub_headline'); ?></h2>						
		</div>
	</div>

	<div class="players">
		<?php if(have_rows('players')): while(have_rows('players')): the_row(); ?>
		 
		    <div class="player">
		    	<div class="content">

			    	<a href="#<?php echo sanitize_title_with_dashes(get_sub_field('name')); ?>" class="profile-trigger">
				    	<div class="photo">
				    		<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				    	</div>

				    	<div class="info">
				    		<h3><?php echo get_sub_field('name'); ?>, <em><?php echo get_sub_field('age'); ?></em></h3>
				    		<h4><?php echo get_sub_field('location'); ?></h4>
				    	</div>
			    	</a>
		    		
		    	</div>
		    </div>

		<?php endwhile; endif; ?>				
	</div>

	<?php if(have_rows('players')): while(have_rows('players')): the_row(); ?>

		<article class="profile-overlay" id="<?php echo sanitize_title_with_dashes(get_sub_field('name')); ?>">
			<div class="overlay">
				<div class="overlay-wrapper">

					<div class="info">
						<div class="close">
							<a href="#" class="profile-close-btn close-btn"></a>
						</div>

						<div class="photo">
							<img src="<?php $image = get_sub_field('profile_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						</div>

						<div class="details">
							<div class="meta">
					    		<h3><?php echo get_sub_field('name'); ?>, <em><?php echo get_sub_field('age'); ?></em></h3>
					    		<h4><?php echo get_sub_field('location'); ?></h4>
							</div>

							<div class="copy p3">
								<?php echo get_sub_field('profile_copy'); ?>
							</div>
						</div>

					</div>
					
				</div>
			</div>
		</article>

	<?php endwhile; endif; ?>	
</section>