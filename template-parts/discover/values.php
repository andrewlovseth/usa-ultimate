<section class="values usau-block">		
	<div class="section-header">
		<div class="headline">
			<h5><?php echo get_field('values_sub_headline'); ?></h5>
			<h2><?php echo get_field('values_headline'); ?></h2>					
		</div>
	</div>

	<?php if(have_rows('values')): $count = 1;  while(have_rows('values')): the_row(); ?>

		<div class="value value-<?php echo $count; ?>">
			<div class="info">
				<div class="info-wrapper">
					<div class="headline white uppercase">
						<h2><?php echo get_sub_field('title'); ?></h2>
					</div>

					<div class="copy p2">
						<?php echo get_sub_field('deck'); ?>
					</div>

					<div class="cta">
						<?php 
						$link = get_sub_field('cta');
						if( $link ): 
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
							?>
							<a class="btn white small" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
						<?php endif; ?>				    			
					</div>
				</div>
			</div>

			<div class="photo">
				<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>
		</div>

	<?php $count++; endwhile; endif; ?>
</section>