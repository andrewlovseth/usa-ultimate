<section class="glossary usau-block">		
	<div class="section-header align-center">
		<div class="headline">
			<h5><?php echo get_field('glossary_sub_headline'); ?></h5>
			<h2><?php echo get_field('glossary_headline'); ?></h2>					
        </div>
        
        <div class="note copy p3">
            <p><?php echo get_field('glossary_note'); ?></p>
        </div>
    </div>
    
    <div class="terms">
        <?php if(have_rows('glossary')): while(have_rows('glossary')): the_row(); ?>
    
            <div class="term">
                <div class="term-wrapper">
                    <div class="label headline">
                        <h5><a href="#"><?php echo get_sub_field('term'); ?></a></h5>
                    </div>

                    <div class="definition copy p3">
                        <?php echo get_sub_field('definition'); ?>
                    </div>
                </div>     
            </div>

        <?php endwhile; endif; ?>
    </div>
</section>