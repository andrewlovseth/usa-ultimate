<section class="call-to-action usau-block" >
	<div class="section-header">
		<div class="headline">
			<h5><?php echo get_field('call_to_action_sub_headline'); ?></h5>
			<h2><?php echo get_field('call_to_action_headline'); ?></h2>					
		</div>
	</div>

	<?php get_template_part('template-parts/local/discover-map'); ?>
</section>