<?php

	$background = get_field('great_plays_background');

?>

<section class="great-plays usau-block" style="background-image: url(<?php echo $background['url']; ?>);">
	<div class="section-header">
		<div class="headline">		
			<h5><?php echo get_field('great_plays_sub_headline'); ?></h5>
			<h2><?php echo get_field('great_plays_headline'); ?></h2>					
		</div>
	</div>		

	<?php if(have_rows('great_plays')): $count = 1; while(have_rows('great_plays')): the_row(); ?>
		<div class="great-play great-play-<?php echo $count; ?>">
			<div class="info">
				<div class="info-wrapper">
					<div class="sub-headline headline white">
						<?php if(get_sub_field('sub_headline')): ?>
							<h5><?php echo get_sub_field('sub_headline'); ?></h5>
						<?php endif; ?>						
					</div>

					<div class="headline white uppercase">
						<h2><?php echo get_sub_field('headline'); ?></h2>
					</div>

					<div class="copy p2">
						<?php echo get_sub_field('deck'); ?>
					</div>
				</div>
			</div>

			<div class="video">
				<div class="content">
					<a href="#" class="discover-play-btn video-trigger" data-video-id="<?php echo get_sub_field('vimeo_id'); ?>" data-video-type="vimeo">
						<span class="icon">
							<img src="<?php $image = get_sub_field('thumbnail'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />	
						</span>
					</a>					
				</div>
			</div>
		</div>
	<?php $count++; endwhile; endif; ?>
</section>