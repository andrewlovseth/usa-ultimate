<?php
	$file = get_field('file');
	$file_url = $file['url'];
	$file_data_modified = $file['modified'];

	$terms = get_the_terms($post->ID, 'resource_filters');
?>

<div class="resource">
	<?php if($terms): ?>
		<div class="cat headline red uppercase">
			<h6>
				<?php foreach($terms as $term): ?>
					<span class="topic"><?php echo $term->name; ?></span>
				<?php endforeach; ?>				
			</h6>		
		</div>
	<?php endif; ?>
	
	<div class="headline">
		<h5><a href="<?php echo $file_url; ?>" target="_window"><?php the_title(); ?></a></h5>
	</div>

	<?php if(get_field('description')): ?>
		<div class="copy p4">
			<?php echo get_field('description'); ?>
		</div>
	<?php endif; ?>

	<?php if(get_field('external_link')): ?>

		<div class="url p4">
			<a href="<?php echo get_field('link'); ?>" target="_window">Visit Site</a>
		</div>

	<?php else: ?>
		
		<div class="url p4">
			<a href="<?php echo $file_url; ?>" target="_window">View PDF</a>
		</div>

		<?php if($file_data_modified): ?>
			<div class="meta p4">
				<em>Last Modified: <?php echo $file_data_modified; ?></em>
			</div>
		<?php endif; ?>

	<?php endif; ?>
</div>