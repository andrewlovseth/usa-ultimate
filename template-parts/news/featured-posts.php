<section class="featured-posts usau-block">
    <?php
        $args = array(
            'post_type' => 'post',
            'posts_per_page' => 7
        );
        $query = new WP_Query( $args );
        if ( $query->have_posts() ) : $count = 1; while ( $query->have_posts() ) : $query->the_post(); ?>

        <?php if($count == 1): ?>

            <?php include( locate_template( 'template-parts/news/article-cover-full.php', false, false ) ); ?>

        <?php elseif($count == 2 || $count == 3): ?>

            <?php include( locate_template( 'template-parts/news/article-cover-half.php', false, false ) ); ?>

        <?php elseif($count >= 4 && $count <= 6): ?>

            <?php include( locate_template( 'template-parts/news/article-three-col.php', false, false ) ); ?>

        <?php elseif($count == 7): ?>

            <?php include( locate_template( 'template-parts/news/article-cover-full.php', false, false ) ); ?>

        <?php endif; ?>

    <?php  $count++;  endwhile; endif; wp_reset_postdata(); ?>

</section>
