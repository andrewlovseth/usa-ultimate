<?php $page_for_posts = get_option('page_for_posts'); ?>

<nav class="news-subnav usau-block">
	<div class="subnav-container">
		<div class="page-header">
			<h2><a href="<?php echo site_url('/news/'); ?>">News</a></h2>
		</div>
					
		<label class="dropdown">
			<div class="dd-button">Topics</div>

			<input type="checkbox" class="dd-input" id="test">

			<ul class="dd-menu">
				<?php $terms = get_field('news_subnav', $page_for_posts); if( $terms ): ?>
					<?php foreach( $terms as $term ): ?>
						<li><a href="<?php echo esc_url( get_term_link( $term ) ); ?>"><?php echo esc_html( $term->name ); ?></a></li>
					<?php endforeach; ?>
				<?php endif; ?>
			</ul>
		</label>
		
		<div class="cta full-archive">
			<a class="underline red" href="<?php echo site_url('/news/archive/'); ?>">Full Archive</a>
		</div>
	</div>
</nav>
