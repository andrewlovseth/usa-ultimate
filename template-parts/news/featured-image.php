<section class="featured-image">
	<div class="content">
		<?php the_post_thumbnail( 'large' ); ?>

		<?php 
			$image_id = get_post_thumbnail_id();
			$image_title = get_the_title($image_id);
			if($image_title): 
		?>

			<div class="credit">
				<em><?php echo $image_title; ?></em>
			</div>

		<?php endif; ?>
	</div>				
</section>