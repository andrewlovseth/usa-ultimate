<?php $authors = get_field('authors'); if( $authors ): ?>
	<div class="authors">
		<h4 class="sidebar-header">Authors</h4>
	
		<?php foreach( $authors as $a ): ?>
			<div class="author">
				<a href="<?php echo get_permalink( $a->ID ); ?>">
					<div class="photo">
						<img src="<?php $image = get_field('photo', $a->ID); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>
					<div class="name">
						<?php echo get_the_title( $a->ID ); ?>
					</div>
				</a>
			</div>
		<?php endforeach; ?>
	</div>
<?php endif; ?>