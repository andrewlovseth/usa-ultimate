<?php

/*
 * Programmatically find Related content from SearchWP Related
 */

// Instantiate SearchWP Related
$searchwp_related = new SearchWP_Related();

// Use the keywords as defined in the SearchWP Related meta box
$keywords = get_post_meta( get_the_ID(), $searchwp_related->meta_key, true );

$args = array(
  's'              => $keywords,
  'engine'         => 'posts',
  'posts_per_page' => 3,
);



// Retrieve Related content for the current post
$related_content = $searchwp_related->get( $args );
if ( ! empty( $related_content ) ) :?>

    <div class="related-posts">
        <div class="section-header headline blue underline">
            <h3>Related Posts</h3>
        </div>        

        <div class="grid three-col">
            <?php $i = 1; foreach ( $related_content as $post ) : setup_postdata( $post ); ?>

                <article class="related news col article-<?php echo $i; ?>">
                    <div class="photo">
                        <a href="<?php the_permalink(); ?>">
                            <div class="content">
                                <img src="<?php the_post_thumbnail_url('large'); ?>" alt="Photo for <?php the_title(); ?>" />
                            </div>          
                        </a>
                    </div>                           

                    <div class="info">
                        <div class="info-wrapper">
                            <div class="meta">
                                <?php get_template_part('template-parts/news/date'); ?>                               
                            </div>

                            <div class="headline">
                                <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                            </div>

                            <?php
                                $args = ['color' => 'red'];
                                get_template_part('template-parts/news/categories-list', null, $args);
                            ?>                            
                        </div>          
                    </div>
                </article>

            <?php $i++; endforeach; wp_reset_postdata(); ?>
        </div>
    </div>

<?php endif; ?>