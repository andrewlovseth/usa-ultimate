<article class="news archive">

	<div class="photo">
		<a href="<?php the_permalink(); ?>">
			<div class="content">
				<?php the_post_thumbnail('large'); ?>
			</div>			
		</a>
	</div>
			
	<div class="info">
		<div class="info-wrapper">

			<div class="meta">
				<?php get_template_part('template-parts/news/date'); ?>				
			</div>

			<div class="headline">
				<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
			</div>

			<?php
				$args = ['color' => 'red'];
				get_template_part('template-parts/news/categories-list', null, $args);
			?>

		</div>			
	</div>		

</article>