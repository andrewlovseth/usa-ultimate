<div class="social-share">
	<h4 class="sidebar-header">Share</h4>


	<div class="links">
		<div class="link facebook">
			<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink(); ?>" rel="external">
				<img src="<?php bloginfo('template_directory') ?>/images/icon-facebook-red.svg" alt="Facebook" />
			</a>
		</div>

		<div class="link twitter">
			<a href="https://twitter.com/intent/tweet/?text=<?php echo get_the_title(); ?>+<?php echo get_permalink(); ?>" rel="external">
				<img src="<?php bloginfo('template_directory') ?>/images/icon-twitter-red.svg" alt="Twitter" />
			</a>
		</div>

		<div class="link linkedin">
			<a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo get_permalink(); ?>&title=<?php echo get_the_title(); ?>" rel="external">
				<img src="<?php bloginfo('template_directory') ?>/images/icon-linkedin-red.svg" alt="LinkedIn" />
			</a>
		</div>
	</div>


</div>