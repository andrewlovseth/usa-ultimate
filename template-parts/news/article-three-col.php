<article class="news col article-<?php echo $count; ?>">
	<div class="photo">
		<a href="<?php the_permalink(); ?>">
			<div class="content">
				<?php $post_ID = get_the_ID(); if(get_field('square_photo_thumbnail', $post_ID)): ?>
					<picture>
						<source media="(max-width: 1023px)" srcset="<?php $thumb = get_field('square_photo_thumbnail', $post_ID); echo $thumb['sizes']['medium']; ?>">
						<source media="(min-width: 1024px)" srcset="<?php $featured_img_url = get_the_post_thumbnail_url($post_ID,'medium'); echo $featured_img_url; ?>">
						<?php the_post_thumbnail('medium'); ?>
					</picture>
				<?php else: ?>
					<?php the_post_thumbnail('medium'); ?>
				<?php endif; ?>
			</div>			
		</a>
	</div>

	<div class="info">
		<div class="info-wrapper">
			<div class="meta">
				<?php get_template_part('template-parts/news/date'); ?>				
			</div>

			<div class="headline">
				<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
			</div>

			<?php
				$args = ['color' => 'red'];
				get_template_part('template-parts/news/categories-list', null, $args);
			?>
		</div>			
	</div>
</article>