<article class="news cover cover-full article-<?php echo $count; ?>">
	<a class="cover-link" href="<?php the_permalink(); ?>">
	
		<div class="content">
			<?php $post_ID = get_the_ID(); if(get_field('square_photo_thumbnail', $post_ID)): ?>
				<picture>
					<source media="(max-width: 567px)" srcset="<?php $thumb = get_field('square_photo_thumbnail', $post_ID); echo $thumb['sizes']['medium']; ?>">
					<source media="(min-width: 568px)" srcset="<?php $featured_img_url = get_the_post_thumbnail_url($post_ID, 'large'); echo $featured_img_url; ?>">
					<?php the_post_thumbnail('large'); ?>
				</picture>
			<?php else: ?>
				<?php the_post_thumbnail('large'); ?>
			<?php endif; ?>	

			<div class="info">
				<div class="info-wrapper">

					<div class="meta">
						<?php get_template_part('template-parts/news/date'); ?>						
					</div>

					<div class="headline white">
						<h3><?php the_title(); ?></h3>
					</div>
	
				</div>			
			</div>		

		</div>	

	</a>

	<?php
		$args = ['color' => 'white'];
		get_template_part('template-parts/news/categories-list', null, $args);
	?>

</article>