<?php

$args = wp_parse_args($args);

if(!empty($args)) {
	$color = $args['color']; 
}

$categories = get_the_category(); if($categories): ?>
	<div class="categories">

		<?php if(is_singular('post')): ?>
			<h4 class="sidebar-header">Categories</h4>
		<?php endif; ?>

		<?php foreach($categories as $category): ?>

			<a class="cat-link <?php if($color) { echo $color; } ?>" href="<?php echo esc_url( get_category_link( $category->term_id ) ); ?>"><?php echo $category->name; ?></a>

		<?php endforeach; ?>

	</div>
<?php endif; ?>