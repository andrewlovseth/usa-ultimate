<article class="profile">
    <?php if(get_field('bio')): ?>
        <section class="bio profile-section">
            <div class="headline blue">
                <h4>Biography</h4>
            </div>

            <div class="copy p2 extended">
                <?php echo get_field('bio'); ?>
            </div>						
        </section>
    <?php endif; ?>

    <?php if(have_rows('playing_history')): ?>
        <section class="playing-history profile-section">

            <div class="headline blue">
                <h4>Playing History</h4>
            </div>

            <table class="playing-history-table p3">
                <thead>
                    <tr>
                        <th class="year">Year</th>
                        <th class="team">Team</th>
                    </tr>						
                </thead>

                <tbody>
                    <?php while(have_rows('playing_history')): the_row(); ?>

                        <tr>
                            <td class="year"><?php echo get_sub_field('years'); ?></td>
                            <td class="team"><?php echo get_sub_field('team'); ?></td>
                        </tr>

                    <?php endwhile; ?>
                </tbody>
            </table>
            
        </section>
    <?php endif; ?>

    <?php if(have_rows('career_highlights')): ?>
        <section class="career-highlights profile-section">
            
            <div class="headline blue">
                <h4>Career Highlights</h4>
            </div>
            
            <table class="career-highlights-table p3">
                <thead>
                    <tr>
                        <th class="year">Year</th>
                        <th class="highlight">Highlight</th>
                    </tr>						
                </thead>

                <tbody>
                    <?php while(have_rows('career_highlights')): the_row(); ?>

                        <tr>
                            <td class="year"><?php echo get_sub_field('year'); ?></td>
                            <td class="highlight"><?php echo get_sub_field('highlight'); ?></td>
                        </tr>

                    <?php endwhile; ?>
                </tbody>
            </table>

        </section>
    <?php endif; ?>

</article>
