<section class="player-gallery usau-block">

    <div class="section-header headline red underline">
        <h4>Explore the Roster</h4>
    </div>

    <div class="grid">
        <?php
            $args = array(
                'post_type' => 'world_games_players',
                'posts_per_page' => 40,
                'orderby' => 'title',
                'order' => 'ASC'
            );
            $query = new WP_Query( $args );
            if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

            <div class="player grid-item">

                <div class="photo">
                    <a href="<?php the_permalink(); ?>">
                        <img src="<?php $image = get_field('headshot'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                    </a>
                </div>

                <div class="info">
                    <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                </div>
            </div>

        <?php endwhile; endif; wp_reset_postdata(); ?>
        
    </div>

</section>