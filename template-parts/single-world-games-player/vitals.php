<?php

    $headshot = get_field('headshot');
    $instagram = get_field('instagram');
    $twitter = get_field('twitter');
    $nickname = get_field('nickname');
    $place_of_birth = get_field('place_of_birth');
    $hometown = get_field('hometown');
    $current_residence = get_field('current_residence');
    $birthdate = get_field('birthdate');
    $college = get_field('college');
    $club = get_field('club');
    $occupation = get_field('occupation');

?>


<aside class="vitals">
    <div class="vitals-wrapper">
        <?php if($headshot): ?>
            <div class="headshot">
                <?php echo wp_get_attachment_image($headshot['ID'], 'medium'); ?>
            </div>
        <?php endif; ?>

        <div class="name">
            <h2><?php the_title(); ?></h2>
        </div>

        <?php if($twitter || $instagram): ?>
            <div class="social">
                <?php if($twitter): ?>
                    <div class="social-link instagram">
                        <a href="<?php echo $instagram; ?>" target="window"><?php get_template_part('svg/icon-instagram'); ?></a>
                    </div>
                <?php endif; ?>

                <?php if($twitter): ?>
                    <div class="social-link twitter">
                        <a href="<?php echo $twitter; ?>" target="window"><?php get_template_part('svg/icon-twitter'); ?></a>
                    </div>
                <?php endif; ?>

            </div>


        <?php endif; ?>

        <?php if($nickname): ?>
            <div class="vital">
                <h3>Nickname</h3>
                <p><?php echo $nickname; ?></p>				
            </div>
        <?php endif; ?>

        <?php if($place_of_birth): ?>
            <div class="vital">
                <h3>Place of Birth</h3>
                <p><?php echo $place_of_birth; ?></p>				
            </div>
        <?php endif; ?>

        <?php if($hometown): ?>
            <div class="vital">
                <h3>Hometown</h3>
                <p><?php echo $hometown; ?></p>				
            </div>
        <?php endif; ?>

        <?php if($current_residence): ?>
            <div class="vital">
                <h3>Current Residence</h3>
                <p><?php echo $current_residence; ?></p>				
            </div>
        <?php endif; ?>

        <?php if($birthdate): ?>
            <div class="vital">
                <h3>Birthdate</h3>
                <p><?php echo $birthdate; ?></p>				
            </div>
        <?php endif; ?>

        <?php if($college): ?>
            <div class="vital">
                <h3>College</h3>
                <p><?php echo $college; ?></p>				
            </div>
        <?php endif; ?>

        <?php if($club): ?>
            <div class="vital">
                <h3>Club</h3>
                <p><?php echo $club; ?></p>				
            </div>
        <?php endif; ?>

        <?php if($occupation): ?>
            <div class="vital">
                <h3>Occupation</h3>
                <p><?php echo $occupation; ?></p>				
            </div>
        <?php endif; ?>
    </div>
</aside>