<section class="hero-photo hero-photo-world-games">
    <div class="photo">
        <div class="content">
            <img src="<?php $image = get_field('hero_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

            <div class="info">
                <div class="usau-block">
                    <?php if(get_field('jersey_number')): ?>
                        <div class="sub-headline">
                            <h2><span><?php if(get_field('jersey_number')): ?>#<?php echo get_field('jersey_number'); ?> / <?php endif; ?><?php echo get_field('position'); ?></span></h2>
                        </div>
                    <?php endif; ?>
                    <div class="headline">
                        <h1 class="cover-title"><?php the_title(); ?></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>