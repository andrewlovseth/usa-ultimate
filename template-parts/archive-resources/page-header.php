<section class="page-header usau-block">
    <div class="headline blue underline align-center">
        <h2><?php echo get_field('resources_archive_headline', 'options'); ?></h2>
    </div>

    <div class="copy p2">
        <?php echo get_field('resources_archive_copy', 'options'); ?>
    </div>
</section>