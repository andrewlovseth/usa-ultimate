<div class="filters-container">
    <div class="headline red uppercase align-center">
        <h5>Filters</h5>
    </div>    

    <div class="filters-wrapper">
        <?php echo do_shortcode('[ajax_load_more_filters id="resources_filter" target="resources_archive"]'); ?>

        <div class="filters-clear">
            <a href="#" class="clear-btn">Clear</a>
        </div>
    </div>
</div>