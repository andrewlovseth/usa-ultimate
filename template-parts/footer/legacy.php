<?php

$link = get_field('legacy_link', 'options');

if( $link ): 
$link_url = $link['url'];
$link_title = $link['title'];
$link_target = $link['target'] ? $link['target'] : '_self';

?>

    <section class="legacy-banner usau-block">

        <div class="link">
            <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
        </div>

    </section>

<?php endif; ?>

