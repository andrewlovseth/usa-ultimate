<div class="footer-columns">
	<div class="col footer-logo">
		<div class="usau-logo">
			<a href="<?php echo site_url('/'); ?>">
				<?php if(is_page(24)): ?>
					<img src="<?php $image = get_field('usa_ultimate_logo_white_lettering', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				<?php else: ?>
					<img src="<?php $image = get_field('usa_ultimate_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				<?php endif; ?>
			</a>
		</div>
	</div>

	<?php if(have_rows('footer_links', 'options')): $col = 1; while(have_rows('footer_links', 'options')) : the_row(); ?>
	 
	    <?php if( get_row_layout() == 'column' ): ?>

	    	<div class="links col col-<?php echo $col; ?>">
			
				<?php if(have_rows('groups')): $group = 1; while(have_rows('groups')) : the_row(); ?>
				 
				    <?php if( get_row_layout() == 'group' ): ?>

						<div class="group group-<?php echo $group; ?>">
								
									<?php if(get_sub_field('mobile_label') !== ""): ?>
										<?php
											$link = get_sub_field('header_link'); if( $link ): 
											$link_url = $link['url'];
											$link_title = $link['title'];
											$link_target = $link['target'] ? $link['target'] : '_self';
										?>
											<div class="mobile-header-link header-link">
												<a href="#"><?php echo esc_html( $link_title ); ?></a>
											</div>					    	

										<?php endif; ?>
									<?php else: ?>
										<?php
											$link = get_sub_field('header_link'); if( $link ): 
											$link_url = $link['url'];
											$link_title = $link['title'];
											$link_target = $link['target'] ? $link['target'] : '_self';
										?>
										
											<div class="header-link mobile-only">
												<a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
											</div>

										<?php endif; ?>
									<?php endif; ?>

							<div class="desktop-header-link header-link">
								<?php
									$link = get_sub_field('header_link'); if( $link ): 
								    $link_url = $link['url'];
								    $link_title = $link['title'];
								    $link_target = $link['target'] ? $link['target'] : '_self';
								?>

									<a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>								    	

								<?php endif; ?>
							</div>

							<?php if(have_rows('links')): ?>
								<div class="sub-links">
									<div class="link mobile-header">
										<a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo get_sub_field('mobile_label'); ?></a>		
									</div>
									<?php while(have_rows('links')): the_row(); ?>
									
										<div class="link">
											<?php get_template_part('template-parts/global/link'); ?>
										</div>

									<?php endwhile; ?>								
								</div>
							<?php endif; ?>
						</div>
						
				    <?php endif; ?>
				 
				<?php $group++; endwhile; endif; ?>

			</div>
			
	    <?php endif; ?>
	 
	<?php $col++; endwhile; endif; ?>
</div>