<div class="footer-utilities">
	<div class="copyright">
		<p><?php echo get_field('copyright', 'options'); ?></p>					
	</div>

	<div class="utility-links">
		<?php if(have_rows('footer_utility_links', 'options')): while(have_rows('footer_utility_links', 'options')): the_row(); ?>
		 
		    <?php get_template_part('template-parts/global/link'); ?>

		<?php endwhile; endif; ?>								
	</div>

	<?php get_template_part('template-parts/global/social-links'); ?>
</div>