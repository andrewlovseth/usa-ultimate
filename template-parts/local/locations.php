<?php

$abbr = get_field('abbreviation');
$spreadsheet_id = get_field('local_sheet_id', 'options');

if(get_transient('USAU_local-orgs')) {
    $entries = get_transient('USAU_local-orgs');
} else {
    $entries = usau_get_sheet_data('USAU_local-orgs', $spreadsheet_id, 600);
}

?>

<section class="locations usau-block">
    <div class="locations-grid">
        
        <?php
            $locations = array_filter($entries, function ($entry) use ($abbr) {
                return ($entry[1] == $abbr); }
            );
            
            if(!empty($locations)): ?>

            <?php foreach ($locations as $location): ?>

                <?php 
                    $organization = $location[0];
                    $state = $location[1];
                    $zip = $location[2];
                    $region = $location[3];
                    $city = $location[4];
                    $url = $location[5];
                    $short_description = $location[6];
                ?>

                <?php if($region !== $region_check): ?>

                    <?php if($region !== ''): ?>

                        <div class="headline blue underline region-header">
                            <h4><?php echo $region; ?></h4>
                        </div>

                    <?php endif; ?>

                <?php endif; $region_check = $region; ?>

                <div class="location">
                    <div class="headline">
                        <h5><?php echo $city; ?></h5>
                        <h6><?php echo $organization; ?></h6>
                    </div>

                    <?php if($url): ?>
                        <div class="url p4">
                            <a href="<?php echo $url; ?>" rel="external">Website</a>
                        </div>
                    <?php endif; ?>
                </div>

            <?php endforeach; ?>

        <?php else: ?>

            <div class="location error-message">
                <div class="headline blue">
                    <h4>No local organizations found.</h4>
                </div>
                <div class="copy p3">
                    <p>Check out other states listed below.</p>
                </div>						
            </div>

        <?php endif; ?>

    </div>
</section>
