<section class="states-list usau-block">
	<div class="list-columns">
		<?php
			$args = array(
				'post_type' => 'local',
				'posts_per_page' => 60,
				'orderby' => 'name',
				'order' => 'ASC'
			);
			$query = new WP_Query( $args );
			if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

			<div class="state">
				<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
			</div>

		<?php endwhile; endif; wp_reset_postdata(); ?>		
	</div>

	<div class="local-submit">
		<div class="headline white underline">
			<h5><?php echo get_field('local_submit_headline', 'options'); ?></h5>
		</div>

		<div class="copy p3">
			<?php echo get_field('local_submit_copy', 'options'); ?>
		</div>

		<?php 
			$link = get_field('local_submit_cta', 'options');
			if( $link ): 
			$link_url = $link['url'];
			$link_title = $link['title'];
			$link_target = $link['target'] ? $link['target'] : '_self';
		?>

			<div class="cta">
				<a class="btn white small" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
			</div>

		<?php endif; ?>
	</div>		
</section>