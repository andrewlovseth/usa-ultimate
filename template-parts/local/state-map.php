<?php

$abbr = get_field('abbreviation');
$graphic = esc_url( get_stylesheet_directory_uri() . "/images/states/" . $abbr . ".svg" );

if (!file_exists($graphic)): ?>

    <section class="map usau-block">
        <div class="graphic">
            <img src="<?php echo $graphic; ?>" alt="<?php echo $abbr; ?>" />
        </div>
    </section>

<?php endif; ?>