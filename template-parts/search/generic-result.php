<article class="generic">
    <div class="photo">
        
    </div>

    <div class="info">
        <div class="type">
            <h5><?php echo get_post_type(); ?></h5>
        </div>

        <div class="headline">
            <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
        </div>
    </div>
</article>