<article class="search-result-item team">
	<div class="photo">
		<div class="content">
            <a href="<?php the_permalink(); ?>">
            <img src="<?php $image = get_field('photo'); echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
			</a>								
		</div>
	</div>

	<div class="info">
		<div class="type">
			<h5>Team</h5>
		</div>

		<div class="headline">
			<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
		</div>
	</div>
</article>