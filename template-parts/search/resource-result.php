<?php
	$file = get_field('file');
	$file_url = $file['url'];
	$file_data_modified = $file['modified'];

?>

<article class="resource-result">
    <div class="photo">
        <div class="content">
            <div class="icon">
                <a href="<?php echo $file_url; ?>" target="_window">
                    <img src="<?php bloginfo('template_directory'); ?>/images/icon-pdf-red.svg" alt="PDF" />
                </a>
            </div>
        </div>
    </div>

    <div class="info">
        <div class="type">
            <h5>Resource</h5>
        </div>

        <div class="headline">
            <h4><a href="<?php echo $file_url; ?>" target="_window"><?php the_title(); ?></a></h4>
        </div>

        <?php if(get_field('description')): ?>
            <div class="copy p3">
                <?php echo get_field('description'); ?>
            </div>
        <?php endif; ?>

        <?php if(get_field('external_link')): ?>

            <div class="url p3">
                <a href="<?php echo get_field('link'); ?>" target="_window">Visit Site</a>
            </div>

        <?php else: ?>
            
            <div class="url p3">
                <a href="<?php echo $file_url; ?>" target="_window">View PDF</a>
            </div>

            <?php if($file_data_modified): ?>
                <div class="meta p3">
                    <em>Last Modified: <?php echo $file_data_modified; ?></em>
                </div>
            <?php endif; ?>

        <?php endif; ?>
    </div>
</article>