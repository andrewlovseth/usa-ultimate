<article class="search-result-item video-result">

    <div class="photo">

        <?php 
            $title = get_the_title();
            $thumbnail  = get_field('thumbnail');
            $youtube_id = get_field('youtube_id');
            $is_vimeo = get_field('vimeo');
            $vimeo_id = get_field('vimeo_id');

            if($is_vimeo == true): ?>

                <?php
                    if($vimeo_id) {
                        $video_endpoint = 'http://vimeo.com/api/v2/video/' . $vimeo_id . '.json';
                        $video_response = file_get_contents($video_endpoint);
                        $video_data = json_decode($video_response, true);
                        $vimeo_thumbnail = $video_data[0]['thumbnail_large'];
                    }
                ?>

                <div class="video">
                    <a href="#" class="video-trigger" data-title="<?php echo $title; ?>" data-video-id="<?php echo $vimeo_id; ?>" data-video-type="vimeo">
                        <div class="thumbnail">
                            <div class="play-btn">
                                <div class="triangle"></div>
                            </div>

                            <div class="content">
                                <?php if($thumbnail): ?>
                                    <img src="<?php echo $thumbnail['url']; ?>" alt="Video Thumbnail: <?php echo $title; ?>" />
                                <?php else: ?>
                                    <img src="<?php echo $vimeo_thumbnail; ?>?mw=960&mh=540" alt="Video Thumbnail: <?php echo $title; ?>" />
                                <?php endif; ?>				
                            </div>
                        </div>
                    </a>
                </div>

        <?php 
            // YouTube
                else: ?>

                <div class="video">
                    <a href="#" class="video-trigger" data-title="<?php echo $title; ?>" data-video-id="<?php echo $youtube_id; ?>" data-video-type="youtube">
                        <div class="thumbnail">
                            <div class="play-btn">
                                <div class="triangle"></div>
                            </div>

                            <div class="content">
                                <?php if($thumbnail): ?>
                                    <img src="<?php echo $thumbnail['url']; ?>" alt="Video Thumbnail: <?php echo $title; ?>" />
                                <?php else: ?>
                                    <img src="https://i3.ytimg.com/vi/<?php echo $youtube_id; ?>/hqdefault.jpg" alt="Video Thumbnail: <?php echo $title; ?>" />
                                <?php endif; ?>				
                            </div>
                        </div> 
                    </a>
                </div>

        <?php endif ; ?>

    </div>

    <div class="info">
        <div class="type">
            <h5>Video</h5>
        </div>

        <div class="headline">
            <?php if($is_vimeo == true): ?>                
                <h4><a href="#" class="video-trigger" data-title="<?php echo $title; ?>" data-video-id="<?php echo $vimeo_id; ?>" data-video-type="vimeo"><?php the_title(); ?></a></h4>
            <?php else: ?>
                <h4><a href="#" class="video-trigger" data-title="<?php echo $title; ?>" data-video-id="<?php echo $youtube_id; ?>" data-video-type="youtube"><?php the_title(); ?></a></h4>
            <?php endif ; ?>
        </div>
    </div>

</article>






