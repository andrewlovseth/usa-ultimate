<?php 
	if ( function_exists( 'get_field' ) ) {
		$pid = get_post();
		if ( has_blocks( $pid ) ) {
			$blocks = parse_blocks( $pid->post_content );

			foreach ( $blocks as $block ) {
				if ( $block['blockName'] === 'acf/hero-photo' ) {
					$photo = $block['attrs']['data']['photo'];
					$image_arr = wp_get_attachment_image_src($photo, 'medium');
					$image_url = $image_arr[0];
					echo '<img src=' . $image_url . ' />';
				}
			}
		}
	}