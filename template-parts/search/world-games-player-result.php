<article class="search-result-item world-games-player">
	<div class="photo">
		<div class="content">
            <a href="<?php the_permalink(); ?>">
                <img src="<?php $image = get_field('headshot'); echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
			</a>								
		</div>
	</div>

	<div class="info">
		<div class="type">
			<h5>World Games Player</h5>
		</div>

		<div class="headline">
			<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
		</div>		

		<div class="copy p2">
            <?php echo wp_trim_words( get_field('bio'), 25, '...' ); ?>
		</div>
    </div>
</article>