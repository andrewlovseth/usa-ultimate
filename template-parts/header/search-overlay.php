<section id="search-overlay">
	<div class="overlay">
		<div class="overlay-wrapper">

			<div class="info">
				<div class="close">
					<a href="#" class="search-close-btn close-btn"></a>
				</div>

				<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
				    <label>
				    	<span class="search-label">Search the site</span>
						<input type="search" class="search-field"
							data-swplive="true"
				            placeholder="<?php echo esc_attr_x( '(e.g. "rankings" or "schedule")', 'placeholder' ) ?>"
				            value="<?php echo get_search_query() ?>" name="s"
				            title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
				    </label>
				    <input type="submit" class="search-submit"
				        value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
				</form>

				<div class="common-searches">
					<h4>Common site searches:</h4>
					
					<ul>
						<li><a href="<?php echo site_url('/watch/'); ?>">Video highlights</a></li>
						<li><a href="<?php echo site_url('/rules/'); ?>">Rules of ultimate</a></li>
						<li><a href="<?php echo site_url('/spirit-of-the-game/'); ?>">Spirit of the Game</a></li>
					</ul>
				</div>
			</div>
			
		</div>
	</div>
</section>