<?php if(have_rows('desktop_menu', 'options')): ?>
	<div class="desktop-menu">

		<?php while(have_rows('desktop_menu', 'options')): the_row(); ?>

			<?php $link = get_sub_field('link'); if( $link ): 
			    $link_url = $link['url'];
			    $link_title = $link['title'];
			    $link_target = $link['target'] ? $link['target'] : '_self';
			    ?>
			    <a href="<?php echo esc_url( $link_url ); ?>" class="nav-<?php echo sanitize_title_with_dashes($link_title); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
			<?php endif; ?>

		<?php endwhile; ?>
		
	</div>
<?php endif; ?>