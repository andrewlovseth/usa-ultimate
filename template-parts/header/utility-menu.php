<nav class="utility-menu">
	<?php if(have_rows('announcement_menu', 'options')): ?>
		<div class="announcement-menu">

			<?php while(have_rows('announcement_menu', 'options')): the_row(); ?>

				<?php $link = get_sub_field('link'); if( $link ): 
				    $link_url = $link['url'];
				    $link_title = $link['title'];
				    $link_target = $link['target'] ? $link['target'] : '_self';
				    ?>
				    <a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
				<?php endif; ?>			 

			<?php endwhile; ?>
			
		</div>
	<?php endif; ?>

	<div class="utility-links">
		<div class="search-link">
			<a href="#" class="search-trigger">
				<span class="icon"><img src="<?php bloginfo('template_directory'); ?>/images/icon-search-white.svg" alt="Search Icon" /></span>
				<span class="label">Search</span>
			</a>				
		</div>

		<?php get_template_part('template-parts/global/social-links'); ?>

		<div class="login-link">
			<?php $link = get_field('login_link', 'options'); if( $link ): 
			    $link_url = $link['url'];
			    $link_title = $link['title'];
			    $link_target = $link['target'] ? $link['target'] : '_self';
			    ?>
			    <a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
			<?php endif; ?>		
		</div>				
	</div>
</nav>