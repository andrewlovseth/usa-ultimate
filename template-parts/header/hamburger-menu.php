<nav class="mobile-menu">
	<div class="menu-wrapper">

		<a href="#" class="close nav-trigger">
			<img src="<?php bloginfo('template_directory'); ?>/images/icon-close-red.svg" alt="Close" />
		</a>

		<div class="language-switcher">
			<?php echo do_shortcode('[gtranslate]'); ?>
		</div>

		<div class="links">

			
			<?php if(have_rows('hamburger_menu', 'options')): while(have_rows('hamburger_menu', 'options')) : the_row(); ?>
			 
			    <?php if( get_row_layout() == 'group' ): ?>
					
					<div class="group">
						<?php if(have_rows('links')): while(have_rows('links')): the_row(); ?>

							<?php $link = get_sub_field('link'); if( $link ): 
							    $link_url = $link['url'];
							    $link_title = $link['title'];
							    $link_target = $link['target'] ? $link['target'] : '_self';
							    ?>
							    <div class="link">
								    <a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>								    	
							    </div>

							<?php endif; ?>

						<?php endwhile; endif; ?>	
					</div>
					
			    <?php endif; ?>
			 
			<?php endwhile; endif; ?>			
		</div>			

	</div>
</nav>