<div class="site-logo">
	<div class="usau-logo">
		<a href="<?php echo site_url('/'); ?>">
			<?php if(is_page(24)): ?>
				<img src="<?php $image = get_field('usa_ultimate_logo_white_lettering', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			<?php else: ?>
				<img src="<?php $image = get_field('usa_ultimate_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			<?php endif; ?>
		</a>
	</div>

	<div class="olympic-logo">
		<a href="<?php echo get_field('olympic_logo_link', 'options'); ?>" rel="external">
			<?php if(is_page(24)): ?>
				<img src="<?php $image = get_field('olympic_logo_white', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			<?php else: ?>
				<img src="<?php $image = get_field('olympic_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			<?php endif; ?>
		</a>
	</div>
</div>