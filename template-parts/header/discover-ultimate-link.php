<div class="discover-ultimate-link">
	<?php $discover = get_field('discover_ultimate', 'options'); ?>

	<a href="<?php echo site_url($discover['link']); ?>">
		<span class="icon">
			<img src="<?php echo esc_url( $discover['icon']['url'] ); ?>" alt="<?php echo esc_attr( $discover['icon']['alt'] ); ?>" />
		</span>
		<span class="label">
			<?php echo $discover['label']; ?>
		</span>
	</a>
</div>