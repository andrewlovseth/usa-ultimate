<?php if(have_rows('playlists')): while(have_rows('playlists')) : the_row(); ?>
	 
     <?php if( get_row_layout() == 'playlist' ): ?>
         
         <section class="usau-block playlist playlist-<?php echo sanitize_title_with_dashes(get_sub_field('title')); ?>">

             <div class="section-header headline blue underline">
                 <h3 class="has-view-all">
                     <?php echo get_sub_field('name'); ?>
                     
                     <?php $term = get_sub_field('filter_link'); if( $term ): ?>
                         <a href="<?php echo esc_url( get_term_link( $term ) ); ?>" class="p3 view-all">View all <?php echo esc_html( $term->name ); ?> videos</a>
                     <?php endif; ?>
                 </h3>
             </div>
             
             <?php $posts = get_sub_field('videos'); if( $posts ): ?>
                 <div class="videos">
                     <?php foreach( $posts as $p ): ?>

                         <?php 
                             $args = ['post' => $p->ID];
                             get_template_part('template-parts/watch/video', null, $args);
                         ?>

                     <?php endforeach; ?>
                 </div>
             <?php endif; ?>

         </section>
         
     <?php endif; ?>
  
 <?php endwhile; endif; ?>