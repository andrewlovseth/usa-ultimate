<?php

    $banner = get_field('banner');
    $show = $banner['show_banner'];
    $link = $banner['link'];
    $headline = $banner['headline'];
    $deck = $banner['deck'];

    if($link) {
        $link_url = $link['url'];
        $link_title = $link['title'];
        $link_target = $link['target'] ? $link['target'] : '_self';
    }

    if($show == TRUE):

?>

    <section class="banner usau-block">
        <div class="cta-flex">
            <a href="<?php echo esc_url($link_url); ?>">
                <h3><?php echo $headline; ?></h3>
                <p><?php echo $deck; ?></p>
            </a>
        </div> 
    </section>

<?php endif; ?>