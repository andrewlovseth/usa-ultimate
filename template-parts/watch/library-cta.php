<section class="library-cta usau-block">
		
	<div class="cta-flex">
		<h3><a href="<?php echo site_url('/watch/videos/'); ?>">See our entire library of games, highlights, and videos.</a></h3>

		<div class="cta">
			<a href="<?php echo site_url('/watch/videos/'); ?>" class="btn white small">View Library</a>
		</div>
	</div>

</section>