<?php 

// Set defaults.
$args = wp_parse_args($args);
$p = $args['post']; 

$title = get_the_title($p);
$thumbnail  = get_field('thumbnail', $p);
$youtube_id = get_field('youtube_id', $p);
$is_vimeo = get_field('vimeo', $p);
$vimeo_id = get_field('vimeo_id', $p);

if($is_vimeo == true): ?>

	<?php
		if($vimeo_id) {
			$video_endpoint = 'http://vimeo.com/api/v2/video/' . $vimeo_id . '.json';
			$video_response = file_get_contents($video_endpoint);
			$video_data = json_decode($video_response, true);
			$vimeo_thumbnail = $video_data[0]['thumbnail_large'];
		}
	?>

	<div class="video video-thumbnail">
		<a href="#" class="video-trigger" data-title="<?php echo $title; ?>" data-video-id="<?php echo $vimeo_id; ?>" data-video-type="vimeo">
			<div class="thumbnail">
				<div class="play-btn">
					<div class="triangle"></div>
				</div>

				<div class="content">
					<?php if($thumbnail): ?>
						<img src="<?php echo $thumbnail['url']; ?>" alt="Video Thumbnail: <?php echo $title; ?>" />
					<?php else: ?>
						<img src="<?php echo $vimeo_thumbnail; ?>?mw=960&mh=540" alt="Video Thumbnail: <?php echo $title; ?>" />
					<?php endif; ?>				
				</div>
			</div>

			<span class="title p3"><?php echo $title ?></span>
		</a>
	</div>

<?php 
// YouTube
	else: ?>

	<div class="video video-thumbnail">
		<a href="#" class="video-trigger" data-title="<?php echo $title; ?>" data-video-id="<?php echo $youtube_id; ?>" data-video-type="youtube">
			<div class="thumbnail">
				<div class="play-btn">
					<div class="triangle"></div>
				</div>

				<div class="content">
					<?php if($thumbnail): ?>
						<img src="<?php echo $thumbnail['url']; ?>" alt="Video Thumbnail: <?php echo $title; ?>" />
					<?php else: ?>
						<img src="https://i3.ytimg.com/vi/<?php echo $youtube_id; ?>/hqdefault.jpg" alt="Video Thumbnail: <?php echo $title; ?>" />
					<?php endif; ?>				
				</div>
			</div>

			<span class="title p3"><?php echo $title; ?></span>
		</a>
	</div>

<?php endif ; ?>