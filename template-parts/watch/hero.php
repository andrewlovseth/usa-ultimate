<section class="hero usau-block">
    <a href="#" class="video-trigger" data-title="<?php $featured_video = get_field('hero_video'); echo get_the_title($featured_video->ID); ?>" data-video-id="<?php echo get_field('youtube_id', $featured_video->ID); ?>">
        <div class="video">
            <div class="content">
                <iframe src="https://player.vimeo.com/video/<?php echo get_field('hero_background_video_id'); ?>?muted=1&autoplay=1&loop=1&controls=0&byline=0&playsinline=1&quality=1080p" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>		
            </div>

            <div class="info">
                <div class="headline">
                    <h1>Watch</h1>

                    <div class="link">
                        <div class="play-btn">
                            <div class="triangle"></div>
                        </div>

                        <span class="label"><?php echo get_field('hero_video_label'); ?></span>
                    </div>
                </div>
            </div>
        </div>
    </a>
</section>