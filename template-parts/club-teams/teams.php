<?php if(have_rows('club_teams', 'options')): ?>
	
	<nav class="anchor-links usau-block teams-anchor-links">
		<div class="nav-flexbox">
			<div class="nav-label">
				<h4>Jump to:</h4>
			</div>

			<div class="links">
				<?php while(have_rows('club_teams', 'options')) : the_row(); ?>
					<?php if( get_row_layout() == 'division' ): ?>

						<div class="link">
							<a href="#<?php echo sanitize_title_with_dashes(get_sub_field('section_header')); ?>" class="smooth"><?php echo get_sub_field('section_header'); ?></a>
						</div>

					<?php endif; ?>

				<?php endwhile; ?>
			</div>			
		</div>
	</nav>

<?php endif; ?>

<?php if(have_rows('club_teams', 'options')): while(have_rows('club_teams', 'options')) : the_row(); ?>

	<?php if( get_row_layout() == 'division' ): ?>

		<section class="division teams-list usau-block" id="<?php echo sanitize_title_with_dashes(get_sub_field('section_header')); ?>">
				
			<div class="headline blue section-header underline">
				<h4><?php echo get_sub_field('section_header'); ?></h4>
			</div>

			<?php $posts = get_sub_field('teams', 'options'); if( $posts ): ?>
				<div class="grid teams-grid">

					<?php foreach( $posts as $p ): ?>

						<?php include(locate_template('template-parts/club-teams/team.php')); ?>

					<?php endforeach; ?>

				</div>
			<?php endif; ?>

			<?php 
				$link = get_sub_field('link');
				if( $link ): 
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self';
			?>

				<div class="cta">
					<a class="underline red" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
				</div>

			<?php endif; ?>

		</section>

	<?php endif; ?>

<?php endwhile; endif; ?>

