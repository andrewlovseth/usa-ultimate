<section class="page-header usau-block">
	<?php if(get_field('club_teams_page_header_headline', 'options')): ?>

		<div class="section-header headline blue underline">
			<h3><?php echo get_field('club_teams_page_header_headline', 'options'); ?></h3>
		</div>

	<?php endif; ?>

	<?php if(get_field('club_teams_page_header_copy', 'options')): ?>

		<div class="copy p2">
			<p><?php echo get_field('club_teams_page_header_copy', 'options'); ?></p>
		</div>
	
	<?php endif; ?>
</section>