<section id="video-overlay">
	<div class="overlay">
		<div class="overlay-wrapper">

			<div class="info">
				<div class="close">
					<a href="#" class="video-close-btn close-btn"></a>
				</div>

				<div class="video-frame">
					<div class="content">
					    
					</div>				    	
				</div>

				<div class="title">
					
				</div>
			</div>
			
		</div>
	</div>
</section>
