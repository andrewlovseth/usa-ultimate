<div class="social-links">
    <?php if(have_rows('social_links', 'options')): while(have_rows('social_links', 'options')): the_row(); ?>

        <div class="link">
            <a href="<?php echo get_sub_field('link'); ?>" rel="external">
                <img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
            </a>					        
        </div>

    <?php endwhile; endif; ?>					
</div>