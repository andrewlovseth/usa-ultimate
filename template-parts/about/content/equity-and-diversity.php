<?php get_template_part('template-parts/about/global/about-page-header'); ?>

    <div class="headline section-header blue">
        <h4><?php echo get_field('headline'); ?></h4>
    </div>

    <div class="copy p2">
        <?php echo get_field('copy'); ?>
    </div>

    <?php 
        $link = get_field('cta');
        if( $link ): 
        $link_url = $link['url'];
        $link_title = $link['title'];
        $link_target = $link['target'] ? $link['target'] : '_self';
    ?>

        <div class="cta">
            <a class="underline red" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
        </div>

    <?php endif; ?>

