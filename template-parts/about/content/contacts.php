<?php get_template_part('template-parts/about/global/about-page-header'); ?>

<div class="filter">
    <form class="contacts-filter">
        <input type="text" id="filter-field" placeholder="Type to filter" />
    </form>
</div>

<nav class="anchor-links">
    <div class="nav-flexbox">
        <div class="nav-label">
            <h4>Jump to:</h4>
        </div>

        <div class="links">
            <?php if(have_rows('sections')): while(have_rows('sections')): the_row(); ?>
            
                <div class="link">
                    <a href="#<?php echo sanitize_title_with_dashes(get_sub_field('title')); ?>" class="smooth"><?php echo get_sub_field('title'); ?></a>
                </div>

            <?php endwhile; endif; ?>
        </div>			
    </div>
</nav>



<?php if(have_rows('sections')): while(have_rows('sections')): the_row(); ?>

    <?php

        $spreadsheet_id = get_sub_field('sheet_id');
        $sheet = get_sub_field('title');
        $tab_id = get_sub_field('index');
        $section_slug = sanitize_title_with_dashes(get_sub_field('title'));

        if(get_transient('USAU_contacts_' . $section_slug)) {
            $entries = get_transient('USAU_contacts_' . $section_slug);
        } else {
            $entries = usau_get_sheet_data('USAU_contacts_' . $section_slug, $spreadsheet_id, 600, $sheet);
        }


    ?>

    <?php if(!empty($entries)): ?>

        <div class="section contacts-list" id="<?php echo $section_slug; ?>">	
            <div class="headline section-header red">
                <h4><?php echo get_sub_field('title'); ?></h4>
            </div>

            <table class="contact-table">
                <thead>
                    <tr>
                        <?php $col = 0; if(have_rows('columns')): while(have_rows('columns')): the_row(); ?>
                            
                            <th class="<?php echo get_sub_field('slug'); ?>"><?php echo get_sub_field('name'); ?></th>

                            <?php
                                if(get_sub_field('name') == 'Name') {
                                    $name_col = $col;
                                }

                            ?>

                        <?php $col++; endwhile; endif; ?>											
                    </tr>
                </thead>

                <tbody>
                    <?php foreach ($entries as $entry): ?>

                        <?php
                            //var_dump($name_col);
                            $trClassName = "contacts-row";                        
                            if($entry[$name_col] == 'Vacant') {
                                $trClassName .= ' vacant';
                            }
                        ?>

                        <tr class="<?php echo $trClassName; ?>">
                            <?php $i = 0; if(have_rows('columns')): while(have_rows('columns')): the_row(); ?>
                                <?php
                                    $slug = get_sub_field('slug');
                                    $datum = $entry[$i];
                                    $tdClassName = $slug;          
                                ?>
                                
                                <?php if($slug == "email"): ?>
                                    <td class="<?php echo $tdClassName; ?>"><a href="mailto:<?php echo $datum; ?>"><?php echo $datum; ?></a></td>
                                <?php else: ?>
                                    <td class="<?php echo $tdClassName; ?>"><?php echo $datum; ?></td>
                                <?php endif; ?>
                                
                            <?php $i++; endwhile; endif; ?>													
                        </tr>					

                    <?php endforeach; ?>

                </tbody>
            </table>
        </div>

    <?php else: ?>

        <div class="section contacts-list none">
            <p>No contact information available at this time.</p>
        </div>

    <?php endif; ?>

<?php endwhile; endif; ?>