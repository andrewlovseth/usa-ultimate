<div class="intro">
    <div class="headline blue underline">
        <h3><?php echo get_field('volunteer_headline'); ?></h3>
    </div>

    <div class="copy p2">
        <?php echo get_field('volunteer_copy'); ?>
    </div>
</div>

<nav class="anchor-links volunteer-anchor-links">
    <div class="nav-flexbox">
        <div class="nav-label">
            <h4>Jump to:</h4>
        </div>

        <div class="links">
            <?php if(have_rows('regions')): while(have_rows('regions')) : the_row(); ?>

                <?php if( get_row_layout() == 'region' ): ?>

                    <div class="link">
                        <a href="#<?php echo sanitize_title_with_dashes(get_sub_field('region_name')); ?>" class="smooth"><?php echo get_sub_field('region_name'); ?></a>
                    </div>

                <?php endif; ?>

            <?php endwhile; endif; ?>
        </div>			
    </div>
</nav>

<?php if(have_rows('regions')): while(have_rows('regions')) : the_row(); ?>

    <?php if( get_row_layout() == 'region' ): ?>

        <div class="volunteer-section region" id="<?php echo sanitize_title_with_dashes(get_sub_field('region_name')); ?>">
            <div class="headline blue underline">
                <h4><?php echo get_sub_field('region_name'); ?></h4>
            </div>

            <div class="positions">
                <?php if(have_rows('volunteer_positions')): ?>
                
                    <?php while(have_rows('volunteer_positions')): the_row(); ?>
                    
                        <div class="listing">
                            <div class="headline blue">
                                <h5><?php echo get_sub_field('title'); ?></h5>
                            </div>
                 
                            <?php if(get_sub_field('application_deadline')): ?>
                                <div class="headline red application-deadline">
                                    <h5>Application Deadline: <span><?php echo get_sub_field('application_deadline'); ?></span></h5>
                                </div>
                            <?php endif; ?>

                            <div class="copy p3">
                                <p>
                                    <?php echo get_sub_field('short_description'); ?>
                                    <?php if(get_sub_field('job_description')): ?>
                                        <a href="<?php echo get_sub_field('job_description'); ?>" rel="external">Full description (PDF)</a>
                                    <?php endif; ?>
                                </p>
                            </div>

                            <?php 
                                $link = get_sub_field('apply_link');

                                if( $link ): 
                                $link_url = $link['link'];
                                $link_type = $link['type'];
                            ?>

                                <div class="cta">
                                    <a class="underline red" href="<?php if($link_type == "email"): ?>mailto:<?php endif; ?><?php echo $link_url; ?>">Apply Now</a>
                                </div>

                            <?php endif; ?>
                            
                        </div>

                    <?php endwhile; ?>
                
                <?php else: ?>
                    
                    <div class="listing no-openings">
                        <div class="headline blue">
                            <h5><?php echo get_field('no_openings_headline'); ?></h5>
                        </div>

                        <div class="copy p3">
                            <p><?php echo get_field('no_openings_copy'); ?></p>
                        </div>
                    </div>
                
                <?php endif; ?>
            </div>
        </div>

    <?php endif; ?>

<?php endwhile; endif; ?>

