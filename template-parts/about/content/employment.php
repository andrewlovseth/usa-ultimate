<div class="intro">
    <div class="headline blue underline">
        <h3><?php echo get_field('jobs_headline'); ?></h3>
    </div>

    <div class="copy p2">
        <?php echo get_field('jobs_copy'); ?>
    </div>
</div>

<div class="employment-positions">
    <?php if(have_rows('jobs')): ?>

        <?php while(have_rows('jobs')) : the_row(); ?>
            <?php if( get_row_layout() == 'job' ): ?>

                <div class="listing">
                    <div class="headline blue">
                        <h4><?php echo get_sub_field('title'); ?></h4>
                    </div>

                    <?php if(get_sub_field('application_deadline')): ?>
                        <div class="headline red application-deadline">
                            <h5>Application Deadline: <span><?php echo get_sub_field('application_deadline'); ?></span></h5>
                        </div>
                    <?php endif; ?>

                    <div class="copy p2">
                        <p>
                            <?php echo get_sub_field('short_description'); ?>
                            <?php if(get_sub_field('job_description')): ?>
                                <a href="<?php echo get_sub_field('job_description'); ?>" rel="external">Full description (PDF)</a>
                            <?php endif; ?>
                        </p>
                    </div>

                    <?php 
                        $link = get_sub_field('apply_link');
                        if( $link ): 
                        $link_url = $link['link'];
                        $link_type = $link['type'];
                    ?>

                        <div class="cta">
                            <a class="underline red" href="<?php if($type == "email"): ?>mailto:<?php endif; ?><?php echo esc_url($link_url); ?>">Apply Now</a>
                        </div>

                    <?php endif; ?>                    
                </div>

            <?php endif; ?>
        <?php endwhile; ?>

    <?php else: ?>

        <div class="listing no-listings">
            <div class="headline blue">
                <h4><?php echo get_field('no_listings_headline'); ?></h4>
            </div>

            <div class="copy p2">
                <?php echo get_field('no_listings_copy'); ?>
            </div>                 
        </div>

    <?php endif; ?>
</div>