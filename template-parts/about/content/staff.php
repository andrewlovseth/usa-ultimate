<?php get_template_part('template-parts/about/global/about-page-header'); ?>

<?php if(have_rows('staff')): while(have_rows('staff')) : the_row(); ?>

	<?php if( get_row_layout() == 'department' ): ?>

		<div class="department">

			<div class="department-header headline blue">
				<h4><?php $dept_name = get_sub_field('department_name'); echo $dept_name; ?></h4>
			</div>

			<div class="people-grid">
				<?php if(have_rows('members')): while(have_rows('members')): the_row(); ?>

				    <div class="person">
				    	<a href="#<?php $profile_slug = sanitize_title_with_dashes(get_sub_field('name')); echo $profile_slug; ?>" class="profile-trigger">
					    	<div class="photo">
					    		<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					    	</div>

					    	<div class="info">
					    		<div class="headline name">
					    			<h5><?php echo get_sub_field('name'); ?></h5>
					    		</div>

					    		<?php if(get_sub_field('position')): ?>

						    		<div class="meta p4">
						    			<p><?php echo get_sub_field('position'); ?></p>
						    		</div>
				    			<?php endif; ?>

					    	</div>
				    	</a>

						<section class="profile-overlay" id="<?php echo $profile_slug; ?>">
							<div class="overlay">
								<div class="overlay-wrapper">

									<div class="info">
										<div class="close">
											<a href="#" class="profile-close-btn close-btn"></a>
										</div>

										<div class="profile-wrapper">
									    	<div class="photo">
									    		<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
									    	</div>

									    	<div class="info">
									    		<div class="name">
									    			<h4><?php echo get_sub_field('name'); ?></h4>
									    		</div>

									    		<div class="position headline">
									    			<h5><?php echo $dept_name; ?></h5>
									    			<em><?php echo get_sub_field('position'); ?></em>

										    		<span class="meta">
										    			<?php if(get_sub_field('term')): ?>
										    				<span><?php echo get_sub_field('term'); ?></span>
										    			<?php endif; ?>

										    			<?php if(get_sub_field('role')): ?>
										    				<span><?php echo get_sub_field('role'); ?></span>
										    			<?php endif; ?>
										    		</span>
									    		</div>

									    		<div class="biography copy p3">
									    			<?php echo get_sub_field('biography'); ?>
									    		</div>

									    		<?php if(get_sub_field('email')): ?>
									    			<div class="email p3">
									    				<p>Email: <a href="mailto:<?php echo get_sub_field('email'); ?>"><?php echo get_sub_field('email'); ?></a></p>
									    			</div>
									    		<?php endif; ?>
								    		</a>
										</div>


									</div>
									
								</div>
							</div>
						</section>

				    </div>

				<?php endwhile; endif; ?>
			</div>
				
		</div>
	
	<?php endif; ?>

<?php endwhile; endif; ?>