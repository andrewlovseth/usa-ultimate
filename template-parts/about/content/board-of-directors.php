<?php get_template_part('template-parts/about/global/about-page-header'); ?>

<div class="people-grid">

	<?php if(have_rows('members')): while(have_rows('members')): the_row(); ?>

		<?php $profile_slug = sanitize_title_with_dashes(get_sub_field('name')); ?>

	    <div class="person">
	    	<a href="#<?php echo $profile_slug; ?>" class="profile-trigger">
		    	<div class="photo">
		    		<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		    	</div>

		    	<div class="info">
	    			<?php if(get_sub_field('officer_title')): ?>
	    				<div class="officer">
	    					<h5><?php echo get_sub_field('officer_title'); ?></h5>
	    				</div>			    				
	    			<?php endif; ?>

		    		<div class="name">
		    			<h4><?php echo get_sub_field('name'); ?></h4>
		    		</div>

		    		<div class="location">
		    			<em><?php echo get_sub_field('location'); ?></em>
		    		</div>
	    		</a>
	    	</div>

			<section class="profile-overlay" id="<?php echo $profile_slug; ?>">
				<div class="overlay">
					<div class="overlay-wrapper">

						<div class="info">
							<div class="close">
								<a href="#" class="profile-close-btn close-btn"></a>
							</div>

							<div class="profile-wrapper">
						    	<div class="photo">
						    		<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						    	</div>

						    	<div class="info">
					    			<?php if(get_sub_field('officer_title')): ?>
					    				<div class="officer">
					    					<h5><?php echo get_sub_field('officer_title'); ?></h5>
					    				</div>			    				
					    			<?php endif; ?>

						    		<div class="name headline">
						    			<h5><?php echo get_sub_field('name'); ?></h5>
						    		</div>

						    		<div class="location">
						    			<em><?php echo get_sub_field('location'); ?></em>

							    		<span class="meta">
							    			<?php if(get_sub_field('term')): ?>
							    				<span><?php echo get_sub_field('term'); ?></span>
							    			<?php endif; ?>

							    			<?php if(get_sub_field('role')): ?>
							    				<span><?php echo get_sub_field('role'); ?></span>
							    			<?php endif; ?>
							    		</span>
						    		</div>

						    		<div class="biography copy p3">
						    			<?php echo get_sub_field('biography'); ?>
						    		</div>

						    		<?php if(get_sub_field('email')): ?>
						    			<div class="email p3">
						    				<p>Email: <a href="mailto:<?php echo get_sub_field('email'); ?>"><?php echo get_sub_field('email'); ?></a></p>
						    			</div>
						    		<?php endif; ?>
					    		</a>
							</div>

						</div>
						
					</div>
				</div>
			</section>
	    </div>

	<?php endwhile; endif; ?>
	
</div>	