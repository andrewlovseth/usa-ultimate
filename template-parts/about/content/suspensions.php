<section class="about-page-header">
    <div class="headline blue underline uppercase">
        <h2><?php echo get_field('title'); ?></h2>
    </div>

    <div class="copy p2">
        <?php echo get_field('copy'); ?>
    </div>
</section>

<?php

    $spreadsheet_id = get_field('sheet_id');

    if(get_transient('USAU_suspensions')) {
        $entries = get_transient('USAU_suspensions');
    } else {
        $entries = usau_get_sheet_data('USAU_suspensions', $spreadsheet_id, DAY_IN_SECONDS);
    }

?>

<?php if(!empty($entries)): ?>

    <div class="suspensions-list">	
        <table class="suspensions-table">
            <thead>
                <tr>
                    <?php if(have_rows('columns')): while(have_rows('columns')): the_row(); ?>                        
                        <th class="<?php echo get_sub_field('slug'); ?>"><?php echo get_sub_field('name'); ?></th>
                    <?php endwhile; endif; ?>											
                </tr>
            </thead>

            <tbody>
                <?php foreach ($entries as $entry): ?>

                    <tr class="suspensions-row">
                        <?php $i = 0; if(have_rows('columns')): while(have_rows('columns')): the_row(); ?>

                            <?php
                                $slug = get_sub_field('slug');
                                $datum = $entry[$i];
                            ?>	
                                <td class="<?php echo get_sub_field('slug'); ?>"><?php echo $datum; ?></td>											    
                        <?php $i++; endwhile; endif; ?>
                    </tr>					

                <?php endforeach; ?>

            </tbody>
        </table>
    </div>

<?php endif; ?>
