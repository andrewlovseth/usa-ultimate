<?php get_template_part('template-parts/about/global/about-page-header'); ?>

<?php 
    $shortcode = '[ajax_load_more id="about-news" container_type="div" theme_repeater="about-news.php" post_type="post" posts_per_page="8" category="usa-ultimate" scroll="false" transition_container="false" button_label="Load More" button_loading_label="Loading"]';
    echo do_shortcode($shortcode);
?>