<div class="overview">
    <div class="photo">
        <div class="photo-wrapper">
            <div class="content">
                <img src="<?php $image = get_field('header_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
            </div>
        </div>
    </div>
    
    <div class="headline section-header blue underline">
        <h4><?php echo get_field('section_header'); ?></h4>
    </div>

    <div class="copy p2 extended">
        <?php echo get_field('overview'); ?>
    </div>
</div>

<div class="mission">
    <div class="headline underline white">
        <h5>Our Mission</h5>
    </div>
    
    <div class="copy p2 extended">
        <?php echo get_field('mission'); ?>
    </div>   
</div>

<div class="vision">
    <div class="headline underline white">
        <h5>Our Vision</h5>
    </div>

    <div class="copy p2 extended">
        <?php echo get_field('vision'); ?>
    </div>   
</div>

<div class="organization">
    <div class="headline underline blue">
        <h4><?php echo get_field('organization_headline'); ?></h4>
    </div>

    <div class="copy p2 extended">
        <?php echo get_field('organization_copy'); ?>
    </div>

    <div class="graphic">
    <img src="<?php $image = get_field('organization_graphic'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
    </div>
</div>

<div class="contact-info">
    <div class="section-header headline blue underline">
        <h4>Contact Us</h4>
    </div>
    
    <div class="photo">
        <img src="<?php $image = get_field('location_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
    </div>

    <div class="info">
        <div class="name">
            <img src="<?php $image = get_field('usa_ultimate_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
        </div>

        <div class="basic p3">

            <div class="address">
                <p><?php echo get_field('address'); ?></p>
            </div>

            <div class="phone">
                <p><?php echo get_field('phone'); ?></p>
                <p><?php echo get_field('toll_free_phone'); ?></p>
            </div>

            <div class="email">
                <p><a href="mailto:<?php echo get_field('email'); ?>"><?php echo get_field('email'); ?></a></p>
            </div>	
            
        </div>

        <div class="hours p3">
            <h4>Hours of Operation</h4>
            
                <?php if(have_rows('hours')): while(have_rows('hours')): the_row(); ?>
                    
                    <div class="entry">
                        <div class="day"><strong><?php echo get_sub_field('day'); ?></strong></div>
                        <div class="time"><p><?php echo get_sub_field('time'); ?></p></div>
                    </div>

                <?php endwhile; endif; ?>

            <div class="note">
                <p><?php echo get_field('hours_note'); ?></p>
            </div>

        </div>
    </div>
</div>