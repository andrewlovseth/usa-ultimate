<?php
	$args = wp_parse_args($args);
	$file = $args['file'];
	$file_name = $file['title'];
	$file_url = $file['url'];
	$file_data_modified = $file['modified'];
?>

<div class="resource">	
	<div class="headline blue">
		<h5><a href="<?php echo $file_url; ?>" target="_window"><?php echo $file_name; ?></a></h5>
	</div>
	
	<div class="url p3">
		<a href="<?php echo $file_url; ?>" target="_window">View PDF</a>
	</div>

	<div class="meta p4">
		<em>Last Modified: <?php echo $file_data_modified; ?></em>
	</div>
</div>

