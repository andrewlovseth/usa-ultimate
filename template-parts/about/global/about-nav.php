<nav class="about-nav">
	<div class="nav-wrapper">

		<div class="nav-header">
			<div class="logo">
				<a href="<?php echo site_url('/about/'); ?>" class="usau-logo">
					<img src="<?php $image = get_field('usa_ultimate_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>
			</div>

			<div class="toggle">
            	<a href="#" class="about-nav-trigger"><span>Menu</span></a>
        	</div>		
		</div>       

		<?php if(have_rows('about_menu', 'options')): while(have_rows('about_menu', 'options')) : the_row(); ?>
		 
		    <?php if( get_row_layout() == 'group' ): ?>

				<div class="links">
					<?php if(have_rows('links')): while(have_rows('links')): the_row(); ?>

						<?php $link = get_sub_field('link'); if( $link ): 
						    $link_url = $link['url'];
						    $link_title = $link['title'];
						    $link_target = $link['target'] ? $link['target'] : '_self';
						    $url = parse_url($link_url);
                            $path = $url['path'];                            
						?>
						    
						    	<div class="link">
						    		<a href="<?php echo esc_url( $path ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>	
						    	</div>

						<?php endif; ?>

					<?php endwhile; endif; ?>	
				</div>
				
		    <?php endif; ?>
		 
		<?php endwhile; endif; ?>	
		
	</div>
</nav>