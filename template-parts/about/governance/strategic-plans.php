<?php if(have_rows('strategic_plans')): ?>
    <section class="strategic-plans files">

        <div class="headline section-header blue underline">
            <h3>Strategic Plans</h3>
        </div>

        <div class="file-grid">
            <?php while(have_rows('strategic_plans')): the_row(); ?>

                <?php 
                    $file = get_sub_field('file');
                    $args = ['file' => $file];
                    get_template_part('template-parts/about/global/file-grid', null, $args);
                ?>

            <?php endwhile; ?>	
        </div>
    </section>
<?php endif; ?>