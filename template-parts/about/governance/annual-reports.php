<?php if(have_rows('annual_reports')): ?>
    <section class="annual-reports files">

        <div class="headline section-header blue underline">
            <h3>Annual Reports</h3>
        </div>

        <div class="file-grid">
            <?php while(have_rows('annual_reports')): the_row(); ?>

                <?php 
                    $file = get_sub_field('file');
                    $args = ['file' => $file];
                    get_template_part('template-parts/about/global/file-grid', null, $args);
                ?>

            <?php endwhile; ?>	
        </div>
    </section>
<?php endif; ?>