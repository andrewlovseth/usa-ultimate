<section class="suspensions files">

    <div class="headline section-header blue underline">
        <h3>Suspensions and Bans</h3>
    </div>    

    <div class="cta">
        <a href="<?php echo site_url('/about/suspensions/'); ?>" class="btn small blue">Current Member Suspensions</a>
    </div>
</section>