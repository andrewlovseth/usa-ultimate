<?php if(have_rows('audited_financial_statements')): ?>
    <section class="audited-financial-statements files">
        
        <div class="headline section-header blue underline">
            <h3>Audited Financial Statements</h3>
        </div>
        
        <div class="file-grid">
            <?php while(have_rows('audited_financial_statements')): the_row(); ?>

                <?php 
                    $file = get_sub_field('file');
                    $args = ['file' => $file];
                    get_template_part('template-parts/about/global/file-grid', null, $args);
                ?>

            <?php endwhile; ?>	
        </div>
    </section>
<?php endif; ?>