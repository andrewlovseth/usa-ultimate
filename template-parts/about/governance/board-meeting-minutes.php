<?php if(have_rows('board_meeting_minutes')): ?>
    <section class="board-meeting-minutes files">

        <div class="headline section-header blue underline">
            <h3>Board Meeting Minutes</h3>
        </div>
        
        <div class="file-grid">
            <?php while(have_rows('board_meeting_minutes')): the_row(); ?>

                <?php 
                    $file = get_sub_field('file');
                    $args = ['file' => $file];
                    get_template_part('template-parts/about/global/file-grid', null, $args);
                ?>

            <?php endwhile; ?>	
        </div>
    </section>
<?php endif; ?>