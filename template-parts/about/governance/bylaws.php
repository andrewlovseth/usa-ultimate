<?php if(have_rows('bylaws')): ?>
    <section class="bylaws files">

        <div class="headline section-header blue underline">
            <h3>Bylaws</h3>
        </div>

        <div class="file-grid">
            <?php while(have_rows('bylaws')): the_row(); ?>

                <?php 
                    $file = get_sub_field('file');
                    $args = ['file' => $file];
                    get_template_part('template-parts/about/global/file-grid', null, $args);
                ?>

            <?php endwhile; ?>	
        </div>
    </section>
<?php endif; ?>