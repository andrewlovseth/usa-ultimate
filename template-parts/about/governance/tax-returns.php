<?php if(have_rows('tax_returns')): ?>
    <section class="tax-returns files">

        <div class="headline section-header blue underline">
            <h3>Tax Returns</h3>
        </div>
        
        <div class="file-grid">
            <?php while(have_rows('tax_returns')): the_row(); ?>

                <?php 
                    $file = get_sub_field('file');
                    $args = ['file' => $file];
                    get_template_part('template-parts/about/global/file-grid', null, $args);
                ?>

            <?php endwhile; ?>	
        </div>
    </section>
<?php endif; ?>