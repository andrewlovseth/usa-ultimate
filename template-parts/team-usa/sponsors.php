<?php if(is_page('17') || is_descendent_of('team-usa') || is_singular( 'world_games_players' )): ?>
	<section class="sponsors team-usa-sponsors usau-block">
		<div class="section-header headline white align-center underline uppercase">
			<h4>Team USA Sponsors</h4>
		</div>

		<div class="sponsor-gallery">
			<?php if(have_rows('team_usa_sponsors', 'options')): while(have_rows('team_usa_sponsors', 'options')): the_row(); ?>
	
				<div class="sponsor">
					<a href="<?php echo get_sub_field('link'); ?>" rel="external">
						<img src="<?php $image = get_sub_field('logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</a>
				</div>
			<?php endwhile; endif; ?>
		</div>
	</section>
<?php endif; ?>