<?php if(have_rows($event_value . '_archive', 'options')): ?>

	<section class="team-usa-archive usau-block">

		<div class="event-header headline red align-center">
			<h4><?php echo $event_display_value; ?> Results</h4>
		</div>

		<div class="grid two-col count-<?php $count = count(get_field($event_value . '_archive', 'options')); echo $count; ?>">

			<?php while(have_rows($event_value . '_archive', 'options')) : the_row(); ?>
			
				<?php if( get_row_layout() == 'division' ): ?>

					<div class="division">
						<div class="division-header">
							<h4><?php echo get_sub_field('division_label'); ?></h4>
						</div>

						<?php $teams = get_sub_field('teams'); if( $teams ): ?>
							<table class="results">
								<thead>
									<tr>
										<th class="year">
											<strong>Year</strong>
										</th>

										<th class="location">
											<strong>Location</strong>
										</th>

										<th class="record">
											<strong>Record</strong>
										</th>

										<th class="finish">
											<strong>Finish</strong>
										</th>

										<th class="details">
											<strong>Details</strong>
										</th>		
									</tr>
								</thead>

								<tbody>		    				
									<?php foreach( $teams as $team ): ?>

										<?php
											$content_post = get_post($team);
											$post_blocks = parse_blocks( $content_post->post_content );
											
											//echo '<pre class="dump">' . var_dump($post_blocks) . '</pre>';

											foreach( $post_blocks as $block ) {
												if( 'acf/team-usa-roster'  == $block['blockName'] || 'acf/google-sheet-roster' == $block['blockName'] ) {
													$usa_block = $block;

													//echo '<pre class="dump">' , var_dump($usa_block['attrs']) , '</pre>';

													if( !empty( $usa_block['attrs']['data']['year'] ) ) {
														$year = $block['attrs']['data']['year'];
													}
		
													if( !empty( $usa_block['attrs']['data']['location'] ) ) {
														$location = $block['attrs']['data']['location'];
													}
		
													if( !empty( $usa_block['attrs']['data']['record'] ) ) {
														$record = $block['attrs']['data']['record'];
													} else {
														$record = NULL;
													}		
		
													if( !empty( $usa_block['attrs']['data']['finish'] ) ) {
														$finish = $block['attrs']['data']['finish'];
													}
		
													if( !empty( $usa_block['attrs']['data']['medal'] ) ) {
														$medal = $block['attrs']['data']['medal'];
													}
												}									
											}
										?>

											<tr>
												<td class="year">
													<p><?php echo $year; ?></p>
												</td>

												<td class="location">
													<p><?php echo $location; ?></p>
												</td>	

												<td class="record">
													<p><?php echo $record; ?></p>
												</td>

												<td class="finish">
													<div class="finish-flex">
														<?php if(!empty( $usa_block['attrs']['data']['medal'] )): ?>
															<div class="medal">
																<img src="<?php bloginfo('template_directory'); ?>/images/<?php echo $medal; ?>.svg" alt="<?php echo get_field('medal'); ?>">
															</div>
														<?php endif; ?>

														<?php if(!empty( $usa_block['attrs']['data']['finish'] )): ?>
															<p><span class="medal gold"><?php echo $finish; ?></span></p>
														<?php endif; ?>
													</div>
												</td>

												<td class="details">
													<p><a href="<?php echo get_permalink($team); ?>">View</a></p>
												</td>

											</tr>

									<?php unset($year, $location, $record, $finish, $medal); endforeach; ?>
								</tbody>								    			
							</table>

						<?php endif; ?>

					</div>
					
				<?php endif; ?>
			
			<?php endwhile; ?>
			
		</div>
	
	</section>

<?php endif; ?>