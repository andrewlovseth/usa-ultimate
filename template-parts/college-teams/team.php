<div class="grid-item team team-trigger" data-team="#<?php echo $p->post_name; ?>">
	<div class="logo">
		<img src="<?php $image = get_field('logo', $p->ID); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
	</div>

	<div class="info">
		<div class="headline red">
			<h4><?php echo get_the_title($p->ID); ?></h4>
		</div>

		<div class="meta p3">
			<p class="school"><?php echo get_field('school', $p->ID); ?></p>
		</div>
	</div>

	<div class="team-overlay" id="<?php echo $p->post_name; ?>">
		<div class="overlay">
			<div class="overlay-wrapper">
				
				<div class="info">
					<div class="close">
						<a href="#" class="team-close-btn close-btn"></a>
					</div>

					<div class="photo">
						<div class="content">
							<img src="<?php $image = get_field('photo', $p->ID); echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
						</div>
					</div>

					<div class="details">
						<div class="meta meta-main">
							<div class="names">
					    		<h3><?php echo get_the_title($p->ID); ?></em></h3>
					    		<h4><?php echo get_field('location', $p->ID); ?></h4>
							</div>

							<?php if(get_field('twitter', $p->ID) || get_field('facebook', $p->ID) || get_field('instagram', $p->ID) || get_field('website', $p->ID)): ?>
								<div class="social">
									<?php if(get_field('twitter', $p->ID)): ?>
										<a href="<?php echo get_field('twitter', $p->ID); ?>" rel="external">
											<img src="<?php bloginfo('template_directory'); ?>/images/icon-twitter-red.svg" alt="Twitter" />
										</a>
									<?php endif; ?>

									<?php if(get_field('facebook', $p->ID)): ?>
										<a href="<?php echo get_field('facebook', $p->ID); ?>" rel="external">
											<img src="<?php bloginfo('template_directory'); ?>/images/icon-facebook-red.svg" alt="Facebook" />
										</a>
									<?php endif; ?>

									<?php if(get_field('instagram', $p->ID)): ?>
										<a href="<?php echo get_field('instagram', $p->ID); ?>" rel="external">
											<img src="<?php bloginfo('template_directory'); ?>/images/icon-instagram-red.svg" alt="Instagram" />
										</a>
									<?php endif; ?>

									<?php if(get_field('website', $p->ID)): ?>
										<a href="<?php echo get_field('website', $p->ID); ?>" rel="external">
											<img src="<?php bloginfo('template_directory'); ?>/images/icon-link-red.svg" alt="Website" />
										</a>
									<?php endif; ?>
								</div>

							<?php endif; ?>

						</div>

						<?php if(get_field('profile', $p->ID)): ?>
							<div class="copy p3">
								<?php echo get_field('profile', $p->ID); ?>
							</div>
						<?php endif; ?>

						<?php if(get_field('year_founded', $p->ID)): ?>
							<div class="detail year-founded">
								<h5>Year Founded</h5>
								<p><?php echo get_field('year_founded', $p->ID); ?></p>
							</div>
						<?php endif; ?>

						<?php if(get_field('championships', $p->ID)): ?>
							<div class="detail championships">
								<h5>Championships</h5>
								<p><?php echo get_field('championships', $p->ID); ?></p>
							</div>
						<?php elseif(get_field('best_finish', $p->ID)): ?>
							<div class="detail best-finsish">
								<h5>Best Finish</h5>
								<p><?php echo get_field('best_finish', $p->ID); ?></p>
							</div>
						<?php endif; ?>

						<?php if(get_field('championship_appearances', $p->ID)): ?>
							<div class="detail championship-appearances">
								<h5>Championship Appearances</h5>
								<p><?php echo get_field('championship_appearances', $p->ID); ?></p>
							</div>
						<?php endif; ?>

						<?php if(get_field('callahan_award_winners', $p->ID)): ?>
							<div class="detail callahans">
								<h5>Callahan Award Winners</h5>
								<p><?php echo get_field('callahan_award_winners', $p->ID); ?></p>
							</div>
						<?php endif; ?>

						<?php if(get_field('notable_alumni', $p->ID)): ?>
							<div class="detail notable-alumni">
								<h5>Notable Alumni</h5>
								<p><?php echo get_field('notable_alumni', $p->ID); ?></p>
							</div>
						<?php endif; ?>

						<?php if(get_field('usau_link', $p->ID)): ?>
							<div class="cta usua-link">
								<a href="<?php echo get_field('usau_link', $p->ID); ?>" class="btn solid-red small" rel="external">Roster & Schedule</a>
							</div>
						<?php endif; ?>


					</div>

				</div>

			</div>
		</div>
	</div>

</div>