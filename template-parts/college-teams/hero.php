<section class="hero-photo">
	<div class="photo">
		<div class="content">
			<img src="<?php $image = get_field('college_teams_hero_photo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

			<div class="info">
				<div class="usau-block">
					<?php if(get_field('college_teams_hero_sub_headline', 'options')): ?>
						<div class="sub-headline">
							<h2><?php echo get_field('college_teams_hero_sub_headline', 'options'); ?></h2>
						</div>
					<?php endif; ?>
					<div class="headline">
						<h1 class="cover-title"><?php echo get_field('college_teams_hero_headline', 'options'); ?></h1>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>