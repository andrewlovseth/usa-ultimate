		<?php get_template_part('template-parts/team-usa/sponsors'); ?>

		<?php get_template_part('template-parts/footer/back-to-top'); ?>

	</main>

	<footer class="site-footer usau-block">

		<?php get_template_part('template-parts/footer/columns'); ?>

		<?php get_template_part('template-parts/footer/utilities'); ?>

	</footer>

	<?php get_template_part('template-parts/footer/legacy'); ?>

</div>

<?php get_template_part('template-parts/global/video-overlay'); ?>

<?php echo get_field('body_bottom_code', 'options'); ?>

<?php wp_footer(); ?>

</body>
</html>