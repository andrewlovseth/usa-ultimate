<?php

/*
	Template Name: News Archive

*/

get_header(); ?>

	<section class="archived-posts usau-block">
		<div class="section-header headline blue align-center underline">
			<h3>News Archive</h3>
		</div>		
	</section>

	<section class="archive-list cat-list usau-block">
		<div class="headline red section-header underline">
			<h4>Topics</h4>
		</div>

		<div class="link-list grid four-col-grid">
			<?php 
				$args = array(
					'orderby' => 'title',
					'order' => 'ASC',
					'hide_empty' => true,
					'number' => 100
				);
				$cats = get_categories($args);
				foreach ( $cats as $cat ): ?>

					<div class="link">
						<a href="<?php echo get_category_link( $cat->term_id ); ?>"><?php echo $cat->name; ?></a>
					</div>

			<?php endforeach; ?>
		</div>
	</section>

	<section class="archive-list date-list usau-block">		
		<div class="headline red section-header underline">
			<h4>Date</h4>
		</div>

		<?php $years = $wpdb->get_col("SELECT DISTINCT YEAR(post_date) FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = 'post' ORDER BY post_date DESC");
			foreach($years as $year) : ?>
		
			<div class="year">
				<div class="year-header">
					<h4><a href="<?php echo get_year_link($year); ?>"><?php echo $year; ?></a></h4>
				</div>

				<div class="month grid four-col-grid">
					<?php $months = $wpdb->get_col("SELECT DISTINCT MONTH(post_date) FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = 'post' AND YEAR(post_date) = '".$year."' ORDER BY post_date ASC");
						foreach($months as $month) : ?>

							<div class="month-link">
								<a href="<?php echo get_month_link($year, $month); ?>"><?php echo date( 'F', mktime(0, 0, 0, $month) );?></a>
							</div>

					<?php endforeach;?>
				</div>
			</div>				
		<?php endforeach; ?>
	</section>

<?php get_footer(); ?>