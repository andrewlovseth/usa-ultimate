<?php

$search_term = get_search_query();


get_header(); ?>

	<section class="archived-posts search-results usau-block">

		<div class="section-header headline blue align-center underline">
			<h3>Search Results: <?php echo $search_term; ?></h3>
		</div>
	
		<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
			<label>
				<input type="search" class="search-field"
					data-swplive="true"
					placeholder="<?php echo esc_attr_x( '(e.g. "rankings" or "schedule")', 'placeholder' ) ?>"
					value="<?php echo get_search_query() ?>" name="s"
					title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
			</label>
			<input type="submit" class="search-submit"
				value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
		</form>

		<div class="search-results-list">			
			<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

				<?php if(get_post_type() == 'post' ): ?>

					<?php get_template_part('template-parts/search/post-result'); ?>

				<?php elseif(get_post_type() == 'page' ): ?>

					<?php get_template_part('template-parts/search/page-result'); ?>

				<?php elseif(get_post_type() == 'videos' ): ?>
					
					<?php get_template_part('template-parts/search/video-result'); ?>

				<?php elseif(get_post_type() == 'resources' ): ?>
					
					<?php get_template_part('template-parts/search/resource-result'); ?>

				<?php elseif(get_post_type() == 'world_games_players' ): ?>
					
					<?php get_template_part('template-parts/search/world-games-player-result'); ?>

				<?php elseif(get_post_type() == 'club_teams' ||  get_post_type() == 'college_teams'): ?>

					<?php get_template_part('template-parts/search/team-result'); ?>

				<?php else: ?>

					<?php get_template_part('template-parts/search/generic-result'); ?>

				<?php endif; ?>

			<?php endwhile; endif; ?>

			<div class="pagination">
				<?php
					global $wp_query;
						
					$big = 999999999; // need an unlikely integer
						
					echo paginate_links( array(
						'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
						'format' => '?paged=%#%',
						'current' => max( 1, get_query_var('paged') ),
						'total' => $wp_query->max_num_pages
					) );
				?>
			</div>
		</div>

	</section>

<?php get_footer(); ?>