<?php get_header(); ?>

	<?php get_template_part('template-parts/news/subnav'); ?>

	<?php get_template_part('template-parts/news/featured-posts'); ?>

	<?php get_template_part('template-parts/news/archived-posts'); ?>

<?php get_footer(); ?>