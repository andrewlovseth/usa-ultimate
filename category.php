<?php

	$category_name = get_queried_object()->name;
	$category_slug = get_queried_object()->slug;

	get_header();

?>

	<?php get_template_part('template-parts/news/subnav'); ?>

	<section class="archived-posts usau-block">

		<div class="headline blue align-center underline archive-header">
			<h2><?php echo $category_name; ?></h2>
		</div>
		
		<?php echo do_shortcode('[ajax_load_more id="news-archive" theme_repeater="news.php" post_type="post" category="' . $category_slug . '" posts_per_page="10" scroll="false" no_results_text="<aside>Sorry, nothing found.</aside>" button_label="Load More"]'); ?>

	</section>

<?php get_footer(); ?>