<?php

	$author_name = get_queried_object()->post_title;
	$author_id = get_queried_object()->ID;

	get_header();

?>

	<?php get_template_part('template-parts/news/subnav'); ?>

	<section class="archived-posts usau-block">

		<div class="section-header">
			<?php if(get_field('photo', $author_id)): ?>
				<div class="photo">
					<img src="<?php $image = get_field('photo', $author_id); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>
			<?php endif; ?>

			<div class="headline align-center blue">
				<h4>Posts by <?php echo $author_name; ?></h4>
			</div>			
		</div>
		
		<?php echo do_shortcode('[ajax_load_more id="news-archive" theme_repeater="news.php" post_type="post" meta_key="authors" meta_compare="LIKE" meta_value="' . $author_id . '" posts_per_page="10" scroll="false" no_results_text="<aside>Sorry, nothing found.</aside>" button_label="Load More"]'); ?>

	</section>

<?php get_footer(); ?>