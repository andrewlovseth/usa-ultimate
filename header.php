<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<?php echo get_field('head_code', 'options'); ?>

	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php echo get_field('body_top_code', 'options'); ?>
<?php wp_body_open(); ?>

<div id="site">
	
	<header class="site-header">
		<?php get_template_part('template-parts/header/utility-menu'); ?>

		<nav class="main-menu">
			<?php get_template_part('template-parts/header/site-logo'); ?>

			<?php get_template_part('template-parts/header/desktop-menu'); ?>

			<?php get_template_part('template-parts/header/hamburger'); ?>

			<?php get_template_part('template-parts/header/discover-ultimate-link'); ?>
	
		</nav>

		<?php get_template_part('template-parts/header/search-overlay'); ?>

	</header>

	<?php get_template_part('template-parts/header/hamburger-menu'); ?>

	<main class="site-content">