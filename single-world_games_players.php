<?php get_header(); ?>

	<?php get_template_part('template-parts/single-world-games-player/hero'); ?>

	<?php get_template_part('template-parts/single-world-games-player/player-info'); ?>

	<?php get_template_part('template-parts/single-world-games-player/player-gallery'); ?>

<?php get_footer(); ?>