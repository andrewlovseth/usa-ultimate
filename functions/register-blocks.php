<?php

/*
    Register Blocks
*/

add_action('acf/init', 'my_register_blocks');
function my_register_blocks() {

    if( function_exists('acf_register_block_type') ) {

        // HERO VIDEO
        acf_register_block_type(array(
            'name'              => 'hero-video',
            'title'             => __('USAU Hero Video'),
            'description'       => __('Hero Video block.'),
            'render_template'   => 'blocks/hero-video/hero-video.php',
            'category'          => 'layout',
            'icon'              => 'video-alt3',
            'align'             => 'full',
        ));

        // HERO PHOTO
        acf_register_block_type(array(
            'name'              => 'hero-photo',
            'title'             => __('USAU Hero Photo'),
            'description'       => __('Hero Photo block.'),
            'render_template'   => 'blocks/hero-photo/hero-photo.php',
            'category'          => 'layout',
            'icon'              => 'format-image',
            'align'             => 'full',
        ));

        // COVER
        acf_register_block_type(array(
            'name'              => 'cover',
            'title'             => __('USAU / Cover'),
            'description'       => __('Cover block.'),
            'render_template'   => 'blocks/cover/cover.php',
            'category'          => 'layout',
            'icon'              => 'id-alt',
            'align'             => 'full',
        ));

        // TWO COL FEATURES
        acf_register_block_type(array(
            'name'              => 'two-col-features',
            'title'             => __('USAU / Two Col Features'),
            'description'       => __('Two Col Features block.'),
            'render_template'   => 'blocks/two-col-features/two-col-features.php',
            'category'          => 'layout',
            'icon'              => 'forms',
            'align'             => 'full',
        ));

        // HUB NAVIGATION
        acf_register_block_type(array(
            'name'              => 'hub-navigation',
            'title'             => __('USAU / Hub Navigation'),
            'description'       => __('Hub Navigation block.'),
            'render_template'   => 'blocks/hub-navigation/hub-navigation.php',
            'category'          => 'layout',
            'icon'              => 'grid-view',
            'align'             => 'full',
        ));

        // THREE COLUMNS
        acf_register_block_type(array(
            'name'              => 'three-columns',
            'title'             => __('USAU / Three Columns'),
            'description'       => __('Three Col Features block.'),
            'render_template'   => 'blocks/three-columns/three-columns.php',
            'category'          => 'layout',
            'icon'              => 'grid-view',
            'align'             => 'full',
        ));

        // PAGE HEADER
        acf_register_block_type(array(
            'name'              => 'page-header',
            'title'             => __('USAU / Page Header'),
            'description'       => __('Page Header block.'),
            'render_template'   => 'blocks/page-header/page-header.php',
            'category'          => 'layout',
            'icon'              => 'editor-kitchensink',
            'align'             => 'full',
        ));

        // FULL WIDTH AD
        acf_register_block_type(array(
            'name'              => 'full-width-ad',
            'title'             => __('USAU / Full Width Ad'),
            'description'       => __('Full Width Ad block.'),
            'render_template'   => 'blocks/full-width-ad/full-width-ad.php',
            'category'          => 'layout',
            'icon'              => 'money',
            'align'             => 'full',
        ));

        // CTA
        acf_register_block_type(array(
            'name'              => 'cta',
            'title'             => __('USAU / CTA'),
            'description'       => __('Full Width Ad block.'),
            'render_template'   => 'blocks/cta/cta.php',
            'category'          => 'layout',
            'icon'              => 'plus',
            'align'             => 'full',
        ));

        // LATEST NEWS
        acf_register_block_type(array(
            'name'              => 'latest-news',
            'title'             => __('USAU / Latest News'),
            'description'       => __('Latest News block.'),
            'render_template'   => 'blocks/latest-news/latest-news.php',
            'category'          => 'layout',
            'icon'              => 'rss',
            'align'             => 'full',
        ));

        // SOCIAL GRID
        acf_register_block_type(array(
            'name'              => 'social-grid',
            'title'             => __('USAU / Social Grid'),
            'description'       => __('Social Grid block.'),
            'render_template'   => 'blocks/social-grid/social-grid.php',
            'category'          => 'share',
            'icon'              => 'rss',
            'align'             => 'full',
        ));

        // RANKINGS
        acf_register_block_type(array(
            'name'              => 'rankings',
            'title'             => __('USAU / Rankings'),
            'description'       => __('Rankings block.'),
            'render_template'   => 'blocks/rankings/rankings.php',
            'category'          => 'share',
            'icon'              => 'rss',
            'align'             => 'full',
        ));

        // GOOGLE SHEET SCHEDULE
        acf_register_block_type(array(
            'name'              => 'google-sheet-schedule',
            'title'             => __('USAU / Season Schedule'),
            'description'       => __('Season Schedule block.'),
            'render_template'   => 'blocks/season-schedule/season-schedule.php',
            'category'          => 'share',
            'icon'              => 'rss',
            'align'             => 'full',
        ));

        // SCOREBOARD
        acf_register_block_type(array(
            'name'              => 'scoreboard',
            'title'             => __('USAU / Scoreboard'),
            'description'       => __('Scoreboard block.'),
            'render_template'   => 'blocks/scoreboard/scoreboard.php',
            'category'          => 'share',
            'icon'              => 'rss',
            'align'             => 'full',
        ));

        // INFO WITH COUNTER
        acf_register_block_type(array(
            'name'              => 'info-with-counter',
            'title'             => __('USAU / Info with Counter'),
            'description'       => __('Info with Counter block.'),
            'render_template'   => 'blocks/info-with-counter/info-with-counter.php',
            'category'          => 'share',
            'icon'              => 'rss',
            'align'             => 'full',
        ));

        // MEDIA CAROUSEL
        acf_register_block_type(array(
            'name'              => 'media-carousel',
            'title'             => __('USAU / Media Carousel'),
            'description'       => __('Media Carousel block.'),
            'render_template'   => 'blocks/media-carousel/media-carousel.php',
            'category'          => 'share',
            'icon'              => 'rss',
            'align'             => 'full',
        ));

        // GOOGLE SHEETS ROSTER (AKA TEAM USA ARCHIVE)
        acf_register_block_type(array(
            'name'              => 'google-sheet-roster',
            'title'             => __('USAU / Team USA Archive'),
            'description'       => __('Team USA Archive block.'),
            'render_template'   => 'blocks/team-usa-archive/team-usa-archive.php',
            'category'          => 'share',
            'icon'              => 'rss',
            'align'             => 'full',
        ));

        // TEAM USA EXPLORE TEAMS
        acf_register_block_type(array(
            'name'              => 'team-usa-explore-teams',
            'title'             => __('USAU / Team USA Explore Teams'),
            'description'       => __('Team USA Explore Teams block.'),
            'render_template'   => 'blocks/team-usa-explore-teams/team-usa-explore-teams.php',
            'category'          => 'share',
            'icon'              => 'rss',
            'align'             => 'full',
        ));

        // TEAM USA ROSTER
        acf_register_block_type(array(
            'name'              => 'team-usa-roster',
            'title'             => __('USAU / Team USA Roster'),
            'description'       => __('Team USA Roster block.'),
            'render_template'   => 'blocks/team-usa-roster/team-usa-roster.php',
            'category'          => 'share',
            'icon'              => 'rss',
            'align'             => 'full',
        ));

        // TEAM USA RESULTS ARCHIVE
        acf_register_block_type(array(
            'name'              => 'team-usa-results-archive',
            'title'             => __('USAU / Team USA Results Archive'),
            'description'       => __('Team USA Results Archive block.'),
            'render_template'   => 'blocks/team-usa-results-archive/team-usa-results-archive.php',
            'category'          => 'share',
            'icon'              => 'rss',
            'align'             => 'full',
        ));

        // RESOURCES
        acf_register_block_type(array(
            'name'              => 'resources',
            'title'             => __('USAU / Resources'),
            'description'       => __('Resources block.'),
            'render_template'   => 'blocks/resources/resources.php',
            'category'          => 'share',
            'icon'              => 'rss',
            'align'             => 'full',
        ));


        // FEATURED VIDEOS
        acf_register_block_type(array(
            'name'              => 'featured-videos',
            'title'             => __('USAU / Featured Videos'),
            'description'       => __('Featured Videos block.'),
            'render_template'   => 'blocks/featured-videos/featured-videos.php',
            'category'          => 'share',
            'icon'              => 'rss',
            'align'             => 'full',
        ));

        // UPCOMING EVENTS
        acf_register_block_type(array(
            'name'              => 'upcoming-events',
            'title'             => __('USAU / Upcoming Events'),
            'description'       => __('Upcoming Events block.'),
            'render_template'   => 'blocks/upcoming-events/upcoming-events.php',
            'category'          => 'share',
            'icon'              => 'rss',
            'align'             => 'full',
        ));

        // AWARDS
        acf_register_block_type(array(
            'name'              => 'awards',
            'title'             => __('USAU / Awards'),
            'description'       => __('Awards block.'),
            'render_template'   => 'blocks/awards/awards.php',
            'category'          => 'share',
            'icon'              => 'rss',
            'align'             => 'full',
        ));

        // ANCHOR LINKS
        acf_register_block_type(array(
            'name'              => 'anchor-links',
            'title'             => __('USAU / Anchor Links'),
            'description'       => __('Anchor Links block.'),
            'render_template'   => 'blocks/anchor-links/anchor-links.php',
            'category'          => 'share',
            'icon'              => 'rss',
            'align'             => 'full',
        ));

        // ROSTER LOGS
        acf_register_block_type(array(
            'name'              => 'roster-logs',
            'title'             => __('USAU / Roster Logs'),
            'description'       => __('Roster Logs block.'),
            'render_template'   => 'blocks/roster-logs/roster-logs.php',
            'category'          => 'share',
            'icon'              => 'rss',
            'align'             => 'full',
        ));

        // COUNTDOWN
        acf_register_block_type(array(
            'name'              => 'countdown',
            'title'             => __('USAU / Countdown'),
            'description'       => __('Countdown block.'),
            'render_template'   => 'blocks/countdown/countdown.php',
            'category'          => 'share',
            'icon'              => 'rss',
            'align'             => 'full',
        ));

        // CONTACTS
        acf_register_block_type(array(
            'name'              => 'contacts',
            'title'             => __('USAU / Contacts'),
            'description'       => __('Contacts block.'),
            'render_template'   => 'blocks/contacts/contacts.php',
            'category'          => 'share',
            'icon'              => 'rss',
            'align'             => 'full',
        ));

        // COVER SIDE BY SIDE
        acf_register_block_type(array(
            'name'              => 'cover-side-by-side',
            'title'             => __('USAU / Cover Side by Side'),
            'description'       => __('Cover Side by Side block.'),
            'render_template'   => 'blocks/cover-side-by-side/cover-side-by-side.php',
            'category'          => 'share',
            'icon'              => 'rss',
            'align'             => 'full',
        ));

        // MEMBERSHIP OPTIONS
        acf_register_block_type(array(
            'name'              => 'membership-options',
            'title'             => __('USAU / Membership Options'),
            'description'       => __('Membership Options block.'),
            'render_template'   => 'blocks/membership-options/membership-options.php',
            'category'          => 'share',
            'icon'              => 'rss',
            'align'             => 'full',
        ));


        // LOCAL MAP
        acf_register_block_type(array(
            'name'              => 'local-map',
            'title'             => __('USAU / Local Map'),
            'description'       => __('Local Map block.'),
            'render_template'   => 'blocks/local-map/local-map.php',
            'category'          => 'share',
            'icon'              => 'rss',
            'align'             => 'full',
        ));


        // EDI
        acf_register_block_type(array(
            'name'              => 'edi',
            'title'             => __('USAU / EDI'),
            'description'       => __('EDI block.'),
            'render_template'   => 'blocks/edi/edi.php',
            'category'          => 'share',
            'icon'              => 'rss',
            'align'             => 'full',
        ));


        // TEAM USA PLAYER CAROUSEL
        acf_register_block_type(array(
            'name'              => 'team-usa-player-carousel',
            'title'             => __('USAU / Team USA Player Carousel'),
            'description'       => __('Team USA Player Carousel block.'),
            'render_template'   => 'blocks/team-usa-player-carousel/team-usa-player-carousel.php',
            'category'          => 'share',
            'icon'              => 'rss',
            'align'             => 'full',
        ));

        // FAQs
        acf_register_block_type(array(
            'name'              => 'faqs',
            'title'             => __('USAU / FAQs'),
            'description'       => __('FAQs block.'),
            'render_template'   => 'blocks/faqs/faqs.php',
            'category'          => 'share',
            'icon'              => 'rss',
            'align'             => 'full',
        ));


        // ANCHOR
        acf_register_block_type(array(
            'name'              => 'anchor',
            'title'             => __('USAU / Anchor'),
            'description'       => __('Anchor block.'),
            'render_template'   => 'blocks/anchor/anchor.php',
            'category'          => 'share',
            'icon'              => 'rss',
            'align'             => 'full',
        ));

        // MONTHLY CALENDAR
        acf_register_block_type(array(
            'name'              => 'monthly-calendar',
            'title'             => __('USAU / Monthly Calendar'),
            'description'       => __('Monthly Calendar block.'),
            'render_template'   => 'blocks/monthly-calendar/monthly-calendar.php',
            'category'          => 'share',
            'icon'              => 'rss',
            'align'             => 'full',
        ));


        // YEARLY CALENDAR
        acf_register_block_type(array(
            'name'              => 'yearly-calendar',
            'title'             => __('USAU / Yearly Calendar'),
            'description'       => __('Yearly Calendar block.'),
            'render_template'   => 'blocks/yearly-calendar/yearly-calendar.php',
            'category'          => 'share',
            'icon'              => 'rss',
            'align'             => 'full',
        ));


        // USAU SECTION
        acf_register_block_type(array(
            'name'              => 'usau-section',
            'title'             => __('USAU / Section'),
            'description'       => __('USAU Section block.'),
            'render_template'   => 'blocks/usau-section/usau-section.php',
            'category'          => 'share',
            'icon'              => 'rss',
            'align'             => 'full',
            'supports'          => array(
                'align' => true,
                'mode' => false,
                'jsx' => true
            ),
        ));

        // HTML
        acf_register_block_type(array(
            'name'              => 'usau-html',
            'title'             => __('USAU / HTML'),
            'description'       => __('USAU HTML block.'),
            'render_template'   => 'blocks/html/html.php',
            'category'          => 'share',
            'icon'              => 'rss',
            'align'             => 'full',
        ));

        // STREAMING SCHEDULE
        acf_register_block_type(array(
            'name'              => 'streaming-schedule',
            'title'             => __('USAU / Streaming Schedule'),
            'description'       => __('Streaming Schedule block.'),
            'render_template'   => 'blocks/streaming-schedule/streaming-schedule.php',
            'category'          => 'share',
            'icon'              => 'rss',
            'align'             => 'full',
        ));

        // THREE COLUMN FEATURES
        acf_register_block_type(array(
            'name'              => 'three-column-features',
            'title'             => __('USAU / Three Column Features'),
            'description'       => __('Three Column Features block.'),
            'render_template'   => 'blocks/three-column-features/three-column-features.php',
            'category'          => 'share',
            'icon'              => 'rss',
            'align'             => 'full',
        ));

        // SITEMAP
        acf_register_block_type(array(
            'name'              => 'site-map',
            'title'             => __('USAU / Site Map'),
            'description'       => __('Site map block.'),
            'render_template'   => 'blocks/site-map/site-map.php',
            'category'          => 'share',
            'icon'              => 'rss',
            'align'             => 'full',
        ));

        // HEADER
        acf_register_block_type(array(
            'name'              => 'usau-header',
            'title'             => __('USAU / Header'),
            'description'       => __('USAU Header block.'),
            'render_template'   => 'blocks/header/header.php',
            'category'          => 'share',
            'icon'              => 'rss',
            'align'             => 'full',
        ));

        // PHOTO ESSAY
        acf_register_block_type(array(
            'name'              => 'photo-essay',
            'title'             => __('USAU / Photo Essay'),
            'description'       => __('USAU Photo Essay block.'),
            'render_template'   => 'blocks/photo-essay/photo-essay.php',
            'category'          => 'share',
            'icon'              => 'rss',
            'align'             => 'full',
        ));

        // Admin Button
        acf_register_block_type(array(
            'name'              => 'admin-button',
            'title'             => __('USAU / Admin Button'),
            'description'       => __('USAU Admin Button block.'),
            'render_template'   => 'blocks/admin-button/admin-button.php',
            'category'          => 'share',
            'icon'              => 'rss',
            'align'             => 'full',
        ));

        // Spirit Awards Button
        acf_register_block_type(array(
            'name'              => 'spirit-awards',
            'title'             => __('USAU / Spirit Awards'),
            'description'       => __('USAU Spirit Awards block.'),
            'render_template'   => 'blocks/spirit-awards/spirit-awards.php',
            'category'          => 'share',
            'icon'              => 'rss',
            'align'             => 'full',
        ));

        // News Updates
        acf_register_block_type(array(
            'name'              => 'news-updates',
            'title'             => __('USAU / News Updates'),
            'description'       => __('USAU News Updates block.'),
            'render_template'   => 'blocks/news-updates/news-updates.php',
            'category'          => 'share',
            'icon'              => 'rss',
            'align'             => 'full',
        ));

        // USAU Video
        acf_register_block_type(array(
            'name'              => 'usau-video',
            'title'             => __('USAU / Video'),
            'description'       => __('USAU Video block.'),
            'render_template'   => 'blocks/video/video.php',
            'category'          => 'share',
            'icon'              => 'rss',
            'align'             => 'full',
        ));

        // People Grid
        acf_register_block_type(array(
            'name'              => 'people-grid',
            'title'             => __('USAU / People Grid'),
            'description'       => __('USAU People Grid block.'),
            'render_template'   => 'blocks/people-grid/people-grid.php',
            'category'          => 'share',
            'icon'              => 'rss',
            'align'             => 'full',
        ));

    }
}