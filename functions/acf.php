<?php

/*
    Advanced Custom Fields
*/

if( function_exists('acf_add_options_page') ) {
    acf_add_options_page();
    acf_add_options_sub_page('Global');
    acf_add_options_sub_page('Header');
    acf_add_options_sub_page('Footer');
    acf_add_options_sub_page('Hub Navigation');
    acf_add_options_sub_page('Team USA');
    acf_add_options_sub_page('Local');
    acf_add_options_sub_page('About');
    acf_add_options_sub_page('College Teams');
    acf_add_options_sub_page('Club Teams');
    acf_add_options_sub_page('Resources');
    acf_add_options_sub_page('Watch');
    acf_add_options_sub_page('404');
}

// filter for every field

function my_relationship_query( $args, $field, $post_id ) {
    $args['orderby'] = 'date';
    $args['order'] = 'DESC';
    return $args;
}

add_filter('acf/fields/relationship/query', 'my_relationship_query', 10, 3);

