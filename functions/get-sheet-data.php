<?php

// Pull data from Google Spredsheeet Source
function usau_get_sheet_data($transient_name, $spreadsheet_ID, $timeout, $sheet = NULL) {
    if(empty($spreadsheet_ID)) {
        return array();
    }

    if($sheet !== NULL) {
        $url = "https://sheets.googleapis.com/v4/spreadsheets/" . $spreadsheet_ID . "/values/" . urlencode($sheet) . "!A2:Z?key=AIzaSyAJ1Hzo79gycFQGGSECizbUGo9aTW1uV5M";
    } else {
        $url = "https://sheets.googleapis.com/v4/spreadsheets/" . $spreadsheet_ID . "/values/A2:Z?key=AIzaSyAJ1Hzo79gycFQGGSECizbUGo9aTW1uV5M";
    }

    if(get_transient($transient_name)) {
        $transient = get_transient($transient_name);
        return $transient;
    } else {
        $response = wp_remote_get($url);
        
        if (is_wp_error($response)) {
            return array();
        }

        $api_response = json_decode(wp_remote_retrieve_body($response), true);
        
        if (!is_array($api_response) || !isset($api_response['values'])) {
            return array();
        }

        $values = $api_response['values'];

        if($values) {
            set_transient($transient_name, $values, $timeout);
            return $values;
        }
        
        return array();
    }
}