<?php

// Clear Menu Cache

function flush_transients_button() {
	global $wp_admin_bar;

	// If User isnt even logged in or if admin bar is disabled
	if ( !is_user_logged_in() || !is_admin_bar_showing() )
		return false;

	// If user doesnt have the perms
	if ( function_exists('current_user_can') && false == current_user_can('activate_plugins') )
		return false;

	// Button args
	$wp_admin_bar->add_menu( array(
		'parent' => '',
		'id' => 'flush_transients_button',
		'title' => __( 'Clear Spreadsheet Data Cache' ),
		'meta' => array( 'title' => __( 'Clear spreadsheet data manually to get latest data from source' )),
		'href' => wp_nonce_url( admin_url( 'index.php?action=deltransientpage'), 'flush_transients_button' ))
	);
}
add_action( 'admin_bar_menu', 'flush_transients_button', 10000 );

function flush_transients() {
	global $_wp_using_ext_object_cache;

	// Check Perms
	if ( function_exists('current_user_can') && false == current_user_can('activate_plugins') )
		return false;

	// Flush Cache
	if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'deltransientpage' && ( isset( $_GET[ '_wpnonce' ] ) ? wp_verify_nonce( $_REQUEST[ '_wpnonce' ], 'flush_transients_button' ) : false ) ) {
        

		function get_transient_keys_with_prefix( $prefix ) {
			global $wpdb;
		
			$prefix = $wpdb->esc_like( '_transient_' . $prefix );
			$sql    = "SELECT `option_name` FROM $wpdb->options WHERE `option_name` LIKE '%s'";
			$keys   = $wpdb->get_results( $wpdb->prepare( $sql, $prefix . '%' ), ARRAY_A );
		
			if ( is_wp_error( $keys ) ) {
				return [];
			}
		
			return array_map( function( $key ) {
				// Remove '_transient_' from the option name.
				return ltrim( $key['option_name'], '_transient_' );
			}, $keys );
		}
		
		function delete_transients_with_prefix( $prefix ) {
			foreach ( get_transient_keys_with_prefix( $prefix ) as $key ) {
				delete_transient( $key );
			}
		}

		delete_transients_with_prefix('USAU');

		
    	// If using object cache
    	if($_wp_using_ext_object_cache) {
	    	wp_cache_flush();
    	}

		wp_redirect(admin_url().'?cache_type=transients&cache_status=flushed');
		die();
	} else {
		wp_redirect(admin_url().'?cache_type=transients&cache_status=not_flushed');
		die();
	}

}
if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'deltransientpage' ) {
	add_action( 'admin_init', 'flush_transients');
}

function flush_display_admin_msg() {
	if($_GET[ 'cache_status' ] == '')
		return;

	// Display Msg
	if ( $_GET[ 'cache_status' ] == 'flushed' ) { ?>
	    <div class="updated">
	        <p>Spreadsheet data cache was successfully cleared.</p>
	    </div>
    	<?php
	} elseif ( $_GET[ 'cache_status' ] == 'not_flushed' ) { ?>
	    <div class="error">
	        <p>Spreadsheet data cache was NOT cleared.</p>
	    </div>
    	<?php
	}
}
add_action( 'admin_notices', 'flush_display_admin_msg' );