<?php

/*
    Enqueue Styles & Scripts
*/


// Enqueue custom styles and scripts
function enqueue_styles_and_scripts() {

    // Register and noConflict jQuery 3.6.0
    wp_register_script( 'jquery.3.6.0', 'https://code.jquery.com/jquery-3.6.0.min.js' );
    wp_add_inline_script( 'jquery.3.6.0', 'var jQuery = $.noConflict(true);' );

    // Add style.css
    wp_enqueue_style( 'adobe-fonts', 'https://use.typekit.net/vcx3lxt.css' );

    wp_enqueue_style( 'style', get_stylesheet_directory_uri() . '/style.css' );
    wp_enqueue_style('font-awesome', "//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css");

    // Add plugins.js & site.js (with jQuery dependency)
    wp_enqueue_script( 'custom-plugins', get_stylesheet_directory_uri() . '/js/plugins.js', array( 'jquery.3.6.0' ) );
    wp_enqueue_script( 'custom-site', get_stylesheet_directory_uri() . '/js/site.js', array( 'jquery.3.6.0' ) );
}
add_action( 'wp_enqueue_scripts', 'enqueue_styles_and_scripts' );



// Add backend styles for Gutenberg
function gutenberg_styles() {
    wp_enqueue_style( 'adobe-fonts', 'https://use.typekit.net/vcx3lxt.css' );
    wp_enqueue_style( 'gutenberg-styles', get_theme_file_uri('gutenberg.css'), false );
}
add_action( 'enqueue_block_editor_assets', 'gutenberg_styles' );



