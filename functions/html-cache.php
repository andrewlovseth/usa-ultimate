<?php
require_once(ABSPATH . 'wp-admin/includes/file.php');

function usau_write_data_to_html_file( $filename, $url ) {
    $stream_opts = [
        "ssl" => [
            "verify_peer"=>false,
            "verify_peer_name"=>false,
        ]
    ];    

    $html = file_get_contents($url, false, stream_context_create($stream_opts));
	$time = time();
	file_put_contents( $filename, $html, FILE_USE_INCLUDE_PATH );
}

function usau_get_html_cache($url, $title) {
    $cached_filename = get_home_path() . "wp-content/themes/usa-ultimate/usau-html/" . sanitize_title_with_dashes($title) .  ".html";

    if ( file_exists( $cached_filename ) ) {
        $file_time = filemtime( $cached_filename );
        $expire = 2;
        if ( $file_time < ( time() - $expire ) ) {
            usau_write_data_to_html_file( $cached_filename, $url );
        }
    } else {
        usau_write_data_to_html_file( $cached_filename, $url );
    }

    $data = file_get_contents($cached_filename);

    return $data;
}