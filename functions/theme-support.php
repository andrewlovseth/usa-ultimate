<?php

/*
	Theme Support
*/



// Theme Support for title tags, post thumbnails, HTML5 elements, feed links
add_theme_support( 'title-tag' );
add_theme_support( 'post-thumbnails' );
add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ));
add_theme_support( 'automatic-feed-links' );

// Add wp_body_open
if ( ! function_exists( 'wp_body_open' ) ) {
    function wp_body_open() {
        do_action( 'wp_body_open' );
    }
}

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');


//Page Slug Body Class
function add_slug_body_class( $classes ) {
	global $post;
    if ( isset( $post ) ) {
        $classes[] = $post->post_type . '-' . $post->post_name;
    }

	return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );


// Remove Menus
function remove_menus(){
  remove_menu_page( 'edit-comments.php' );
}
add_action( 'admin_menu', 'remove_menus' );


function mytheme_add_body_class($classes) {
    // You can modify this check so it will run on every post type
    if (is_page()) {
        global $post;
        
        // If we *do* have an ancestors list, process it
        // http://codex.wordpress.org/Function_Reference/get_post_ancestors
        if ($parents = get_post_ancestors($post->ID)) {
            foreach ((array)$parents as $parent) {
                // As the array contains IDs only, we need to get each page
                if ($page = get_page($parent)) {
                    // Add the current ancestor to the body class array
                    $classes[] = "{$page->post_type}-{$page->post_name}";
                }
            }
        }

        // Add the current page to our body class array
        $classes[] = "{$post->post_type}-{$post->post_name}";
    }
    
    return $classes;
}
// Add a filter for WP
add_filter('body_class', 'mytheme_add_body_class');




// If descendant page
function is_descendent_of( $page_path ){
    if( !is_page() ){
        return false;
    }

    $ancestors = get_post_ancestors( get_queried_object_id() );
    $page = get_page_by_path( $page_path );
    if( $page )
        return in_array( $page->ID, $ancestors );
    return false;   
}


// Move Yoast to bottom
function yoasttobottom() {
  return 'low';
}
add_filter( 'wpseo_metabox_prio', 'yoasttobottom');