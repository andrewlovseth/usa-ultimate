<?php 

// Video Playlist Slug Rewrite
add_filter( 'organizewp_taxonomy_args_video_filters', function( $args ) {

	$args['rewrite'] = array(
	  'slug' => 'watch/playlist',
	  'with_front' => false
	);

	return $args;
});


// College Teams Slug Rewrite
add_filter( 'organizewp_post_type_args_college_teams', function( $args ) {
	
	$args->rewrite = array(
		'slug' => 'college/teams',
		'with_front' => false
	);
  
  return $args;
});


// Club Teams Slug Rewrite
add_filter( 'organizewp_post_type_args_club_teams', function( $args ) {
	
	$args->rewrite = array(
		'slug' => 'club/teams',
		'with_front' => false
	);
  
  return $args;
});


// World Games Players Slug Rewrite
add_filter( 'organizewp_post_type_args_world_games_players', function( $args ) {
	
	$args->rewrite = array(
		'slug' => 'team-usa/world-games-players',
		'with_front' => false
	);
  
  return $args;
});


// Videos Slug Rewrite
add_filter( 'organizewp_post_type_args_videos', function( $args ) {
	
	$args->rewrite = array(
		'slug' => 'watch/videos',
		'with_front' => false
	);
  
  return $args;
});