<?php

	get_header();

	$video_filter_name = get_queried_object()->name;
	$video_filter_slug = get_queried_object()->slug;

?>

	<section class="page-header usau-block">
		<h1>Watch: <?php echo $video_filter_name; ?> </h1>
	</section>

	<section class="video-archive usau-block">
		<?php echo do_shortcode('[ajax_load_more id="video-filters" container_type="div" theme_repeater="video.php" post_type="videos" posts_per_page="16" taxonomy="video_filters" taxonomy_terms="' . $video_filter_slug . '" taxonomy_operator="IN" scroll="false" button_label="Load More"]'); ?>
	</section>

	<?php get_template_part('template-parts/watch/library-cta'); ?>

<?php get_footer(); ?>