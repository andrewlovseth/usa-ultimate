<?php get_header(); ?>

	<?php get_template_part('template-parts/archive-resources/page-header'); ?>

	<section class="resource-archive usau-block">

		<?php get_template_part('template-parts/archive-resources/filters'); ?>

		<?php 
			$shortcode = get_field('resources_archive_shortcode', 'options');
			echo do_shortcode($shortcode);
		?>

	</section>

<?php get_footer(); ?>