<?php get_header(); ?>

	<?php get_template_part('template-parts/college-teams/hero'); ?>

	<?php get_template_part('template-parts/college-teams/hub-navigation'); ?>

	<?php get_template_part('template-parts/college-teams/page-header'); ?>

	<?php get_template_part('template-parts/college-teams/teams'); ?>

<?php get_footer(); ?>