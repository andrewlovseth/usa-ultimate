<?php get_header(); ?>

    <section class="resource-item usau-block">

        <?php get_template_part('template-parts/resources/resource'); ?>

        <div class="cta back align-center">
            <a href="<?php echo site_url('/resources/'); ?>" class="btn red">&larr; Back to Resources</a>
        </div>
    
    </section>

<?php get_footer(); ?>