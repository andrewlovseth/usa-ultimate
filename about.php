<?php

/*
	Template Name: About

*/
global $post;
$slug = $post->post_name;

get_header(); ?>
	
	<section class="about-content usau-block">
		
		<?php get_template_part('template-parts/about/global/about-nav'); ?>

        <section class="about-main <?php echo $slug; ?>">

            <?php get_template_part('template-parts/about/content/' . $slug); ?>

        </section>

	</section>

<?php get_footer(); ?>