<?php
    $title = get_sub_field('title'); 
    $video = get_sub_field('video'); 
    $thumbnail = $video['thumbnail'];
    $video_post = $video['video_post'];
    $video_id = get_field('youtube_id', $video_post);

?>


<div class="grid-item feature video-feature">
    <div class="video-thumbnail">
        <a href="#" data-video-id="<?php echo $video_id; ?>" class="video-trigger discover-play-btn">
            <span class="icon"></span>
            <span class="thumbnail">
                <div class="content">
                    <?php if($thumbnail): ?>
                        <img src="<?php echo $thumbnail['sizes']['medium']; ?>" alt="<?php echo $thumbnail['alt']; ?>" />
                    <?php else: ?>
                        <img src="https://i3.ytimg.com/vi/<?php echo $video_id; ?>/hqdefault.jpg" alt="Video Thumbnail: <?php echo $title; ?>" />
                    <?php endif; ?>    
                </div>            
            </span>		
        </a>				
    </div>

    <div class="info">
        <div class="headline blue">
            <h4><?php echo get_sub_field('title'); ?></h4>
        </div>

        <div class="copy p3">
            <?php echo get_sub_field('description'); ?>
        </div>							    		
    </div>
</div>