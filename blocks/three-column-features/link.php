<?php 
    $link = get_sub_field('link');
    if($link):
	$link_url = $link['url'];
	$link_title = $link['title'];
	$link_target = $link['target'] ? $link['target'] : '_self';
 ?>

    <div class="grid-item feature link-feature">
        <?php if(get_field('hide_photos') == false): ?>
            <div class="photo">
                <div class="content">
                    <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
                        <img src="<?php $image = get_sub_field('photo'); echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
                    </a>
                </div>
            </div>
        <?php endif; ?>

        <div class="info">
            <div class="headline blue">
                <h4><a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo get_sub_field('title'); ?></a></h4>
            </div>

            <div class="copy p3">
                <?php echo get_sub_field('description'); ?>
            </div>
            
            <div class="cta">
                <a class="underline red" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
            </div>
        </div>
    </div>

<?php endif ; ?>