<div class="grid-item feature toggle-feature">
    <?php if(get_field('hide_photos') == false): ?>
        <div class="photo">
            <div class="content">
                <img src="<?php $image = get_sub_field('photo'); echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
            </div>
        </div>
    <?php endif; ?>

    <div class="info">
        <div class="headline blue">
            <h4><?php echo get_sub_field('title'); ?></h4>
        </div>

        <div class="copy p3">
            <a href="#" class="text-toggle">Learn More</a>
                                                        
            <div class="text">
                <?php echo get_sub_field('description'); ?>
            </div>
        </div>							    		
    </div>
</div>