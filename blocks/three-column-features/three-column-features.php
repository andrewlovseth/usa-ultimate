<?php

/*
 * Three Columns Block Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'three-column-features' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'three-column-features usau-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

$layout = get_field('layout');
if($layout) {
    $className .= ' ' . $layout . '-features';
}

?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

    <?php if(get_field('section_header')): ?>
        <div class="headline blue align-center underline features-header">
            <h3><?php echo get_field('section_header'); ?></h3>
        </div>
    <?php endif; ?>

    <div class="features features-grid">
        <?php if(have_rows('features')): while(have_rows('features')): the_row(); ?>

            <?php get_template_part('blocks/three-column-features/' . $layout); ?>

        <?php endwhile; endif; ?>							
    </div>

</section>