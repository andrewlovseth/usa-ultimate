<?php

/*
 * Cover Side by Side Block Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'cover-side-by-side-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'cover-side-by-side';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}


?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

	<?php if(have_rows('cover_side_by_side')): while(have_rows('cover_side_by_side')) : the_row(); ?>

		<?php if( get_row_layout() == 'side' ): ?>

			<div class="side photo<?php if(get_sub_field('color')): ?> <?php echo get_sub_field('color'); ?><?php endif; ?>">
				<div class="content">
					<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

					<div class="info">
						<div class="info-wrapper">
							<div class="headline white uppercase">
								<h2 class="cover-title"><?php echo get_sub_field('headline'); ?></h2>
							</div>

							<div class="copy p1">
								<?php echo get_sub_field('deck'); ?>
							</div>

							<div class="cta">
								<?php
									$link = get_sub_field('cta'); if( $link ): 
								    $link_url = $link['url'];
								    $link_title = $link['title'];
								    $link_target = $link['target'] ? $link['target'] : '_self';
								?>

									<a class="btn white" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>								    	
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>

		<?php endif; ?>

	<?php endwhile; endif; ?>

</section>