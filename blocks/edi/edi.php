<?php

/*
 * EDI Block
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'edi-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'edi usau-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

	<?php if(have_rows('edi')): $count =1; while(have_rows('edi')) : the_row(); ?>

		<?php if( get_row_layout() == 'commitment' ): ?>

			<section class="commitment" id="commitment-<?php echo $count; ?>">
				<div class="commitment-header">
					<div class="info">
						<div class="headline white">
							<span class="count"><?php echo $count; ?></span>
							<h2><?php echo get_sub_field('commitment_statement'); ?></h2>						
						</div>
					</div>

					<div class="photo">
						<div class="content">
							<img src="<?php $image = get_sub_field('main_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						</div>
					</div>
				</div>

				<div class="basic-info">
					<div class="why">
						<div class="headline blue underline">
							<h3><?php echo get_sub_field('why_sub_header'); ?></h3>
						</div>

						<div class="copy p1">
							<?php echo get_sub_field('why_copy'); ?>
						</div>							
					</div>

					<div class="how">
						<div class="headline blue underline">
							<h3><?php echo get_sub_field('how_sub_header'); ?></h3>
						</div>

						<div class="copy p2">
							<?php echo get_sub_field('how_copy'); ?>
						</div>							
					</div>
				</div>

				<div class="initiatives">
					<div class="headline sub-header blue align-center underline">
						<h3><?php echo get_sub_field('initiatives_sub_header'); ?></h3>
					</div>

					<div class="grid initiatives-grid">
						<?php if(have_rows('initiatives')): while(have_rows('initiatives')): the_row(); ?>
							
							<div class="grid-item initiative">
								<div class="photo">
									<div class="content">
										<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
									</div>
								</div>

								<div class="info">
									<div class="headline blue">
										<h4><?php echo get_sub_field('title'); ?></h4>
									</div>

									<div class="copy p3">
										<a href="#" class="text-toggle">Learn More</a>
																					
										<div class="text">
											<?php echo get_sub_field('description'); ?>
										</div>
									</div>							    		
								</div>
							</div>

						<?php endwhile; endif; ?>							
					</div>
				</div>

				<div class="get-involved">
					<div class="headline white underline">
						<h3><?php echo get_sub_field('get_involved_sub_header'); ?></h3>
					</div>

					<div class="copy p2">
						<?php echo get_sub_field('get_involved_copy'); ?>
					</div>
				</div>
			</section>

		<?php endif; ?>

	<?php $count++; endwhile; endif; ?>

</section>