<?php

/*
 * FAQs Block Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'faqs-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'faqs grid usau-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
        
    <div class="section-header headline underline blue">
        <h3><?php echo get_field('section_header'); ?></h3>
    </div>

    <div class="faqs-content">
        <?php if(have_rows('faqs')): $count = 1; while(have_rows('faqs')): the_row(); ?>

            <div class="faq<?php if($count == 1): ?> active<?php endif; ?>">
                <div class="question headline">
                    <h4><?php echo get_sub_field('question'); ?></h4>
                </div>

                <div class="answer">
                    <div class="copy p2">
                        <?php echo get_sub_field('answer'); ?>
                    </div>                    
                </div>

            </div>

        <?php $count++; endwhile; endif; ?>
    </div>

</section>