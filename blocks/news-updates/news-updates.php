<?php

/*
 * News Updates Block Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'news-updates-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'news-updates usau-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="section-header headline blue underline">
        <h4><?php echo get_field('section_header'); ?></h4>
    </div>

    <div class="news-updates-grid">

        <?php if(have_rows('columns')): while(have_rows('columns')) : the_row(); ?>

            <?php if( get_row_layout() == 'text' ): ?>

                <div class="news-update col">
                    <div class="col-header headline blue">
                        <h5><?php echo get_sub_field('headline'); ?></h5>
                    </div>

                    <div class="copy p3">
                        <?php echo get_sub_field('copy'); ?>

                    </div>
                </div>

            <?php endif; ?>
        
        <?php endwhile; endif; ?>

    </div>
</section>