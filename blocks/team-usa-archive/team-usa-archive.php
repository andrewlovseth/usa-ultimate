<?php

/*
 * Team USA Archive Block Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'team-usa-archive-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'team-usa-archive usau-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

$event = get_field('event');
$event_value = $event['value'];
$event_display_value = $event['label'];



?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

	<div class="info">
		<?php include(locate_template('blocks/team-usa-archive/partials/meta.php', false, false)); ?>

		<?php include(locate_template('blocks/team-usa-archive/partials/results.php', false, false)); ?>
	</div>

	<?php include(locate_template('blocks/team-usa-archive/partials/roster.php', false, false)); ?>

	<?php include(locate_template('blocks/team-usa-archive/partials/gallery.php', false, false)); ?>

</section>

<?php include(locate_template('template-parts/team-usa/event-archive.php', false, false)); ?>