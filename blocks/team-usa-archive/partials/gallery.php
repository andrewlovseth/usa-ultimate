<?php $gallery = get_field('gallery'); if( $gallery ): ?>
    <div class="player-gallery">
        <div class="section-header headline blue align-center underline">
            <h3>Roster Gallery</h3>
        </div>

        <div class="player-grid">
            <?php foreach( $gallery as $player ): ?>
                <?php
                    $name = $player['title'];
                    $meta = $player['caption']
                ?>
                <div class="player">
                    <div class="player__photo">
                        <?php echo wp_get_attachment_image($player['ID'], 'full'); ?>
                    </div>

                    <div class="player__info">
                        <?php if($name): ?>
                            <h3 class="player__headline"><?php echo $name; ?></h3>
                        <?php endif; ?>
                        
                        <?php if($meta): ?>
                            <h4 class="player__meta"><?php echo $meta; ?></h4>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>