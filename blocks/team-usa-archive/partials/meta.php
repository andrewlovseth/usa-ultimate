<div class="meta">
	<?php if(have_rows('delegation')): ?>
		<div class="delegation">
			<div class="headline red">
				<h5><?php $event = get_field('event'); echo $event['label']; ?> <?php echo get_field('year'); ?></h5>
			</div>

			<div class="teams">
				<?php while(have_rows('delegation')): the_row(); $team = get_sub_field('team'); ?>
				<?php 
					if ( function_exists( 'get_field' ) ) {
						$pid = get_post($team->ID);
						if ( has_blocks( $pid ) ) {
							$blocks = parse_blocks( $pid->post_content );

							foreach ( $blocks as $block ) {
								if ( $block['blockName'] === 'acf/google-sheet-roster' ) {
									$gender_division = $block['attrs']['data']['gender_division'];
								}
							}
						}
					}
				?>
					<a href="<?php echo get_permalink($team->ID); ?>"<?php if(get_sub_field('active') == true): ?> class="active"<?php endif; ?>>
						<?php echo $gender_division; ?>
					</a>

				<?php endwhile; ?>
			</div>
		</div>
	<?php endif; ?>

	<div class="location">
		<h2><?php echo get_field('location'); ?></h2>
	</div>

	<?php if(get_field('show_placeholder') == TRUE): ?>
		<?php 
			$placeholder = get_field('placeholder');
			$photo = $placeholder['photo'];
			$headline = $placeholder['headline'];
			$copy = $placeholder['copy'];		
		?>

		<div class="placeholder">
			<?php if($photo): ?>
				<div class="photo">
					<div class="content">
						<img src="<?php echo $photo['sizes']['large']; ?>" alt="<?php echo $photo['alt']; ?>" />
					</div>
				</div>			
			<?php endif; ?>

			<?php if($headline): ?>
				<div class="headline red">
					<h4><?php echo $headline; ?></h4>
				</div>
			<?php endif; ?>

			<?php if($copy): ?>
				<div class="copy p2">
					<?php echo $copy; ?>
				</div>
			<?php endif; ?>			
		</div>
	<?php endif; ?>

	<?php if(get_field('finish')): ?>
		<div class="result">
			<?php if(get_field('medal')): ?>
				<div class="medal">
					<img src="<?php bloginfo('template_directory'); ?>/images/<?php echo get_field('medal'); ?>.svg" alt="<?php echo get_field('medal'); ?>">
				</div>
			<?php endif; ?>
			<h3><?php echo get_field('finish'); ?> Place</h3>
		</div>
	<?php endif; ?>

	<?php if(get_field('photo')): ?>
		<div class="photo">
			<img src="<?php $image = get_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		</div>
	<?php endif; ?>
</div>