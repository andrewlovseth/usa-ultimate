<?php

global $post;
$slug = $post->post_name;

$roster = get_field('roster');
$spreadsheet_id = $roster['sheet_id'];
$roster_tab_index = $roster['tab_index'];

$gender_division = get_field('gender_division');
$transient_name = 'USAU_roster-' . $slug;
if(get_transient($transient_name)) {
    $roster_entries = get_transient($transient_name);
} else {
    $roster_entries = usau_get_sheet_data($transient_name, $spreadsheet_id, DAY_IN_SECONDS, $gender_division);
}

if(!empty($roster_entries)): ?>

	<div class="roster-list">	

		<div class="roster-header headline blue">
			<h4>Roster</h4>
		</div>

		<table>
			<thead>
				<tr>
					<th class="name">Name</th>
					<th class="city">City</th>
					<?php if(isset($roster_entries[0][2])): ?>
						<th class="city">Team</th>
					<?php endif; ?>
				</tr>
			</thead>

			<tbody>
				<?php foreach ($roster_entries as $player): ?>

					<?php
						if(isset($player[0])) {
							$name = $player[0];
						} else {
							$name = NULL;
						}

						if(isset($player[1])) {
							$city = $player[1];
						} else {
							$city = NULL;
						}

						if(isset($player[2])) {
							$team = $player[2];
						} else {
							$team = NULL;
						}
					?>						

					<tr class="player">
						<td class="name"><?php echo $name; ?></td>
						<?php if(isset($player[1])): ?>
							<td class="city"><?php echo $city; ?></td>
						<?php endif; ?>

						<?php if(isset($player[2])): ?>
							<td class="team"><?php echo $team; ?></td>
						<?php endif; ?>
					</tr>

				<?php endforeach; ?>

					<?php if(have_rows('staff')): ?>

						<tr class="staff-header">
							<td>Staff</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>

						<?php while(have_rows('staff')): the_row(); ?>

						<tr class="player staff">
							<td class="name"><?php echo get_sub_field('name'); ?></td>
							<?php if(get_sub_field('city')): ?>
								<td class="city"><?php echo get_sub_field('city'); ?></td>
							<?php endif; ?>

							<?php if(get_sub_field('team')): ?>
								<td class="team"><?php echo get_sub_field('team'); ?></td>			
							<?php endif; ?>					
						</tr>

					<?php endwhile;?>

				<?php endif; ?>

			</tbody>
		</table>
	</div>

<?php else: ?>

	<div class="roster-list copy p2 error-message">
		<p>No roster information available at this time.</p>
	</div>

<?php endif; ?>