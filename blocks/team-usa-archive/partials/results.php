<?php

global $post;
$slug = $post->post_name;

$results = get_field('results');
$spreadsheet_id = $results['sheet_id'];
$results_tab_index = $results['tab_index'];

$gender_division = get_field('gender_division');
$transient_name = 'USAU_results-' . $slug;

if($spreadsheet_id) {
	if(get_transient($transient_name)) {
		$results_entries = get_transient($transient_name);
	} else {
		$results_entries = usau_get_sheet_data($transient_name, $spreadsheet_id, DAY_IN_SECONDS, $gender_division);
	}
}


if(!empty($results_entries)): ?>

		<div class="results">
			<div class="results-header headline blue">
				<h4>Results</h4>
			</div>

			<div class="game-list">

				<?php foreach ($results_entries as $game): ?>

					<?php
						$usa_score = $game[0];
						$opp_score = $game[1];
						$opponent = $game[2];
						if(isset($game[3])) {
							$round = $game[3];
						}
					?>						

				    <div class="game">
				    	<div class="usa team">USA</div>

				    	<div class="score">
				    		<div class="usa-score"><?php echo $usa_score; ?></div>
				    		<div class="divider">-</div>
				    		<div class="opponent-score"><?php echo $opp_score; ?></div>

					    	<?php if(isset($game[3])): ?>
					    		<div class="round"><?php echo $round; ?></div>
					    	<?php endif; ?>
				    	</div>

				    	<div class="opponent team"><?php echo $opponent; ?></div>


				    </div>

				<?php endforeach; ?>

			</div>
		</div>

<?php endif; ?>