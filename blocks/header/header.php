<?php

/*
 * Header Block Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'usau-header-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'usau-block usau-header';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}

if( $is_preview ) {
    $className .= ' is-admin';
}

$headlineClassName = 'headline section-header';
$color = get_field('color');
$uppercase = get_field('uppercase');
$underline = get_field('underline');

if( !empty($block['align']) ) {
    $headlineClassName .= ' align-' . $block['align'];
}

if( $color ) {
    $headlineClassName .= ' '. $color;
}

if( $uppercase == true ) {
    $headlineClassName .= ' uppercase';
}

if( $underline == true ) {
    $headlineClassName .= ' underline';
}


$hTag = get_field('heading');
$headline = get_field('headline');

?>


<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

    <div class="<?php echo esc_attr($headlineClassName); ?>">
        <<?php echo $hTag; ?>><?php echo $headline; ?></<?php echo $hTag; ?>>
    </div>

</section>