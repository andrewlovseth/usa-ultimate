<?php

/*
 * Hub Navigation Block Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'hub-navigation-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'hub-navigation usau-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}

if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

if( $is_preview ) {
    $className .= ' is-admin';
}

$hub = get_field('hub');
if( $hub ) {
	 $className .= ' ' . $hub;
}

?>


<nav id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
	<div class="hub-navigation-wrapper">
		<?php if(get_field($hub . '_hub_navigation_mobile_header', 'options')): ?>
			<div class="mobile-header">
				<h4><?php echo get_field($hub . '_hub_navigation_mobile_header', 'options'); ?></h4>
			</div>
		<?php endif; ?>

		<?php if(have_rows($hub . '_hub_navigation', 'options')): ?>

			<div class="hub-wrapper">

				<?php while(have_rows($hub . '_hub_navigation', 'options')) : the_row(); ?>

					<?php if( get_row_layout() == 'group' ): ?>
				
						<div class="group">
							<?php if(get_sub_field('group_label')): ?>
								<div class="group-header">
									<h4><?php echo get_sub_field('group_label'); ?></h4>
								</div>
							<?php endif; ?>

							<div class="links">
								<?php if(have_rows('links')): while(have_rows('links')): the_row(); ?>
									<div class="link">
										<?php
											$icon = get_sub_field('icon');
											$link = get_sub_field('link'); if( $link ): 
											$link_url = $link['url'];
											$link_title = $link['title'];
											$link_target = $link['target'] ? $link['target'] : '_self';

										?>
											<a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
												<span class="icon"><img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>" /></span>
												<span class="label"><?php echo esc_html( $link_title ); ?></span>
											</a>								    	

										<?php endif; ?>
										
									</div>

								<?php endwhile; endif; ?>						
							</div>
						</div>
						
					<?php endif; ?>

				<?php endwhile; ?>

				<?php if(get_field($hub . '_hub_admin_link', 'options')): ?>
					<div class="admin-link">
					
						<?php

							$link = get_field($hub . '_hub_admin_link', 'options'); if( $link ): 
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';

						?>
							<a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">	<span class="label"><?php echo esc_html( $link_title ); ?></span>
							</a>								    	

						<?php endif; ?>				

					</div>
				<?php endif; ?>
				
			</div>
		
		<?php endif; ?>
	</div>
</nav>