<?php

/*
 * Upcoming Events Block
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'upcoming-events-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'upcoming-events usau-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

	<?php if(get_field('section_header')): ?>

		<div class="section-header headline blue underline">
			<h3><?php echo get_field('section_header'); ?></h3>
		</div>

	<?php endif; ?>

	<?php if(have_rows('events')): ?>

		<div class="events grid three-col-grid">

			<?php while(have_rows('events')): the_row(); ?>

				<div class="grid-item event">
					
					<?php $display_date = get_sub_field('display_date'); if( $display_date ): ?>
						<div class="display-date">
							<?php if($display_date['month']): ?>
								<div class="month">
									<span><?php echo $display_date['month']; ?></span>
								</div>
							<?php endif; ?>

							<?php if($display_date['date']): ?>
								<div class="date">
									<span><?php echo $display_date['date']; ?></span>
								</div>
							<?php endif; ?>

							<?php if($display_date['year']): ?>
								<div class="year">
									<span><?php echo $display_date['year']; ?></span>
								</div>
							<?php endif; ?>
						</div>
					<?php endif; ?>

					<div class="info">
						<div class="headline red">
							<h4><?php echo get_sub_field('name'); ?></h4>
						</div>

						<?php if(get_sub_field('date') || get_sub_field('location')): ?>
							<div class="meta p4">
								<p>
									<?php if(get_sub_field('date')): ?>
										<span class="date"><?php echo get_sub_field('date'); ?></span>
									<?php endif; ?>

									<?php if(get_sub_field('location')): ?>
										<span class="location"><?php echo get_sub_field('location'); ?></span>
									<?php endif; ?>
								</p>
							</div>
						<?php endif; ?>

						<div class="copy p3">
							<?php echo get_sub_field('description'); ?>
						</div>
						
						<?php 
							$link = get_sub_field('cta');
							if( $link ): 
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
							?>

							<div class="cta p4">
								<a class="underline blue" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
							</div>

						<?php endif; ?>
						
					</div>
				</div>

			<?php endwhile; ?>

		</div>

	<?php endif; ?>

	<?php 
		$link = get_field('cta');
		if( $link ): 
		$link_url = $link['url'];
		$link_title = $link['title'];
		$link_target = $link['target'] ? $link['target'] : '_self';
		?>

		<div class="full-season-cta cta align-center">
			<a class="btn small red" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
		</div>

	<?php endif; ?>

</section>