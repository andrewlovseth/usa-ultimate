<?php

/*
* Countdown Block
*/

// Create id attribute allowing for custom "anchor" value.
$id = 'countdown-' . $block['id'];

if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'countdown usau-block';

if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}

if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

if( $is_preview ) {
    $className .= ' is-admin';
}

if (date('I', time())) {
	$target_deadline = get_field('date') . ' GMT-0700';
} else {
	$target_deadline = get_field('date') . ' GMT-0600';
}

?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="countdown-container">
        <div class="section-header">
            <div class="headline">
                <h4><?php echo get_field('headline'); ?></h4>
            </div>

            <div class="copy p3 deck">
                <?php echo get_field('deck'); ?>
            </div>
        </div>

        <div id="<?php echo esc_attr($id); ?>-clock" class="countdown-clock">
            <div class="days interval">
                <span class="days-counter counter"></span>
                <div class="interval-label">Days</div>
            </div>

            <div class="hours interval">
                <span class="hours-counter counter"></span>
                <div class="interval-label">Hours</div>
            </div>

            <div class="minutes interval">
                <span class="minutes-counter counter"></span>
                <div class="interval-label">Minutes</div>
            </div>

            <div class="seconds interval">
                <span class="seconds-counter counter"></span>
                <div class="interval-label">Seconds</div>
            </div>
        </div>

        <?php if(have_rows('ctas')): ?>
            <div class="ctas">
                <?php while(have_rows('ctas')): the_row(); ?>
        
                <?php 
                    $link = get_sub_field('cta');
                    if( $link ): 
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_self';
                ?>

                    <div class="cta">
                        <a class="btn black" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                    </div>

                <?php endif; ?>

                <?php endwhile; ?>
            </div>
        <?php endif; ?>
    </div>

    <script>
        var deadline = '<?php echo $target_deadline; ?>';
        var countdownClock = "<?php echo esc_attr($id); ?>-clock";

        function getTimeRemaining(endtime) {
            var t = Date.parse(endtime) - Date.parse(new Date());
            var seconds = Math.floor((t / 1000) % 60);
            var minutes = Math.floor((t / 1000 / 60) % 60);
            var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
            var days = Math.floor(t / (1000 * 60 * 60 * 24));

            return {
                'total': t,
                'days': days,
                'hours': hours,
                'minutes': minutes,
                'seconds': seconds
            };
        }

        function initializeClock(id, endtime) {
            var clock = document.getElementById(id);
            var daysSpan = clock.querySelector('.days-counter');
            var hoursSpan = clock.querySelector('.hours-counter');
            var minutesSpan = clock.querySelector('.minutes-counter');
            var secondsSpan = clock.querySelector('.seconds-counter');

            function updateClock() {
                var t = getTimeRemaining(endtime);



                if (t.total <= 0 || isNaN(t.total)) {
                    clock.style.display = "none";
/*
                    daysSpan.innerHTML = '0';
                    hoursSpan.innerHTML = '00';
                    minutesSpan.innerHTML = '00';
                    secondsSpan.innerHTML = '00';
*/
                } else {
                    daysSpan.innerHTML = t.days;
                    hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
                    minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
                    secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);
                }
            }

            updateClock();
              var timeinterval = setInterval(updateClock, 1000);
            }

        initializeClock(countdownClock, deadline);
    </script>
    



    
</section>