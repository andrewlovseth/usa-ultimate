<div class="games">    
    <?php
        
        $spreadsheet_id = get_field('sheet_id');

        $transient_name = 'USAU_scoreboard-' . $block['id'];
        if(get_transient($transient_name)) {
            $entries = get_transient($transient_name);
        } else {
            $entries = usau_get_sheet_data($transient_name, $spreadsheet_id, 60);
        }

        $entries = get_transient('USAU_scoreboard-' . $block['id']);

        foreach ($entries as $entry):             
            $division = $entry[0];
            $team_1 = $entry[1];
            $team_1_score = $entry[2];
            $team_2 = $entry[3];
            $team_2_score = $entry[4];
            $field = $entry[5];
            $round = $entry[6];
            $date = $entry[7];
            $time = $entry[8];
            $status = $entry[9];
            $watch_link = $entry[10];

            if($team_1_score > $team_2_score) {
                $score_status = 'team-1-up';
            } elseif($team_2_score > $team_1_score) {
                $score_status = 'team-2-up';
            } else {
                $score_status = 'tied';
            }
    ?>

        <div class="game <?php echo $score_status; ?>">
            <div class="division">
                <span><?php echo $division; ?></span>
            </div>

            <div class="game-wrapper">
                <div class="status">
                    <span><?php echo $status; ?></span>
                </div>

                <div class="game-score">
                    <div class="team team-1">
                        <div class="name">
                            <span><?php echo $team_1; ?></span>								
                        </div>

                        <div class="score">
                            <span><?php echo $team_1_score; ?></span>								
                        </div>
                    </div>

                    <div class="team team-2">
                        <div class="name">
                            <span><?php echo $team_2; ?></span>								
                        </div>

                        <div class="score">
                            <span><?php echo $team_2_score; ?></span>								
                        </div>
                    </div>						
                </div>

                <div class="meta">
                    <span><?php echo $round; ?></span>					
                    <span><?php echo $date; ?> - <?php echo $time; ?><?php if($field): ?> | <?php echo $field; ?><?php endif;?></span>
                </div>							
            </div>

            <?php if($watch_link): ?>
                <div class="watch-link">
                    <a href="<?php echo $watch_link; ?>" rel="external">Watch</a>
                </div>
            <?php endif; ?>			
        </div>

    <?php endforeach; ?>
</div>