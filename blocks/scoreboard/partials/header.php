<div class="tournament-header">    
    <?php if(get_field('logo')): ?>
        <div class="logo">
            <img src="<?php $image = get_field('logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />					
        </div>
    <?php endif; ?>

    <div class="info">
        <div class="scoreboard-header">
            <h2><?php echo get_field('tournament_name'); ?></h2>
            <h3><?php echo get_field('location'); ?> | <?php echo get_field('dates'); ?></h3>
        </div>

        <?php if(have_rows('score_reporter_links')): ?>
            <div class="links">

                <div class="header">
                    <h4>Schedules and Results:</h4>
                </div>						

                <?php while(have_rows('score_reporter_links')): the_row(); ?>

                    <div class="link">
                        <a href="<?php echo get_sub_field('link'); ?>" rel="external"><?php echo get_sub_field('division'); ?></a>
                    </div>

                <?php endwhile; ?>

            </div>
        <?php endif; ?>
    </div>
</div>