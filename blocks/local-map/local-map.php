<?php

/*
 * Page Header Block Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'local-map-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'local-map usau-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

	<div class="headline blue align-center underline">
		<h3><?php echo get_field('section_header'); ?></h3>
	</div>
	
	<?php get_template_part('template-parts/local/map'); ?>

</section>