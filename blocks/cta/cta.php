<?php

/*
 * CTA Block Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'cta-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'btn';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}

if( $is_preview ) {
    $className .= ' is-admin';
}

$color = get_field('color');
if( $color ) {
    $className .= ' '. $color;
}

$corners = get_field('corners');
if( $corners ) {
    $className .= ' '. $corners;
}

$size = get_field('size');
if( $size ) {
    $className .= ' '. $size;
}

$wrapperClassName = 'cta-block usau-block';
if( !empty($block['align']) ) {
    $wrapperClassName .= ' align' . $block['align'];
}

?>


<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($wrapperClassName); ?>">

    <div class="cta">
        <a href="<?php echo get_field('link'); ?>" class="<?php echo esc_attr($className); ?>"<?php if(get_field('external')): ?> rel="external"<?php endif; ?>>
            <?php echo get_field('label'); ?>
        </a>
    </div>
    
</section>