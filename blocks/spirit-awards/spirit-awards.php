<?php

/*
 * Spirit Awards Block Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'spirit-awards' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'spirit-awards usau-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

$count = count(get_field('awards'));
$divisionsClassName = 'divisions';

if($count == 1) {
	$divisionsClassName .= ' one-col';
} elseif($count == 2) {
	$divisionsClassName .= ' two-col';
}

?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

    <div class="<?php echo esc_attr($divisionsClassName); ?>">
        <?php if(have_rows('awards')): while(have_rows('awards')): the_row(); $winner = get_sub_field('winner'); ?>
    
            <div class="award">
                <div class="winner">
                    <div class="info">
                        <div class="tagline">
                            <h5><?php echo get_sub_field('award_name'); ?></h5>
                        </div>

                        <div class="photo">
                            <div class="content">
                                <img src="<?php echo $winner['photo']['url']; ?>" alt="<?php echo $winner['photo']['alt']; ?>" />
                            </div>
                        </div>

                            <h4 class="name"><?php echo $winner['name']; ?></h4>
                            <h5 class="team"><?php echo $winner['team']; ?></h5>

                            <?php echo $winner['copy']; ?>

                    </div>
                </div>

                <?php if(get_sub_field('finalists')): ?>
                    <div class="finalists">
                        <div class="headline red">
                            <h5>Nominees</h5>
                        </div>

                        <div class="copy p4">
                            <p><?php echo get_sub_field('finalists'); ?></p>
                        </div>
                    </div>
                <?php endif; ?>
                
            </div>

        <?php endwhile; endif; ?>
    </div>

</section>

