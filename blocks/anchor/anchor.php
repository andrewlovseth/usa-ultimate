<?php

/*
 * Anchor Block Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'anchor-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'anchor-links';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

?>

<?php if($is_preview): ?>

	<div class="anchor anchor-block">

		<p><?php echo get_field('anchor'); ?></p>
		<a name="<?php echo get_field('anchor'); ?>"></a>

	</div>

<?php else: ?>

	<a id="<?php echo get_field('anchor'); ?>" class="anchor-point"></a>

<?php endif; ?>