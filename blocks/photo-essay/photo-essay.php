<?php

/*
 * Photo Essay Block Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'photo-essay' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'photo-essay usau-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
		
    <?php if(have_rows('sections')): while(have_rows('sections')) : the_row(); ?>

        <?php if( get_row_layout() == 'side_by_side' ): ?>

            <section class="photo-section side-by-side align-<?php echo get_sub_field('photo_alignment'); ?>">
                <div class="photo">
                    <img src="<?php $photo = get_sub_field('photo'); echo $photo['url']; ?>" alt="<?php echo $photo['alt']; ?>" />
                </div>

                <div class="caption">
                    <div class="caption-headline">
                        <h5><?php echo get_sub_field('caption_headline'); ?></h5>
                    </div>

                    <div class="caption-body copy p2">
                        <p><?php echo get_sub_field('caption_body'); ?></p>
                    </div>                
                </div>            
            </section>

        <?php endif; ?>

        <?php if( get_row_layout() == 'dual_photo' ): ?>

            <section class="photo-section dual-photo">
                <div class="photos">
                    <img src="<?php $image = get_sub_field('photo_1'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                    <img src="<?php $image = get_sub_field('photo_2'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                </div>

                <div class="caption">
                    <div class="caption-headline">
                        <h5><?php echo get_sub_field('caption_headline'); ?></h5>
                    </div>

                    <div class="caption-body copy p2">
                        <p><?php echo get_sub_field('caption_body'); ?></p>
                    </div>                
                </div>            
            </section>

        <?php endif; ?>

        <?php if( get_row_layout() == 'full_width' ): ?>

            <section class="photo-section full-width">
                <div class="photo">
                    <img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                </div>

                <div class="caption">
                    <div class="caption-headline">
                        <h5><?php echo get_sub_field('caption_headline'); ?></h5>
                    </div>

                    <div class="caption-body copy p2">
                        <p><?php echo get_sub_field('caption_body'); ?></p>
                    </div>                
                </div>            
            </section>

        <?php endif; ?>

    <?php endwhile; endif; ?>



</section>