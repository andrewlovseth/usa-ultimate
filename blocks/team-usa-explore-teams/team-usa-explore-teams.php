<?php

/*
 * Team USA Explore Teams Block Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'team-usa-explore-teams-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'team-usa-explore-teams';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

	<div class="photo<?php if(get_field('height') === 'half'): ?> half<?php endif; ?>">
		<div class="content">
			<img src="<?php $image = get_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

			<div class="info">
				<div class="info-wrapper">
					<div class="headline">
						<h1 class="cover-title"><?php echo get_field('headline'); ?></h1>
					</div>

					<div class="links">
						<?php if(have_rows('team_links')): while(have_rows('team_links')): the_row(); ?>

							<?php
								$link = get_sub_field('link'); if( $link ): 
							    $link_url = $link['url'];
							    $link_title = $link['title'];
							    $link_target = $link['target'] ? $link['target'] : '_self';
							?>
								<div class="cta">
									<a class="btn white rounded" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>	
								</div>						    	
							<?php endif; ?>
				
						<?php endwhile; endif; ?>
					</div>
				</div>
			</div>

		</div>
	</div>

</section>