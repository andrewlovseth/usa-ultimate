<?php

/*
 * Resources Block
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'resources-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'resources usau-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}



?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

	<?php if(get_field('section_header')): ?>

		<div class="headline blue underline">
			<h3><?php echo get_field('section_header'); ?></h3>
		</div>

	<?php endif; ?>

	<?php $posts = get_field('resources'); if( $posts ): ?>

		<div class="resources-grid">

			<?php foreach( $posts as $p): ?>

				<?php
					$file = get_field('file', $p->ID);
					$file_url = $file['url'];
					$file_data_modified = $file['modified'];

					$terms = get_the_terms($p->ID, 'resource_filters');
					$term = $terms[0];
				?>

				<div class="resource">
					<?php if($term): ?>
						<div class="cat">
							<span><?php echo $term->name; ?></span>		
						</div>
					<?php endif; ?>
					
					<div class="headline">
						<h5><a href="<?php echo $file_url; ?>" target="_window"><?php echo get_the_title($p->ID); ?></a></h5>
					</div>

					<?php if(get_field('description', $p->ID)): ?>
						<div class="copy p3">
							<?php echo get_field('description', $p->ID); ?>
						</div>
					<?php endif; ?>

					<?php if(get_field('external_link', $p->ID)): ?>

						<div class="url p4">
							<a href="<?php echo get_field('link', $p->ID); ?>" target="_window">Visit Site</a>
						</div>

					<?php else: ?>
						
						<div class="url p4">
							<a href="<?php echo $file_url; ?>" target="_window">View PDF</a>
						</div>

						<?php if($file_data_modified): ?>
							<div class="meta p4">
								<em>Last Modified: <?php echo $file_data_modified; ?></em>
							</div>
						<?php endif; ?>

					<?php endif; ?>

				</div>

			<?php endforeach; ?>

		</div>

	<?php endif; ?>

</section>