<?php

/*
 * Latest News Block Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'latest-news-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'latest-news usau-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

	<?php if(get_field('latest_news_header')): ?>
		<div class="headline blue underline">
			<h3><?php echo get_field('latest_news_header'); ?></h3>
		</div>
	<?php endif; ?>


	<div class="news-grid">		
		<?php
			if(get_field('category')) {
				$cat = get_field('category');
				$cat_slug = $cat->slug;

				$args = array(
					'post_type' => 'post',
					'posts_per_page' => 6,
					'category_name' => $cat_slug
				);
			} else {
				$args = array(
					'post_type' => 'post',
					'posts_per_page' => 6
				);
			}

			$query = new WP_Query( $args );
			if ( $query->have_posts() ) : $count = 0; while ( $query->have_posts() ) : $query->the_post(); ?>

			<article class="news-article-<?php echo $count; ?>">
				<a class="article-link" href="<?php the_permalink(); ?>">
					<div class="photo">
						<div class="content">
							<?php if($count > 0): ?>
								<?php $post_ID = get_the_ID(); if(get_field('square_photo_thumbnail', $post_ID)): ?>
									<img src="<?php $thumb = get_field('square_photo_thumbnail', $post_ID); echo $thumb['sizes']['medium']; ?>" alt="<?php echo $thumb['alt']; ?>" />
								<?php else: ?>
									<?php the_post_thumbnail('medium'); ?>
								<?php endif; ?>
							<?php else: ?>
								<?php the_post_thumbnail('medium'); ?>
							<?php endif; ?>							
						</div>							
					</div>

					<div class="info">
						<div class="info-wrapper">
							<div class="meta">
								<div class="date p3">
									<span class="day-month"><?php the_time('j M'); ?></span> <span class="year"><?php the_time(' Y'); ?></span>
								</div>
							</div>

							<div class="headline">
								<h4><?php the_title(); ?></h4>						
							</div>								
						</div>
					</div>
				</a>
			</article>

		<?php $count++; endwhile; endif; ?>
	</div>

	<?php
		$link = get_field('more_news_link'); if( $link ): 
		$link_url = $link['url'];
		$link_title = $link['title'];
		$link_target = $link['target'] ? $link['target'] : '_self';
	?>

		<div class="cta align-center">
			<a class="btn red" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
		</div>

	<?php endif; ?>

</section>