<?php

/*
 * People Grid Block Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'usau-people-grid-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'usau-people-grid usau-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    
    <div class="people-grid">

        <?php if(have_rows('people')): while(have_rows('people')): the_row(); ?>

            <?php $profile_slug = sanitize_title_with_dashes(get_sub_field('name')); ?>

            <div class="person">
                <a href="#<?php echo $profile_slug; ?>" class="profile-trigger">
                    <div class="photo thumbnail-photo">
                        <div class="content">
                            <img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                        </div>
                    </div>

                    <div class="info">
                        <div class="name">
                            <h4><?php echo get_sub_field('name'); ?></h4>
                        </div>

                        <div class="meta">
                            <em><?php echo get_sub_field('meta'); ?></em>
                        </div>
                    </a>
                </div>

                <section class="profile-overlay" id="<?php echo $profile_slug; ?>">
                    <div class="overlay">
                        <div class="overlay-wrapper">

                            <div class="info">
                                <div class="close">
                                    <a href="#" class="profile-close-btn close-btn"></a>
                                </div>

                                <div class="profile-wrapper">
                                    <div class="photo">
                                        <div class="content">
                                            <img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                                        </div>
                                    </div>

                                    <div class="info">
                                        <div class="name headline">
                                            <h5><?php echo get_sub_field('name'); ?></h5>
                                        </div>

                                        <div class="meta">
                                            <em><?php echo get_sub_field('meta'); ?></em>
                                        </div>

                                        <div class="biography copy p3">
                                            <?php echo get_sub_field('biography'); ?>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            
                        </div>
                    </div>
                </section>
            </div>

        <?php endwhile; endif; ?>
        
    </div>	


</section>