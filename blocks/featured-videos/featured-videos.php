<?php

/*
 * Featured Videos Block
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'featured-videos-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'featured-videos usau-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

	<section class="playlist">
		<div class="section-header headline blue underline">
			<h3><?php echo get_field('section_header'); ?></h3>
		</div>

		<?php $posts = get_field('videos'); if( $posts ): ?>
			<div class="videos">
				<?php foreach( $posts as $p ): ?>

					<?php 
						$args = ['post' => $p->ID];
						get_template_part('template-parts/watch/video', null, $args);
					?>

				<?php endforeach; ?>
			</div>
		<?php endif; ?>			
	</section>

	<?php 
		$link = get_field('cta');
		if( $link ): 
		$link_url = $link['url'];
		$link_title = $link['title'];
		$link_target = $link['target'] ? $link['target'] : '_self';
		?>

		<div class="cta align-center">
			<a class="btn red small" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
		</div>

	<?php endif; ?>
	
</section>