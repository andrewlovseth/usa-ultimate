<?php

/*
 * Page Header Block Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'page-header-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'page-header usau-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

	<?php if(get_field('headline')): ?>
		<div class="section-header headline blue underline">
			<h3><?php echo get_field('headline'); ?></h3>
		</div>
	<?php endif; ?>

	<?php if(get_field('deck')): ?>
		<div class="copy p2">
			<?php echo get_field('deck'); ?>
		</div>	
	<?php endif; ?>

</section>