<?php

/*
 * Three Columns Block Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'three-columns-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'three-columns usau-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}

if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

if( $is_preview ) {
    $className .= ' is-admin';
}

$count = count(get_field('columns'));
$columnsClassName = 'columns';

if($count == 3) {
	$columnsClassName .= ' three-col';
} elseif($count == 4) {
	$columnsClassName .= ' four-col';
}


?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

	<div class="<?php echo esc_attr($columnsClassName); ?>">

		<?php if(have_rows('columns')): $col = 1; while(have_rows('columns')): the_row(); ?>

			<?php
				$info = get_sub_field('info');
				$link = $info['cta'];
				if($link) {
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self';
				}

			?>

			<div class="column col-<?php echo $col; ?>">

				<?php if(get_sub_field('icon')): ?>
					<div class="icon">
						<img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>
				<?php endif; ?>

				<div class="info">
					<div class="headline blue">
						<?php if( $link ): ?>
							<h4><a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo $info['headline']; ?></a></h4>
						<?php else: ?>
							<h4><?php echo $info['headline']; ?></h4>
						<?php endif; ?>
					</div>

					<?php if($info['deck']): ?>
						<div class="copy p3">
							<?php echo $info['deck']; ?>
						</div>
					<?php endif; ?> 
					
					<?php if( $link ): ?>

						<div class="cta">
							<a class="underline red" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
						</div>

					<?php endif; ?>

				</div>
			</div>

		<?php $col++; endwhile; endif; ?>
		
	</div>
		
</section>