<?php

/*
 * Streaming Schedule Block Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'streaming-schedule-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'streaming-schedule usau-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

    <?php if(have_rows('days')): while(have_rows('days')): the_row(); ?>
    
        <?php
            $day_slug = sanitize_title_with_dashes(get_sub_field('date'));
            $spreadsheet_id = get_sub_field('sheet_id');
            $tab_id = get_sub_field('tab_id');
            $sheet = get_sub_field('date');

            if(get_transient('USAU_streaming-schedule-' . $day_slug)) {
                $streaming_dates = get_transient('USAU_streaming-schedule-' . $day_slug);
            } else {
                $streaming_dates = usau_get_sheet_data('USAU_streaming-schedule-' . $day_slug, $spreadsheet_id, 600, $sheet);
            }

            if(!empty($streaming_dates)): ?>

                <div class="day">
                    <div class="section-header headline red underline">
                        <h4><?php echo get_sub_field('date'); ?></h4>
                    </div>

                    <table class="streaming-schedule-table">
                        <thead>
                            <tr>
                                <th class="division">Division</th>
                                <th class="match-up">Match-up</th>
                                <th class="time">Time (ET)</th>
                                <th class="watch">Watch</th>
                            </tr>
                        </thead>

                        <tbody>

                            <?php
                                foreach ($streaming_dates as $streaming_date): 
                                    $date = $streaming_date[0];
                                    $division = $streaming_date[1];
                                    $match_up = $streaming_date[2];
                                    $time = $streaming_date[3];
                                    $platform = $streaming_date[4];
                                    $link = $streaming_date[5];
                            ?>

                                <tr class="game" data-division="<?php echo sanitize_title_with_dashes($division); ?>">
                                    <td class="division"><span class="label">Division:</span><?php echo $division; ?></td>
                                    <td class="match-up"><span class="label">Match-Up:</span><?php echo $match_up; ?></td>
                                    <td class="time"><span class="label">Time:</span><?php echo $time; ?></td>
                                    <td class="watch">
                                        <?php if($link): ?>
                                            <a href="<?php echo $link; ?>" rel="external"><?php echo $platform; ?></a>
                                        <?php else: ?>
                                            <?php echo $platform; ?>
                                        <?php endif; ?>
                                    </td>								
                                </tr>

                            <?php endforeach; ?>

                        </tbody>
                    </table>
                </div>

        <?php else: ?>

            <div class="day copy p2 error-message">
                <p>No events scheduled.</p>
            </div>

        <?php endif; ?>    

    <?php endwhile; endif; ?>

</section>