<?php

/*
 * Roster Logs Block Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'roster-logs-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'roster-logs usau-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

$spreadsheet_id = get_field('google_sheet_id');


?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
		
	<div class="section-header">
		<div class="headline blue underline align-center">
			<h3><?php echo get_field('headline'); ?></h3>
		</div>			

		<div class="copy p2">
			<?php echo get_field('copy'); ?>
		</div>
	</div>

	<?php if(!$is_preview): ?>

		

		<div class="logs">
			<div class="tab-links">
				<?php if(have_rows('logs')): while(have_rows('logs')) : the_row(); ?>
					<?php if( get_row_layout() == 'log' ): ?>
						<?php $gender_division_slug = sanitize_title_with_dashes(get_sub_field('gender_division')); ?>

						<a href="#<?php echo $gender_division_slug; ?>"><?php echo get_sub_field('gender_division'); ?></a>

					<?php endif; ?>
				<?php endwhile; endif; ?>
			</div>
			
			<div class="tabs">
				<?php if(have_rows('logs')): while(have_rows('logs')) : the_row(); ?>

					<?php if( get_row_layout() == 'log' ): ?>
						<?php $gender_division = sanitize_title_with_dashes(get_sub_field('gender_division')); ?>

						<div class="tab" id="<?php echo $gender_division;  ?>">

							<?php 
								$tab_id = get_sub_field('tab_id');
								if(get_transient('USAU_roster-log-' . $gender_division . '-' . $block['id'])) {
									$entries = get_transient('USAU_roster-log-' . $gender_division . '-' . $block['id']);
								} else {
									$entries = usau_get_sheet_data('USAU_roster-log-' . $gender_division . '-' . $block['id'], $spreadsheet_id, HOUR_IN_SECONDS, $tab_id);
								}
							?>

							<div class="division division-tab">
								<div class="division-header headline blue align-center">
									<h4><?php echo get_sub_field('gender_division'); ?></h4>
								</div>
								
								<table class="roster-log-table">
									<thead>
										<tr>
											<th class="team">Team</th>
											<th class="arrival-date">Arrival Date</th>
											<th class="status">Status</th>
											<th class="region">Region</th>
											<th class="conference">Conference</th>
											<th class="division">Division</th>
										</tr>
									</thead>

									<tbody>
										<?php
											foreach ($entries as $entry):
												$team = $entry[0];
												$arrival_date = $entry[1];
												$status = $entry[2];
												$region = $entry[3];
												$conference = $entry[4];
												$division = $entry[5];
										?>

											<tr class="status-<?php echo $status; ?>">
												<td class="team"><?php echo $team; ?></td>
												<td class="arrival-date"><?php echo $arrival_date; ?></td>
												<td class="status"><?php echo $status; ?></td>
												<td class="region"><?php echo $region; ?></td>
												<td class="conference"><?php echo $conference; ?></td>
												<td class="division"><?php echo $division; ?></td>
											</tr>

										<?php endforeach; ?>

									</tbody>
								</table>

							</div>
							
						</div>

					<?php endif; ?>

				<?php endwhile; endif; ?>
			</div>
		</div>

	<?php endif; ?>

</section>