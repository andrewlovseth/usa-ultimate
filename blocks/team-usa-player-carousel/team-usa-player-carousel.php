<?php

/*
 * Team USA Player Carousel Block Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'team-usa-player-carousel-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'team-usa-player-carousel';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">	
	<div class="section-header headline blue align-center underline">
		<h3><?php echo get_field('section_header'); ?></h3>
	</div>

	<div class="players">
		<?php $posts = get_field('players'); if( $posts ): ?>
			<?php foreach( $posts as $p ): ?>

			    <div class="player">
			    	<div class="content">

				    	<a href="<?php echo get_permalink( $p->ID ); ?>">
					    	<div class="photo">
					    		<img src="<?php $image = get_field('headshot', $p->ID); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />				    		
					    	</div>

					    	<div class="info">
					    		<h3><?php echo get_the_title( $p->ID ); ?></h3>
					    		<h4><span><?php echo get_field('current_residence', $p->ID); ?></span></h4>
					    	</div>
				    	</a>
			    		
			    	</div>
			    </div>

			<?php endforeach; ?>
		<?php endif; ?>		
	</div>
</section>



