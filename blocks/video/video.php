<?php

/*
 * USAU Video Block Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'usau-video-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'usau-video usau-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

$status = get_field('status');
$event = get_field('event');
$title = get_field('title');
$description = get_field('description');
$youtube_id = get_field('youtube_id');
$on_off = get_field('on_off');

if($on_off == TRUE): 

?>

    <section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
        <?php if( !$is_preview ): ?>
            <div class="video">
                <div class="content">
                    <iframe  width="1920" height="1080" src="https://www.youtube.com/embed/<?php echo $youtube_id; ?>?autoplay=1" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        <?php endif; ?>

        <div class="info">
            <div class="headline white underline">
                <div class="meta">
                    <div class="status">
                        <h5><?php echo $status; ?></h5>
                    </div>

                    <div class="event">
                        <p><?php echo $event; ?></p>
                    </div>
                </div>
                
                <h3><?php echo $title; ?></h3>
            </div>
            <div class="copy p2">
                <?php echo $description; ?>
            </div>
        </div>
    </section>

<?php endif; ?>