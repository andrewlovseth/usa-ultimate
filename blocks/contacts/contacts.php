<?php

/*
 * Contacts Block
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'contacts-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'contacts usau-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

	<?php if(get_field('section_header')): ?>

		<div class="section-header headline blue underline">
			<h3><?php echo get_field('section_header'); ?></h3>
		</div>

	<?php endif; ?>

	<?php if(have_rows('contact_group')): while(have_rows('contact_group')): the_row(); ?>

		<?php
			$spreadsheet_id = get_sub_field('sheet_id');
			$sheet = get_sub_field('index');
			$title = sanitize_title_with_dashes(get_sub_field('title'));
			$link_label = get_sub_field('link_label');

			$transient_name = 'USAU_contacts-' . $title . '-' .  $block['id'];
            if(get_transient($transient_name)) {
                $entries = get_transient($transient_name);
            } else {
                $entries = usau_get_sheet_data($transient_name, $spreadsheet_id, DAY_IN_SECONDS, $sheet);
            }

			if(!empty($entries)): ?>

			<div class="section contacts-list">	
				<div class="headline red contacts-header">
					<h4><?php echo get_sub_field('title'); ?></h4>
				</div>

				<table class="contact-table">
					<thead>
						<tr>
							<?php if(have_rows('columns')): while(have_rows('columns')): the_row(); ?>
								
								<th class="<?php echo get_sub_field('slug'); ?>"><?php echo get_sub_field('name'); ?></th>
								<?php
									if(get_sub_field('name') == 'Name') {
										$name_col = $col;
									}
								?>
							<?php endwhile; endif; ?>											
						</tr>
					</thead>

					<tbody>
						<?php foreach ($entries as $entry): ?>

							<?php
								$trClassName = "contacts-row";                        
								if($entry[$name_col] == 'Vacant') {
									$trClassName .= ' vacant';
								}
							?>

							<tr class="<?php echo $trClassName; ?>">
								<?php $i = 0; if(have_rows('columns')): while(have_rows('columns')): the_row(); ?>
									<?php
										$slug = get_sub_field('slug');
										$datum = $entry[$i];
									?>

									<?php if($slug == "email"): ?>
									
										<td class="<?php echo get_sub_field('slug'); ?>"><a href="mailto:<?php echo $datum; ?>"><?php echo $datum; ?></a></td>
									
									<?php elseif($slug == "link"): ?>

										<td class="<?php echo get_sub_field('slug'); ?>">
											<a href="<?php echo $datum; ?>">
												<?php if($link_label): ?>
                                                    <?php echo $link_label; ?>
                                                <?php else: ?>
                                                    Link
                                                <?php endif; ?>  
											</a>
										</td>

									<?php else: ?>

										<td class="<?php echo get_sub_field('slug'); ?>"><?php echo $datum; ?></td>
										
									<?php endif; ?>
									
								<?php $i++; endwhile; endif; ?>													
							</tr>					

						<?php endforeach; ?>

					</tbody>
				</table>
			</div>

		<?php else: ?>

			<div class="section contacts-list none">
				<p>No contact information available at this time.</p>
			</div>

		<?php endif; ?>


	<?php endwhile; endif; ?>

</section>