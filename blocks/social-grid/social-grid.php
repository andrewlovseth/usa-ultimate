<?php

/*
 * Social Grid Block Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'social-grid-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'social-grid usau-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

	<div class="section-header headline blue underline">
		<h3><?php echo get_field('social_grid_header'); ?></h3>
	</div>



</section>