<?php

/*
 * Cover Block Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'cover-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'cover';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

$color = get_field('color_overlay');
$photoClassName = 'photo';

if(get_field('height') == 'half') {
    $photoClassName .= ' half';
}

if($color) {
	if($color != 'none')  {
		$photoClassName .= ' color-overlay ' . $color;
	}
}


?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

	<div class="<?php echo esc_attr($photoClassName); ?>">
		<div class="content">
			<img src="<?php $image = get_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

			<div class="info">
				<div class="info-wrapper">
					<div class="headline">
						<h1 class="cover-title"><?php echo get_field('headline'); ?></h1>
					</div>

					<?php if(get_field('deck')): ?>
						<div class="copy p2">
							<?php echo get_field('deck'); ?>
						</div>
					<?php endif; ?>
					
					<?php if(get_field('cta_type') == 'multiple'): ?>
						<div class="cta multiple">
							<?php if(have_rows('ctas')): while(have_rows('ctas')): the_row(); ?>

								<?php
									$link = get_sub_field('link'); if( $link ): 
									$link_url = $link['url'];
									$link_title = $link['title'];
									$link_target = $link['target'] ? $link['target'] : '_self';
								?>

									<a class="btn white" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>								    	
								<?php endif; ?>
								
							<?php endwhile; endif; ?>
						</div>

					<?php else: ?>
						
						<?php
							$link = get_field('cta'); if( $link ): 
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
						?>
						
							<div class="cta">
								<a class="btn white" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>	
							</div>			

						<?php endif; ?>	

					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>

</section>