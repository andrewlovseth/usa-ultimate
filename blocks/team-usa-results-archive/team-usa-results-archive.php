<?php

/*
 * Team USA Results Archive Block
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'team-usa-results-archive-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'team-usa-results-archive usau-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

	<?php if(get_field('section_header')): ?>

		<div class="section-header headline blue align-center underline uppercase">
			<h3><?php echo get_field('section_header'); ?></h3>
		</div>

	<?php endif; ?>

	<?php if(have_rows('event_archive')): ?>

		<?php while(have_rows('event_archive')) : the_row(); ?>

			<?php if( get_row_layout() == 'section' ): ?>

				<?php
					$event = get_sub_field('event');
					$event_value = $event['value'];
					$event_display_value = $event['label'];
					include(locate_template('template-parts/team-usa/event-archive.php', false, false));
				?>
				
			<?php endif; ?>
		
		<?php endwhile; ?>

	<?php endif; ?>

</section>