<?php

/*
 * Full Width Ad Features Block Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'full-width-graphic-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'full-width-graphic usau-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

	<div class="graphic">
		<a href="<?php echo get_field('link'); ?>" rel="external">
			<img src="<?php $image = get_field('image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		</a>
	</div>
		
</section>