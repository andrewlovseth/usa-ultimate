<?php

/*
 * Media Carousel Block Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'media-carousel-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'media-carousel usau-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

	<div class="section-header">
		<div class="headline blue section-header align-center">
			<h4><?php echo get_field('section_header'); ?></h4>
		</div>		
	</div>
	
	<?php if(have_rows('media_carousel')): ?>

		<div class="slides-wrapper">
			<div class="slides">
				<?php while(have_rows('media_carousel')): the_row(); ?>
			
					<div class="slide">
						<?php if(have_rows('media')): while(have_rows('media')) : the_row(); ?>
						
							<?php if( get_row_layout() == 'photo' ): ?>
								
								<div class="media photo">
									<div class="content">
										<img src="<?php $image = get_sub_field('photo'); echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
									</div>								
								</div>
								
							<?php endif; ?>

							<?php if( get_row_layout() == 'video' ): ?>
								
								<div class="media video">
									<?php echo get_sub_field('video'); ?>
								</div>
								
							<?php endif; ?>

						<?php endwhile; endif; ?>

						<?php if(get_sub_field('caption') || get_sub_field('caption_headline')): ?>
							<div class="caption">	
								<?php if(get_sub_field('caption_headline')): ?>
									<div class="headline">
										<h4><?php echo get_sub_field('caption_headline'); ?></h4>
									</div>
								<?php endif; ?>	

								<?php if(get_sub_field('caption')): ?>
									<div class="copy p2">
										<?php echo get_sub_field('caption'); ?>
									</div>
								<?php endif; ?>			    	
							</div>
						<?php endif; ?>		    		
					</div>

				<?php endwhile; ?>				
			</div>
		</div>



	<?php endif; ?>

</section>