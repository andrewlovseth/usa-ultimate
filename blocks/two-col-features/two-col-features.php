<?php

/*
 * Two Col Features Block Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'two-col-features-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'two-col-features usau-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
		
	<?php if(have_rows('features')): while(have_rows('features')): the_row(); ?>
		
		<div class="feature align-<?php echo get_sub_field('photo_align'); ?>">
			<?php if(get_sub_field('show_video')): ?>
				<div class="photo video">
					<a href="#" data-video-id="<?php echo get_sub_field('video_id'); ?>" class="video-trigger discover-play-btn">
						<span class="icon"></span>
						<span class="thumbnail">
							<img src="<?php $image = get_sub_field('video_thumbnail'); echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
						</span>		
					</a>				
				</div>
			<?php else: ?>
				<div class="photo">
					<img src="<?php $image = get_sub_field('photo'); echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>
			<?php endif; ?>

			<div class="info">
				<?php $info = get_sub_field('info'); ?>

				<?php if($info['sub_headline']): ?>
					<div class="headline sub-headline red">
						<h6><?php echo $info['sub_headline']; ?></h6>
					</div>
				<?php endif; ?>

				<div class="headline main-headline blue underline">
					<h3><?php echo $info['headline']; ?></h3>
				</div>

				<div class="copy p2">
					<?php echo $info['deck']; ?>
				</div>

				<?php
					$link = $info['cta']; if( $link ): 
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self';

					$btnClassName = 'btn red small';
					if( get_sub_field('show_video') && $link_url == "") {
						$btnClassName .= ' video-trigger';
					}
					
					if(get_sub_field('show_video') && $link_url == "" ) {
						$btnData = ' data-video-id="' . get_sub_field('video_id') . '"';
					} else {
						$btnData = "";
					}

				?>
					<div class="cta">
						<a class="<?php echo $btnClassName; ?>" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"<?php echo $btnData; ?>>
							<?php echo esc_html( $link_title ); ?>
						</a>	
					</div>						    	
				<?php endif; ?>
									
			</div>
		</div>

	<?php endwhile; endif; ?>

</section>