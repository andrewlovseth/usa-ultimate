<?php

/*
 * Info with Counter Block Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'info-with-counter-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'info-with-counter usau-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

	<div class="section-header headline blue align-center underline">
		<h3><?php echo get_field('section_header'); ?></h3>
	</div>
	
	<?php if(have_rows('features')): while(have_rows('features')): the_row(); ?>
		
		<div class="feature<?php if(get_sub_field('counter') == ''): ?> no-counter<?php endif; ?>">
			<?php if(get_sub_field('photo')): ?>
				<div class="photo">
					<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>
			<?php endif; ?>

			<?php if(get_sub_field('counter')): ?>
				<div class="counter">
					<span class="count"><?php echo get_sub_field('counter'); ?></span>
				</div>
			<?php endif; ?>

			<div class="info">
				<?php $info = get_sub_field('info'); ?>

				<div class="headline blue">
					<h4><?php echo $info['headline']; ?></h4>
				</div>

				<div class="copy p2">
					<?php echo $info['deck']; ?>
				</div>
				
			</div>
		</div>

	<?php endwhile; endif; ?>

</section>