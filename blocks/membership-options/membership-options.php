<?php

/*
 * Membership Options Block Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'membership-options-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'membership-options usau-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

	<?php if(get_field('section_header')): ?>

		<div class="header">
			<h2><?php echo get_field('section_header'); ?></h2>

			<?php if(get_field('section_deck')): ?>
				<div class="copy p2">
					<?php echo get_field('section_deck'); ?>
				</div>
			<?php endif; ?>
		</div>

	<?php endif; ?>

	<?php if(have_rows('options')): ?>

		<div class="options">

			<?php $i = 1; while(have_rows('options')): the_row(); ?>
	
				<div class="option option-<?php echo $i; ?>">
					<div class="content">
						<a href="<?php echo get_sub_field('link'); ?>" rel="external">
							<div class="info">
								<div class="info-wrapper">

									<div class="headline">
										<h3>
											<?php echo get_sub_field('name'); ?>
											<?php if(get_sub_field('description')): ?>
												<span class="description"><?php echo get_sub_field('description'); ?></span>
											<?php endif; ?>
										</h3>
									</div>

									<?php $price = get_sub_field('price'); ?>

									<div class="price">
										<h4>$<?php echo $price['one_year']; ?></h4>

										<div class="price-options">
											<?php if($price['three_years']): ?>
												<div class="price-option three">
													<h5><strong>3 Years:</strong> $<?php echo $price['three_years']; ?></h5>
												</div>
											<?php endif; ?>

											<?php if($price['five_years']): ?>
												<div class="price-option five">
													<h5><strong>5 Years:</strong> $<?php echo $price['five_years']; ?></h5>
												</div>
											<?php endif; ?>							    				
										</div>
									</div>	
									
								</div>
							</div>

							<div class="photo">
								<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							</div>
							<?php echo get_sub_field('sub_field_1'); ?>
						</a>
						
					</div>

				</div>

			<?php $i++; endwhile; ?>
		</div>

	<?php endif; ?>

</section>