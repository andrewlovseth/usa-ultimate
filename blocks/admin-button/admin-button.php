<?php

/*
 * Admin Button Block Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'admin-button-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'hub-navigation usau-block admin-button';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

?>

<nav id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
	<div class="hub-navigation-wrapper">
        <div class="admin-link">
        
            <?php
                $link = get_field('admin_link'); if( $link ): 
                $link_url = $link['url'];
                $link_title = $link['title'];
                $link_target = $link['target'] ? $link['target'] : '_self'; ?>

                <a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                    <span class="label"><?php echo esc_html( $link_title ); ?></span>
                </a>								    	

            <?php endif; ?>				

        </div>				
	</div>
</nav>