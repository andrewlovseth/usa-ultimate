<?php
    $args = wp_parse_args($args);
	
	if(!empty($args)) {
        $rankings = $args['rankings']; 
    }
?>

<table class="rankings-table">
	<thead>
		<tr>
			<th class="rank">Rank</th>
			<th class="team">Team</th>
			<th class="record">Record</th>
			<th class="rating">Rating</th>
			<th class="region">Region</th>
			<th class="trend">Change</th>
		</tr>
	</thead>

	<tbody class="p3">
		<?php

			foreach ($rankings as $entry): 
				$rank = $entry[0];
				$team = $entry[1];
				$wins = $entry[2];
				$losses = $entry[3];
				$rating = $entry[4];
				$region = $entry[5];

				if (array_key_exists(6, $entry)) {
					$trend = intval($entry[6]);
				} else {
					$trend = NULL;
				}

				if (array_key_exists(7, $entry)) {
					$bid = $entry[7];
				} else {
					$bid = '';
				}

				if (array_key_exists(8, $entry)) {
					$team_link = $entry[8];
				} else {
					$team_link = NULL;
				}


				
		?>

			<tr<?php if($bid != "") { echo ' class="bid"'; }?>>
				<td class="rank"><span class="bid bid-<?php echo $bid; ?>"><?php echo $rank; ?></span></td>
				<td class="team">
					<?php if($team_link): ?>
						<a href="<?php echo $team_link; ?>" rel="external"><?php echo $team; ?></a>
					<?php else: ?>
						<?php echo $team; ?>
					<?php endif; ?>						
				</td>
				<td class="record"><?php echo $wins; ?> - <?php echo $losses; ?></td>
				<td class="rating"><?php echo $rating; ?></td>
				<td class="region"><?php echo $region; ?></td>
				<td class="trend" data-trend="<?php echo $trend; ?>">
					<?php if($trend >= 1): ?>
						<span class="positive"><?php echo $trend; ?></span>
					<?php elseif($trend <= -1): ?>
						<span class="negative"><?php echo abs($trend); ?></span>
					<?php else: ?>
						<span class="no-change">-</span>
					<?php endif; ?>
				</td>
			</tr>

		<?php endforeach; ?>

	</tbody>
</table>