<div class="headline section-header blue uppercase align-center">
    <h3><?php echo get_field('title'); ?></h3>
</div>

<div class="legend p4">
    <div class="bid auto">
        <span>Automatic Bid</span>
    </div>
    
    <div class="bid wildcard">
        <span>Wildcard Bid</span>
    </div>
</div>