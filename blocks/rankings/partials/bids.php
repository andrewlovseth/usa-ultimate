<?php
    $args = wp_parse_args($args);
    if(!empty($args)) {
        $bids = $args['bids']; 
    }
?>

<div class="bid-allocation">
    <div class="headline blue">
        <h5>Championship Bid Allocation</h5>
    </div>

    <div class="regions p3">			
        <?php foreach ($bids as $bid):  ?>

            <?php 
                $region = $bid[0];
                $number = $bid[1];
            ?>

            <div class="region">
                <div class="name">
                    <span><?php echo $region; ?></span>
                </div>

                <div class="number">
                    <span><?php echo $number; ?></span>
                </div>
            </div>		

        <?php endforeach; ?>
    </div>
</div>