<div class="copy p4">
    <p><?php echo get_field('last_updated'); ?></p>
</div>

<?php if(get_field('rankings_link')): ?>
    <div class="cta align-center">
        <a href="<?php echo get_field('rankings_link'); ?>" class="btn red rounded" rel="external">Full Rankings</a>
    </div>
<?php endif; ?>