<?php

/*
 * Rankings Block Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'rankings-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'rankings';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}



$spreadsheet_id = get_field('sheet_id');
$rankings_tab = get_field('tab_index');
$bids_tab = get_field('bid_allocation_tab_index');

$rankings_transient_name = 'USAU_rankings-' . $block['id'];
if(get_transient($rankings_transient_name)) {
    $rankings = get_transient($rankings_transient_name);
} else {
    $rankings = usau_get_sheet_data($rankings_transient_name, $spreadsheet_id, DAY_IN_SECONDS, $rankings_tab);
}

$bids_transient_name = 'USAU_bids-' . $block['id'];
if(get_transient($bids_transient_name)) {
    $bids = get_transient($bids_transient_name);
} else {
    $bids = usau_get_sheet_data($bids_transient_name, $spreadsheet_id, DAY_IN_SECONDS, $bids_tab);
}

?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

	<?php get_template_part('blocks/rankings/partials/header'); ?>

    <?php 
        $args = ['rankings' => $rankings];
        get_template_part('blocks/rankings/partials/table', null, $args);
    ?>

    <?php 
        $args = ['bids' => $bids];
        get_template_part('blocks/rankings/partials/bids', null, $args);
    ?>

	<?php get_template_part('blocks/rankings/partials/meta'); ?>

</section>