<?php

/*
 * Hero Video Block Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'hero-video-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'hero-video';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

	<div class="photo">
		<div class="content">
			<div class="video">
				<div class="content">
					<iframe style="background-color: #000;" id="background-video" src="https://player.vimeo.com/video/<?php echo get_field('background_video_id'); ?>/?background=1&quality=1080p&transparent=0" frameborder="0" allow="autoplay"></iframe>		
				</div>
			</div>

			<div class="info">
				<div class="usau-block">
					<div class="headline">
						<h1 class="cover-title"><?php $coverTitle = get_field('headline'); echo $coverTitle; ?></h1>
					</div>

					<?php if(have_rows('link_options')): while(have_rows('link_options')) : the_row(); ?>

						<?php if( get_row_layout() == 'basic_link' ): ?>

							<div class="cta">
								<?php
									$link = get_sub_field('cta'); if( $link ): 
									$link_url = $link['url'];
									$link_title = $link['title'];
									$link_target = $link['target'] ? $link['target'] : '_self';
								?>

									<a class="btn white rounded" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>								    	
								<?php endif; ?>		
							</div>

						<?php endif; ?>

						<?php if( get_row_layout() == 'video_modal' ): ?>

							<div class="cta play-btn">
								<a class="btn white play-btn rounded<?php if(!$is_preview): ?> video-trigger<?php endif; ?>" href="#" data-video-id="<?php echo get_sub_field('video_id'); ?>" data-title="<?php echo strip_tags($coverTitle); ?>">Play Video</a>			
							</div>


						<?php endif; ?>

					<?php endwhile; endif; ?>
				</div>
			</div>
		</div>
	</div>

	<?php if(!$is_preview): ?>
		<script src="https://player.vimeo.com/api/player.js"></script>
		<script>

			(function ($, window, document, undefined) {	

				var iframe = $('#background-video');
				var player = new Vimeo.Player(iframe);

				$(document).ready(function($) {
					$('.play-btn.video-trigger').on('click', function() {
						player.pause();
					});

					$('.video-close-btn').on('click', function() {
						player.play();
					});
				});

				$(document).keyup(function(e) {				
					if (e.keyCode == 27) {
						player.play();
					}
				});

				$(document).mouseup(function(e) {
					var video_player = $('.video-overlay-open #video-overlay .info');
				
					if (!video_player.is(e.target) && video_player.has(e.target).length === 0) {
						player.play();
					}
				});


			})(jQuery, window, document);

		</script>
	<?php endif; ?>

</section>