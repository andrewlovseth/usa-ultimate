<?php

/*
 * Rankings Block Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'html-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'html usau-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

$url = get_field('url');
$title = get_field('document_name');
$stylesheet = get_field('stylesheet');

$html = usau_get_html_cache($url, $title);

?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

    <?php echo $html; ?>

</section>