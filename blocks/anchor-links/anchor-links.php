<?php

/*
 * Anchor Links Block Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'anchor-links-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'anchor-links usau-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

?>

<nav id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
	<div class="nav-flexbox">
		<div class="nav-label">
			<h4><?php echo get_field('nav_label'); ?></h4>
		</div>

		<div class="links">
			<?php if(have_rows('links')): while(have_rows('links')): the_row(); ?>
				
				<div class="link">
					<a href="#<?php echo get_sub_field('anchor'); ?>" class="smooth"><?php echo get_sub_field('label'); ?></a>
				</div>

			<?php endwhile; endif; ?>
		</div>			
	</div>
</nav>