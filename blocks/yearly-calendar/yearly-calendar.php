<?php

/*
 * Yearly Calendar Block Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'yearly-calendar-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'usau-block schedule yearly-calendar';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

$spreadsheet_id = get_field('sheet_id');
$tab_id = get_field('tab_id');

$transient_name = 'USAU_yearly-calendar-' . $block['id'];
if(get_transient($transient_name)) {
    $entries = get_transient($transient_name);
} else {
    $entries = usau_get_sheet_data($transient_name, $spreadsheet_id, 600);
}

?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

    <?php if(get_field('headline')): ?>
        <div class="section-header headline align-center underline blue">
            <h3><?php echo get_field('headline'); ?></h3>
        </div>
    <?php endif; ?>

    <?php if(have_rows('years')): while(have_rows('years')): the_row(); 
        $display_year = get_sub_field('name');
        $slug = get_sub_field('slug');
    ?>
    
    <section class="year <?php echo $slug; ?>">

        <?php
            $allowed = [$slug];
            $events = array_filter(
                $entries,
                function ($entry) use ($allowed) {
                    return in_array($entry[0], $allowed);
                }
            );
            
            if(!empty($events)): ?>
                <div class="year__header headline blue underline uppercase">
                    <h4><?php echo $display_year; ?></h4>
                </div>
                
                <div class="events events-wrapper">
                    <div class="events__header">
                        <?php if(have_rows('columns')): while(have_rows('columns')): the_row(); ?>                        
                            <div class="<?php echo get_sub_field('slug'); ?>">
                                <strong><?php echo get_sub_field('name'); ?></strong>
                            </div>
                        <?php endwhile; endif; ?>
                    </div>

                    <?php foreach ($events as $event): ?>

                        <?php 
                            $year = $event[0];
                            if($year == $slug): ?>
                            <div class="event-listing">
                                <?php if(have_rows('columns')): $i = 1; while(have_rows('columns')): the_row(); ?>
                                    <div class="<?php echo $slug; ?>">
                                        <?php if(filter_var($event[$i], FILTER_VALIDATE_URL)): ?>
                                            <a href="<?php echo $event[$i]; ?>" rel="external">
                                                <?php if(get_field('link_label')): ?>
                                                    <?php echo get_field('link_label'); ?>
                                                <?php else: ?>
                                                    Link
                                                <?php endif; ?>
                                            </a>
                                        <?php elseif(filter_var($event[$i], FILTER_VALIDATE_EMAIL)): ?>
                                            <a href="mailto:<?php echo $event[$i]; ?>" rel="external">Email</a>
                                        <?php else: ?>
                                            <span><?php echo $event[$i]; ?></span>
                                        <?php endif; ?>  
                                    </div>
                                <?php $i++; endwhile; endif; ?>
                            </div>                                    
                        <?php endif; ?>

                    <?php endforeach; ?>       

                </div>
            <?php endif; ?>
        </section>

    <?php endwhile; endif; ?>

</section>