<?php if(have_rows('results')): ?>

    <div class="results">
        <div class="results-header headline blue">
            <h4>Results</h4>
        </div>

        <div class="game-list">
            <?php while(have_rows('results')): the_row(); ?>
        
                <div class="game">
                    <div class="usa team">USA</div>

                    <div class="score">
                        <div class="usa-score"><?php echo get_sub_field('usa_score'); ?></div>
                        <div class="divider">-</div>
                        <div class="opponent-score"><?php echo get_sub_field('opponent_score'); ?></div>
                    </div>

                    <div class="opponent team"><?php echo get_sub_field('opponent'); ?></div>
                </div>

            <?php endwhile; ?>
        </div>
    </div>

<?php endif; ?>