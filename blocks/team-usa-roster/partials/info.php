<div class="info">
    <div class="meta">
        <div class="location">
            <h2><?php echo get_field('location'); ?></h2>
        </div>

        <?php if(get_field('finish')): ?>
            <div class="result">
                <?php if(get_field('medal')): ?>
                    <div class="medal">
                        <img src="<?php bloginfo('template_directory'); ?>/images/<?php echo get_field('medal'); ?>.svg" alt="<?php echo get_field('medal'); ?>">
                    </div>
                <?php endif; ?>
                <h3><?php echo get_field('finish'); ?> Place</h3>
            </div>
        <?php endif; ?>

        <?php if(get_field('photo')): ?>
            <div class="photo">
                <img src="<?php $image = get_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
            </div>
        <?php endif; ?>
    </div>
</div>