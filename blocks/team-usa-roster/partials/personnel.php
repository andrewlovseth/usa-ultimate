<?php if(have_rows('personnel')):
    
$rows = get_field('personnel');
$layout = $rows[0]['layout'];

if($layout == 'list'){
    $layoutClass = "lists";
}

?>

    <section class="personnel <?php echo $layoutClass; ?>">

        <?php while(have_rows('personnel')) : the_row(); ?>
            
            <?php if( get_row_layout() == 'world_games_group' ): ?>

                <div class="personnel-group world-games-group">
                    <div class="headline personnel-header blue underline">
                        <h4><?php echo get_sub_field('group_label'); ?></h4>
                    </div>

                    <div class="grid four-col">

                        <?php $people = get_sub_field('people'); if( $people ): ?>
                            <?php foreach( $people as $person ):?>
                                <?php
                                    $headshot = get_field('headshot', $person->ID); 
                                ?>
                                <div class="grid-item person">
                                    <div class="photo">
                                        <?php if($headshot): ?>
                                            <a href="<?php echo get_permalink( $person->ID ); ?>">
                                                <?php echo wp_get_attachment_image($headshot['ID'], 'full'); ?>
                                            </a>
                                        <?php else: ?>
                                            <div class="no-photo"></div>
                                        <?php endif; ?>
                                    </div>

                                    <div class="info">
                                        <div class="name headline">
                                            <h5><a href="<?php echo get_permalink( $person->ID ); ?>"><?php echo get_the_title( $person->ID ); ?></a></h5>
                                        </div>

                                        <div class="meta p3">
                                            <p><?php echo get_field('current_residence',  $person->ID); ?></p>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>	
                </div>	
                
            <?php endif; ?>

            <?php if( get_row_layout() == 'basic_group' ): ?>
                
                <div class="personnel-group basic-group layout-<?php echo get_sub_field('layout'); ?>">
                    <div class="headline personnel-header blue underline">
                        <h4><?php echo get_sub_field('group_label'); ?></h4>
                    </div>
                    
                    <div class="grid four-col">
                        <?php if(have_rows('people')): while(have_rows('people')): the_row(); ?>

                            <div class="grid-item person">
                                <?php if(get_sub_field('photo')): ?>
                                    <div class="photo">
                                        <img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                                    </div>
                                <?php endif; ?>

                                <div class="info">
                                    <div class="name headline red">
                                        <h5><?php echo get_sub_field('name'); ?></h5>
                                    </div>

                                    <?php if(get_sub_field('meta')): ?>
                                        <div class="meta p3">
                                            <p><?php echo get_sub_field('meta'); ?></p>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>

                        <?php endwhile; endif; ?>
                    </div>
                </div>
                
            <?php endif; ?>
                
        <?php endwhile; ?>

    </section>	 

<?php endif; ?>