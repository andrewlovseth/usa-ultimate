<?php

/*
 * Team USA Roster Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'team-usa-roster-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'team-usa-roster usau-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

$event = get_field('event');
$event_value = $event['value'];
$event_display_value = $event['label']

?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

	<?php get_template_part('blocks/team-usa-roster/partials/info'); ?>

	<?php get_template_part('blocks/team-usa-roster/partials/results'); ?>

	<?php get_template_part('blocks/team-usa-roster/partials/personnel'); ?>

</section>

<?php include(locate_template('template-parts/team-usa/event-archive.php', false, false)); ?>