<?php

/*
 * Hero Photo Block Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'hero-photo-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'hero-photo';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

$headline_size = get_field('headline_size');
if( !empty($headline_size)) {
    $className .= ' headline-' . $headline_size;
}

if(!get_field('photo')) {
    $className .= ' gradient';

}

?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

	<div class="photo">
		<div class="content">
			<?php if(get_field('photo')): ?>
				<img src="<?php $image = get_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			<?php endif; ?>

			<div class="info">
				<div class="info-wrapper  usau-block">

					<?php if(get_field('sub_headline')): ?>
						<div class="sub-headline">
							<h2><?php echo get_field('sub_headline'); ?></h2>
						</div>
					<?php endif; ?>
					<div class="headline">
						<h1 class="cover-title"><?php echo get_field('headline'); ?></h1>
					</div>

				</div>
			</div>
		</div>
	</div>

</section>