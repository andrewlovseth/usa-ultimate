<?php

/*
 * Awards Block Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'awards-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'awards usau-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

$spreadsheet_id = get_field('google_sheet_id');
$division = get_field('division');

?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
	<div class="division">
		<div class="section-header align-center headline blue">
			<h3><?php echo $division; ?></h3>
		</div>

		<?php $poty = get_field('poty'); if(have_rows('poty')): while(have_rows('poty')): the_row(); ?>
			<div class="poty">

				<div class="award">
					<div class="photo">
						<img src="<?php echo $poty['photo']['url']; ?>" alt="<?php echo $poty['photo']['alt']; ?>" />
					</div>

					<div class="info">
						<div class="award-name headline blue underline">
							<h5><?php echo $poty['award_name']; ?></h5>
						</div>

						<div class="name headline blue">
							<h4><?php echo $poty['name']; ?></h4>
						</div>

						<div class="school headline red">
							<h5><?php echo $poty['school']; ?></h5>
						</div> 

						<?php if(have_rows('finalists')): ?>
							<div class="finalists">
								<h4>Finalists</h4>

								<?php while(have_rows('finalists')): the_row(); ?>
									
									<div class="person">
										<h5><?php echo get_sub_field('name'); ?></h5>
										<p><?php echo get_sub_field('school'); ?></p>
									</div>

								<?php endwhile; ?>
							</div>	
						<?php endif; ?>
					</div>
				</div>

			</div>

		<?php endwhile; endif; ?>

		<div class="award-group">
			<h3><?php echo get_field('award_group'); ?></h3>
		</div>

		<div class="awards-legend">
			<div class="rpoty">
				<span class="color-block"></span>
				<span class="label">Regional Player of the Year</span>
			</div>
		</div>

		<div class="grid regions">
			<?php if(have_rows('regions')): while(have_rows('regions')) : the_row(); ?>

				<?php if( get_row_layout() == 'region' ): ?>

					<?php 
						$region = sanitize_title_with_dashes(get_sub_field('region_name'));
						$division_slug = sanitize_title_with_dashes($division);

					?>

					<div class="grid-item region">
						<div class="region-name">
							<h4><?php echo get_sub_field('region_name'); ?></h4>
						</div>

						<div class="award-winners">

							<?php
								$sheet = get_sub_field('region_name');

								if(get_transient('USAU_awards-' . $region . '-' . $division_slug)) {
									$entries = get_transient('USAU_awards-' . $region . '-' . $division_slug);
								} else {
									$entries = usau_get_sheet_data('USAU_awards-' . $region . '-' . $division_slug, $spreadsheet_id, DAY_IN_SECONDS, $sheet);
								}


								foreach ($entries as $entry): 
									$name = $entry[0];
									$team = $entry[1];
									$poty = $entry[2];
							?>

								<div class="person<?php if($poty == TRUE): ?> poty<?php endif; ?>">
									<h5><?php echo $name; ?></h5>
									<p><?php echo $team; ?></p>
								</div>

							<?php endforeach; ?>

						</div>

						<?php if(have_rows('freshman_of_the_year')): ?>

							<div class="foty">
								<h4>Freshman of the Year</h4>

								<?php while(have_rows('freshman_of_the_year')): the_row(); ?>
									
									<div class="person">
										<h5><?php echo get_sub_field('name'); ?></h5>
										<p><?php echo get_sub_field('school'); ?></p>
									</div>
								
								<?php endwhile; ?>
							</div>

						<?php endif; ?>


						<?php if(have_rows('coach_of_the_year')): ?>

							<div class="coty">
								<h4>Coach of the Year</h4>

								<?php while(have_rows('coach_of_the_year')): the_row(); ?>

									<div class="person">
										<h5><?php echo get_sub_field('name'); ?></h5>
										<p><?php echo get_sub_field('school'); ?></p>
									</div>
								
								<?php endwhile; ?>
							</div>

						<?php endif; ?>

					</div>

				<?php endif; ?>

			<?php endwhile; endif; ?>
		</div>

		<?php if(get_field('additional_copy')): ?>
			<div class="additional-copy copy p3">
				<?php echo get_field('additional_copy'); ?>
			</div>
		<?php endif; ?>
	</div>
</section>