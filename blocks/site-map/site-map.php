<?php

/*
 * Site Map Block
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'site-map-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'site-map usau-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

    <?php if(have_rows('site_map_sections')): while(have_rows('site_map_sections')) : the_row(); ?>

        <?php if( get_row_layout() == 'section' ): ?>

            <div class="site-map-section">

                <?php for ($i = 1; $i <= 3; $i++): ?>

                    <?php if(have_rows('column_' . $i)): ?>
                        <div class="col col-<?php echo $i; ?>">

                            <?php while(have_rows('column_'  . $i)): the_row(); ?>

                                <?php 
                                    $link = get_sub_field('link');
                                    if( $link ): 
                                    $link_url = $link['url'];
                                    $link_title = $link['title'];
                                    $link_target = $link['target'] ? $link['target'] : '_self';
                                ?>

                                    <div class="link<?php if(get_sub_field('header')): ?> header<?php endif; ?>">
                                        <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
                                            <?php echo esc_html($link_title); ?>
                                        </a>
                                    </div>

                                <?php endif; ?>
                                    
                            <?php endwhile; ?>

                        </div>
                    <?php endif; ?>

                <?php endfor; ?>

            </div>

        <?php endif; ?>

    <?php endwhile; endif; ?>

</section>