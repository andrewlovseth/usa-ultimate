<div class="options club">
    <div class="cta">
        <a href="#" class="btn blue rounded small all active" data-division="*">All</a>
    </div>

    <div class="cta">
        <a href="#" class="btn blue rounded small" data-division="mixed">Mixed</a>
    </div>

    <div class="cta">
        <a href="#" class="btn blue rounded small" data-division="women">Women</a>
    </div>

    <div class="cta">
        <a href="#" class="btn blue rounded small" data-division="men">Men</a>
    </div>	
</div>