<?php

$parent = get_post_parent();
$spreadsheet_id = get_field('sheet_id');

$transient_name = 'USAU_season-schedule-';
if ($parent && $parent->post_name) {
    $transient_name .= $parent->post_name;
} else {
    $transient_name .= $post->ID;
}

if(get_transient($transient_name)) {
    $entries = get_transient($transient_name);
} else {
    $entries = usau_get_sheet_data($transient_name, $spreadsheet_id, 600);
}

$today_date = date('Ymd');
$weeks = get_field('weeks');

if($weeks): ?>


<?php

$order = array();
foreach( $weeks as $i => $row ) {                
    $order[ $i ] = $row['end_date'];
}

$end_dates = array();
foreach( $weeks as $week ) {   
    array_push($end_dates, $week['end_date']);             
}

if (min($end_dates) < $today_date): ?>

    <section class="results-weeks" id="results">
        <div class="headline section-header blue underline uppercase">
            <h3>Results</h3>
        </div>

        <?php
            array_multisort( $order, SORT_DESC, $weeks );
            foreach( $weeks as $i => $row ):
                $week = $row['number'];;
                $start_date = $row['start_date'];
                $end_date = $row['end_date'];
                $display_dates = $row['display_dates'];
        ?>

            <?php if($end_date <= $today_date): ?>

                <?php include( locate_template( 'blocks/season-schedule/partials/week.php', false, false ) ); ?>
            
            <?php endif; ?>

        <?php endforeach; ?>

    </section>

<?php endif; ?>

<?php endif; ?>