<div class="week week-<?php echo $week; ?>" data-start-date="<?php echo $start_date; ?>"  data-end-date="<?php echo $end_date; ?>">
	<div class="week-header">
		<?php if(!is_numeric($week)): ?>
			<h3><?php echo $week; ?></h3>
		<?php else: ?>
			<h3>Week <?php echo $week; ?></h3>
		<?php endif; ?>
		
		<h4><?php echo $display_dates; ?></h4>
		<span class="caret"></span>
	</div>

	<?php
		$tournaments = array_filter($entries, function ($entry) use ($week) {
			if (!is_array($entry) || !isset($entry[0])) {
				return false;
			}
			return trim($entry[0]) === trim($week);
		});

		if(!empty($tournaments)): ?>

			<div class="week-schedule">	
				<table class="schedule-table">
					<thead>
						<tr>
							<?php if(have_rows('columns')): while(have_rows('columns')): the_row(); ?>
								<th class="<?php echo get_sub_field('slug'); ?>"><?php echo get_sub_field('label'); ?></th>
							<?php endwhile; endif; ?>
						</tr>
					</thead>

					<tbody>
						<?php foreach ($tournaments as $tournament): ?>

							<?php 
								$tournament_class_name = 'tournament';
								$division_slug = sanitize_title_with_dashes($tournament[2]);
								$level_slug = sanitize_title_with_dashes($tournament[3]);

								if($division_slug !== '') {
									$tournament_class_name .= ' ' . $division_slug;
								}

								if($level_slug !== '') {
									$tournament_class_name .= ' ' . $level_slug;
								}

							?>
							
							<tr class="<?php echo esc_attr($tournament_class_name); ?>">
								<?php $i = 1; if(have_rows('columns')): while(have_rows('columns')): the_row();
									$slug = get_sub_field('slug');
									$datum = isset($tournament[$i]) ? $tournament[$i] : '';
								?>
								
									<?php if($slug == "link"): ?>

										<td class="<?php echo $slug; ?>">
											<?php if(!empty($datum)): ?>
												<a href="<?php echo esc_url($datum); ?>" target="_blank">
													<?php if(get_field('link_label')): ?>
														<?php echo get_field('link_label'); ?>
													<?php else: ?>
														Link
													<?php endif; ?>
												</a>
											<?php endif; ?>
										</td>


									<?php elseif($slug == "results"): ?>

										<td class="<?php echo $slug; ?>">
											<?php if(!empty($datum)): ?>
												<a href="<?php echo esc_url($datum); ?>" target="_blank">Full Results</a>
											<?php endif; ?>
										</td>

									<?php else: ?>
									
										<td class="<?php echo $slug; ?>">
											<?php if(!empty($datum)): ?>
												<span class="label"><?php echo get_sub_field('label'); ?>:</span><?php echo esc_html($datum); ?>
											<?php endif; ?>
										</td>

									<?php endif; ?>
								
								<?php $i++; endwhile; endif; ?>
							</tr>

						<?php endforeach; ?>
					</tbody>
				</table>
			</div>

	<?php else: ?>

		<div class="copy p2 error-message">
			<p>No events scheduled.</p>
		</div>

	<?php endif; ?>
</div>