<div class="options college" data-filters="">
    <div class="cta">
        <a href="#" class="btn blue rounded small all active" data-division="*">All</a>
    </div>

    <div class="cta">
        <a href="#" class="btn blue rounded small" data-division="d-i-women">D-I Women</a>
    </div>

    <div class="cta">
        <a href="#" class="btn blue rounded small" data-division="d-i-men">D-I Men</a>
    </div>

    <div class="cta">
        <a href="#" class="btn blue rounded small" data-division="d-iii-women">D-III Women</a>
    </div>	

    <div class="cta">
        <a href="#" class="btn blue rounded small" data-division="d-iii-men">D-III Men</a>
    </div>

    <div class="cta">
        <a href="#" class="btn blue rounded small" data-division="developmental-women">Developmental Women</a>
    </div>

    <div class="cta">
        <a href="#" class="btn blue rounded small" data-division="developmental-men"> Developmental Men</a>
    </div>
</div>