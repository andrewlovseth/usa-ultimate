<div class="filters">
	<h3>Filters</h3>

	<div class="options" data-filters="">
		<div class="cta">
			<a href="#" class="btn blue rounded small all active" data-division="*">All</a>
		</div>

		<?php if(have_rows('filters')): while(have_rows('filters')): the_row(); ?>

			<?php if(get_sub_field('type') == 'division'): ?>
				<div class="cta">
					<a href="#" class="btn blue rounded small division" data-division="<?php echo get_sub_field('slug'); ?>"><?php echo get_sub_field('label'); ?></a>
				</div>
			<?php elseif(get_sub_field('type') == 'level'): ?>
				<div class="cta">
					<a href="#" class="btn blue rounded small level" data-level="<?php echo get_sub_field('slug'); ?>"><?php echo get_sub_field('label'); ?></a>
				</div>
			<?php endif; ?>

		<?php endwhile; endif; ?>

	</div>
</div>
