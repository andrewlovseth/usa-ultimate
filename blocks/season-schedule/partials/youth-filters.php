<div class="options club">
    <div class="cta">
        <a href="#" class="btn blue rounded small all active" data-division="*">All</a>
    </div>

    <div class="cta">
        <a href="#" class="btn blue rounded small" data-division="girls">Girls</a>
    </div>

    <div class="cta">
        <a href="#" class="btn blue rounded small" data-division="boys">Boys</a>
    </div>

    <div class="cta">
        <a href="#" class="btn blue rounded small" data-division="u15">U15</a>
    </div>	

    <div class="cta">
        <a href="#" class="btn blue rounded small" data-division="u17">U17</a>
    </div>	

    <div class="cta">
        <a href="#" class="btn blue rounded small" data-division="u20">U20</a>
    </div>	
</div>