<?php

/*
 * Google Sheet Schedule Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'google-sheet-schedule-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'season-schedule usau-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

	<?php get_template_part('blocks/season-schedule/partials/filters'); ?>

	<?php get_template_part('blocks/season-schedule/partials/schedule'); ?>
	
	<?php get_template_part('blocks/season-schedule/partials/results'); ?>

</section>