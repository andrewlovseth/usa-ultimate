<?php
/**
 * Search results are contained within a div.searchwp-live-search-results
 * which you can style accordingly as you would any other element on your site
 *
 * Some base styles are output in wp_footer that do nothing but position the
 * results container and apply a default transition, you can disable that by
 * adding the following to your theme's functions.php:
 *
 * add_filter( 'searchwp_live_search_base_styles', '__return_false' );
 *
 * There is a separate stylesheet that is also enqueued that applies the default
 * results theme (the visual styles) but you can disable that too by adding
 * the following to your theme's functions.php:
 *
 * wp_dequeue_style( 'searchwp-live-search' );
 *
 * You can use ~/searchwp-live-search/assets/styles/style.css as a guide to customize
 */
?>

<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<?php $post_type = get_post_type_object( get_post_type() ); ?>

		<?php
			if(get_post_type() == 'post') {
				$post_type_label = 'News';
			} elseif(get_post_type() == 'page') {
				$post_type_label = 'Page'; 
			} elseif(get_post_type() == 'videos') {
				$post_type_label = 'Video'; 
			} elseif(get_post_type() == 'resources') {
				$post_type_label = 'Resource'; 
			} elseif(get_post_type() == 'world_games_players') {
				$post_type_label = 'Player'; 
			} elseif(get_post_type() == 'club_teams' ||  get_post_type() == 'college_teams') {
				$post_type_label = 'Team'; 
			} elseif(get_post_type() == 'local') {
				$post_type_label = 'Location'; 
			} else {
				$post_type_label = 'Other'; 
			}
		?>

		<div class="searchwp-live-search-result" role="option" id="" aria-selected="false">
			<p>
				<a href="<?php echo esc_url( get_permalink() ); ?>"><span class="post-type"><?php echo $post_type_label; ?></span><?php the_title(); ?></a>
			</p>
		</div>
	<?php endwhile; ?>
<?php else : ?>
	<p class="searchwp-live-search-no-results" role="option">
		<em><?php esc_html_e( 'No results found.', 'swplas' ); ?></em>
	</p>
<?php endif; ?>
