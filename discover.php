<?php

/*
	Template Name: Discover

*/

get_header(); ?>

	<?php get_template_part('template-parts/discover/hero'); ?>

	<?php get_template_part('template-parts/discover/this-is-ultimate'); ?>

	<?php get_template_part('template-parts/discover/how-to-play'); ?>

	<?php get_template_part('template-parts/discover/great-plays'); ?>

	<?php get_template_part('template-parts/discover/values'); ?>

	<?php get_template_part('template-parts/discover/glossary'); ?>

	<?php get_template_part('template-parts/discover/spirit-of-the-game'); ?>

	<?php get_template_part('template-parts/discover/call-to-action'); ?>

	<?php // get_template_part('template-parts/discover/why-we-play'); ?>

	<?php get_template_part('template-parts/discover/history'); ?>

	<?php get_template_part('template-parts/discover/where-we-play'); ?>


<?php get_footer(); ?>