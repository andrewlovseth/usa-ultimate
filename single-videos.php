<?php 

get_header(); 

$title = get_the_title();
$thumbnail  = get_field('thumbnail');
$youtube_id = get_field('youtube_id');
$is_vimeo = get_field('vimeo');
$vimeo_id = get_field('vimeo_id');

?>

	<section class="player usau-block">
		<?php if($is_vimeo == true): ?>

			<?php
				if($vimeo_id) {
					$video_endpoint = 'http://vimeo.com/api/v2/video/' . $vimeo_id . '.json';
					$video_response = file_get_contents($video_endpoint);
					$video_data = json_decode($video_response, true);
					$vimeo_thumbnail = $video_data[0]['thumbnail_large'];
				}
			?>

			<div class="video">
				<div class="video-thumbnail">
					<a href="#" class="video-trigger" data-title="<?php echo $title; ?>" data-video-id="<?php echo $vimeo_id; ?>" data-video-type="vimeo">
						<div class="thumbnail">
							<div class="play-btn">
								<div class="triangle"></div>
							</div>

							<div class="content">
								<?php if($thumbnail): ?>
									<img src="<?php echo $thumbnail['url']; ?>" alt="Video Thumbnail: <?php echo $title; ?>" />
								<?php else: ?>
									<img src="<?php echo $vimeo_thumbnail; ?>?mw=960&mh=540" alt="Video Thumbnail: <?php echo $title; ?>" />
								<?php endif; ?>				
							</div>
						</div>
					</a>
				</div>


				<div class="headline blue align-center video-title">
					<h4><a href="#" class="video-trigger" data-title="<?php echo $title; ?>" data-video-id="<?php echo $vimeo_id; ?>" data-video-type="vimeo"><?php echo $title; ?></a></h4>
				</div>
			</div>
			
		<?php else: ?>

			<div class="video">
				<div class="video-thumbnail">
					<a href="#" class="video-trigger" data-title="<?php echo $title; ?>" data-video-id="<?php echo $youtube_id; ?>" data-video-type="youtube">
						<div class="thumbnail">
							<div class="play-btn">
								<div class="triangle"></div>
							</div>

							<div class="content">
								<?php if($thumbnail): ?>
									<img src="<?php echo $thumbnail['url']; ?>" alt="Video Thumbnail: <?php echo $title; ?>" />
								<?php else: ?>
									<img src="https://i3.ytimg.com/vi/<?php echo $youtube_id; ?>/hqdefault.jpg" alt="Video Thumbnail: <?php echo $title; ?>" />
								<?php endif; ?>				
							</div>
						</div>
					</a>
				</div>

				<div class="headline blue align-center video-title">
					<h4><a href="#" class="video-trigger" data-title="<?php echo $title; ?>" data-video-id="<?php echo $youtube_id; ?>" data-video-type="youtube"><?php echo $title; ?></a></h4>
				</div>
			</div>

		<?php endif ; ?>

	</section>

	<?php get_template_part('template-parts/watch/library-cta'); ?>

<?php get_footer(); ?>