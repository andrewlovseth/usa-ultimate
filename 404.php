<?php get_header(); ?>


	<section class="page-not-found usau-block">
		<div class="error-wrapper">
			<div class="headline underline red">
				<h3><?php echo get_field('404_headline', 'options'); ?></h3>
			</div>

			<div class="copy p3">
				<?php echo get_field('404_copy', 'options'); ?>
			</div>	

			<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
				<label>
					<input type="search" class="search-field"
						data-swplive="true"
						placeholder="<?php echo esc_attr_x( '(e.g. "rankings" or "schedule")', 'placeholder' ) ?>"
						value="<?php echo get_search_query() ?>" name="s"
						title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
				</label>
				<input type="submit" class="search-submit"
					value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
			</form>
			
		</div>
	</section>


<?php get_footer(); ?>