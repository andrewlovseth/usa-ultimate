<?php get_header(); ?>

	<?php get_template_part('template-parts/club-teams/hero'); ?>

	<?php get_template_part('template-parts/club-teams/hub-navigation'); ?>

	<?php get_template_part('template-parts/club-teams/page-header'); ?>

	<?php get_template_part('template-parts/club-teams/teams'); ?>

<?php get_footer(); ?>