(function ($, window, document, undefined) {
    $(document).ready(function ($) {
        // Team USA Player Carousel
        $('.team-usa-player-carousel .players').slick({
            dots: true,
            arrows: false,
            centerMode: true,
            infiinte: true,
            slidesToShow: 1,
            autoplay: true,
            autoplaySpeed: 9000,
            mobileFirst: true,
            responsive: [
                {
                    breakpoint: 479,
                    settings: {
                        slidesToShow: 2,
                    },
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 4,
                    },
                },
                {
                    breakpoint: 1279,
                    settings: {
                        slidesToShow: 5,
                    },
                },
            ],
        });

        // FAQs
        $('.faq .question').on('click', function () {
            $(this).closest('.faq').toggleClass('active');

            return false;
        });

        // rel="external"
        $('a[rel="external"]').click(function () {
            window.open($(this).attr('href'));
            return false;
        });

        // Nav Trigger
        $('.nav-trigger').click(function () {
            $('body').toggleClass('nav-overlay-open');
            return false;
        });

        // Search Trigger
        $('.search-trigger').click(function () {
            $('body').toggleClass('search-open');
            return false;
        });

        // Search Close
        $('.search-close-btn').click(function () {
            $('body').toggleClass('search-open');
            return false;
        });

        $('.site-footer .mobile-header-link').click(function () {
            $(this).closest('.group').toggleClass('open');
            return false;
        });

        $('.hub-navigation .mobile-header h4').click(function () {
            $(this).closest('.hub-navigation').toggleClass('open');
            return false;
        });

        $('#top').smoothScroll();

        $('.smooth, .table-of-contents a, .guidelines-toc a').smoothScroll({ offset: -132 });

        $('.wp-block-embed__wrapper').fitVids();

        // Active Week
        $('.week .week-header').on('click', function () {
            $(this).parent('.week').toggleClass('hide');

            return false;
        });

        // Filter Season Schedule
        let combos_data = [];
        let filter_division_data = [];
        let filter_level_data = [];
        let filter_selector = '';

        $('.season-schedule .filters a').on('click', function () {
            filter_selector = '';
            combos_data = [];
            let division = $(this).data('division');
            let level = $(this).data('level');

            // Get Selected Division Filter Values
            if ($(this).hasClass('division')) {
                if ($(this).hasClass('active')) {
                    let division_index = filter_division_data.indexOf(division);
                    if (division_index > -1) {
                        filter_division_data.splice(division_index, 1);
                    }
                } else {
                    filter_division_data.push(division);
                }
            } else if ($(this).hasClass('level')) {
                if ($(this).hasClass('active')) {
                    var level_index = filter_level_data.indexOf(level);
                    if (level_index > -1) {
                        filter_level_data.splice(level_index, 1);
                    }
                } else {
                    filter_level_data.push(level);
                }
            }

            for (var i = 0; i < filter_division_data.length; i++) {
                for (var j = 0; j < filter_level_data.length; j++) {
                    combos_data.push('.season-schedule .' + filter_division_data[i] + '.' + filter_level_data[j]);
                }
            }

            if (combos_data.length !== 0) {
                filter_selector = combos_data.join();
            } else if (filter_division_data.length !== 0) {
                for (var i = 0; i < filter_division_data.length; i++) {
                    if (i == 0) {
                        filter_selector += '.season-schedule .' + filter_division_data[i];
                    } else {
                        filter_selector += ', .season-schedule .' + filter_division_data[i];
                    }
                }
            } else if (filter_level_data.length !== 0) {
                for (var j = 0; j < filter_level_data.length; j++) {
                    if (j == 0) {
                        filter_selector += '.season-schedule .' + filter_level_data[j];
                    } else {
                        filter_selector += ', .season-schedule .' + filter_level_data[j];
                    }
                }
            }

            // Active Filter Nav; Show/Hide Tournaments
            if ($(this).hasClass('all') || (filter_division_data.length == 0 && filter_level_data.length == 0)) {
                $('.season-schedule .filters a').removeClass('active');
                $('.season-schedule .filters a.all').addClass('active');
                $('.season-schedule .tournament').show();
                combos_data = [];
                filter_selector = '';
            } else {
                $(this).toggleClass('active');
                $('.season-schedule .filters a.all').removeClass('active');
                $('.season-schedule .tournament').hide();
                $(filter_selector).show();
            }

            return false;
        });

        // Slider
        $('.slides').slick({
            dots: true,
            arrows: false,
            infiinte: true,
            centerMode: true,
            slidesToShow: 1,
            variableWidth: true,
            autoplay: true,
            autoplaySpeed: 9000,
        });

        // Upcoming Events Slider
        $('.events.grid').slick({
            dots: true,
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            mobileFirst: true,
            infinite: false,
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                    },
                },
                {
                    breakpoint: 1023,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                    },
                },
            ],
        });

        // Video Overlay Show
        $('.video-trigger').click(function () {
            var videoID = $(this).data('video-id');
            var videoType = $(this).data('video-type');
            var videoTitle = $(this).data('title');

            if (videoType == 'vimeo') {
                $('#video-overlay .video-frame .content').html('<iframe width="1920" height="1080" src="https://player.vimeo.com/video/' + videoID + '?autoplay=1&controls=1&byline=0&playsinline=1&quality=1080p" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>');
            } else {
                $('#video-overlay .video-frame .content').html('<iframe width="1920" height="1080" allowfullscreen frameborder="0" allowTransparency="true" src="https://www.youtube.com/embed/' + videoID + '?autoplay=1&modestbranding=1&rel=0"></iframe>');
            }

            $('#video-overlay .title').html(videoTitle);
            $('.video-frame').fitVids();
            $('#video-overlay').fadeIn(200);
            $('body').addClass('video-overlay-open');

            return false;
        });

        // Video Overlay Hide
        $('.video-close-btn').click(function () {
            $('#video-overlay').fadeOut(200);
            $('body').removeClass('video-overlay-open');
            $('#video-overlay .video-frame .content, #video-overlay .title').empty();

            return false;
        });

        // Profile Overlay Show
        $('.profile-trigger').click(function () {
            var profile = $(this).attr('href');

            $(profile).fadeIn(200);
            $('body').addClass('profile-open');

            return false;
        });

        // Profile Overlay Hide
        $('.profile-close-btn').click(function () {
            $('.profile-overlay').fadeOut(200);
            $('body').removeClass('profile-open');

            return false;
        });

        // Team Overlay Show
        $('.team-trigger').click(function () {
            var team = $(this).data('team');

            $(team).fadeIn(200);
            $('body').addClass('team-overlay-open');

            return false;
        });

        // Team Overlay Hide
        $('.team-close-btn').click(function () {
            $('.team-overlay').fadeOut(200);
            $('body').removeClass('team-overlay-open');

            return false;
        });

        // Discover Players Slider
        $('body.page-discover-ultimate .players').slick({
            dots: true,
            arrows: false,
            centerMode: true,
            infiinte: true,
            slidesToShow: 1,
            autoplay: true,
            autoplaySpeed: 9000,
            mobileFirst: true,
            responsive: [
                {
                    breakpoint: 567,
                    settings: {
                        slidesToShow: 2,
                    },
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 3,
                    },
                },
            ],
        });

        // Discover Locations Slider
        $('body.page-discover-ultimate .locations').slick({
            dots: true,
            arrows: false,
            centerMode: true,
            infiinte: true,
            variableWidth: true,
            variableHeight: true,
            slidesToShow: 1,
            autoplay: true,
            autoplaySpeed: 9000,
        });

        $('body.page-discover-ultimate .term a').on('click', function () {
            if ($(this).closest('.term').hasClass('active')) {
                $(this).closest('.term').toggleClass('active');
            } else {
                $('body.page-discover-ultimate .term').removeClass('active');
                $(this).closest('.term').toggleClass('active');
            }

            return false;
        });

        $('#us-map path, #us-map circle').on('click', function () {
            var state_link = $(this).data('state-link');
            window.location.href = state_link;

            return false;
        });

        $('.filters-clear .clear-btn').on('click', function () {
            almfilters.reset();

            return false;
        });

        // Roster Log Tabs
        $('.roster-logs .tab-links a')
            .on('click', function () {
                $('.roster-logs .tab-links a').removeClass('active');
                $(this).addClass('active');

                $('.roster-logs .tabs .tab').removeClass('active');
                var active_tab = $(this).attr('href');
                $(active_tab).addClass('active');

                return false;
            })
            .filter(':first')
            .click();

        //  Text Toggle
        $('.text-toggle').on('click', function () {
            var description = $(this).siblings('.text');

            $(this).toggleClass('show');
            $(description).toggleClass('show');

            return false;
        });

        // Contacts Table Toggle
        $('table.contact-table thead').on('click', function () {
            $(this).closest('table.contact-table').toggleClass('hide');

            return false;
        });

        // About Nav Mobile Toggle
        $('.about-nav-trigger').on('click', function () {
            $(this).closest('nav.about-nav').toggleClass('show');

            return false;
        });
    });

    // About Nav Active
    var current = location.pathname;

    $('nav.about-nav .links a').each(function () {
        var link_href = $(this).attr('href');

        if (link_href == current) {
            $(this).addClass('active');
        }
    });

    // About Contacts Live Filter
    $(document).on('keyup', '#filter-field', function () {
        var value = $(this).val().toLowerCase();

        $('.contacts-row > td')
            .closest('.contacts-row')
            .hide()
            .filter(function () {
                return $(this).text().toLowerCase().indexOf(value) > -1;
            })
            .show();
    });

    $(document).mouseup(function (e) {
        var mobile_menu = $('.mobile-menu');

        // if the target of the click isn't the container nor a descendant of the container
        if (!mobile_menu.is(e.target) && mobile_menu.has(e.target).length === 0) {
            $('body').removeClass('nav-overlay-open');
        }

        var video_player = $('#video-overlay .info');

        // if the target of the click isn't the container nor a descendant of the container
        if (!video_player.is(e.target) && video_player.has(e.target).length === 0) {
            $('body').removeClass('video-overlay-open');
            $('#video-overlay, .profile-overlay').fadeOut(200);
            $('#video-overlay .video-frame .content').empty();
        }

        $('.searchwp-live-search-results').removeClass('searchwp-live-search-results-showing');
    });

    $(document).keyup(function (e) {
        if (e.keyCode == 27) {
            $('body').removeClass('nav-overlay-open search-open profile-open video-overlay-open team-overlay-open');
            $('#video-overlay, .profile-overlay').fadeOut(200);
            $('#video-overlay .video-frame .content').empty();
            $('.team-overlay').fadeOut(200);

            $('.searchwp-live-search-results').removeClass('searchwp-live-search-results-showing');
        }
    });

    window.almComplete = function (alm) {
        // Video Overlay Show
        $('.video-trigger').click(function () {
            var videoID = $(this).data('video-id');
            var videoType = $(this).data('video-type');
            var videoTitle = $(this).data('title');

            if (videoType == 'vimeo') {
                $('#video-overlay .video-frame .content').html('<iframe width="1920" height="1080" src="https://player.vimeo.com/video/' + videoID + '?autoplay=1&controls=1&byline=0&playsinline=1&quality=1080p" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>');
            } else {
                $('#video-overlay .video-frame .content').html('<iframe width="1920" height="1080" allowfullscreen frameborder="0" allowTransparency="true" src="https://www.youtube.com/embed/' + videoID + '?autoplay=1&modestbranding=1&rel=0"></iframe>');
            }

            $('#video-overlay .title').html(videoTitle);
            $('.video-frame').fitVids();
            $('#video-overlay').fadeIn(200);
            $('body').addClass('video-overlay-open');

            return false;
        });

        // Video Overlay Hide
        $('.video-close-btn').click(function () {
            $('#video-overlay').fadeOut(200);
            $('body').removeClass('video-overlay-open');
            $('#video-overlay .video-frame .content, #video-overlay .title').empty();

            return false;
        });
    };
})(jQuery, window, document);
