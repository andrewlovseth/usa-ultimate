<?php

require_once( plugin_dir_path( __FILE__ ) . '/functions/theme-support.php');

require_once( plugin_dir_path( __FILE__ ) . '/functions/enqueue-styles-scripts.php');

require_once( plugin_dir_path( __FILE__ ) . '/functions/acf.php');

require_once( plugin_dir_path( __FILE__ ) . '/functions/register-blocks.php');

require_once( plugin_dir_path( __FILE__ ) . '/functions/disable-editor.php');

require_once( plugin_dir_path( __FILE__ ) . '/functions/post-types-and-taxonomies.php');

require_once( plugin_dir_path( __FILE__ ) . '/functions/html-cache.php');

require_once( plugin_dir_path( __FILE__ ) . '/functions/get-sheet-data.php');

// require_once( plugin_dir_path( __FILE__ ) . '/functions/clear-sheet-data.php');

