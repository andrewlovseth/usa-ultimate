<?php
	get_header();

	if(is_year()) {

		$label = get_the_date('Y');
		$year = get_the_date('Y');
		$alm_query = 'year="'. $year . '"';

	} elseif(is_month()) {

		$label = get_the_date('F Y');
		$month = get_the_date('n');
		$year = get_the_date('Y');
		$alm_query = 'year="'. $year . '" month="' . $month . '"';

	}

?>

	<?php get_template_part('template-parts/news/subnav'); ?>

	<section class="archived-posts usau-block">

		<div class="section-header headline blue underline align-center">
			<h3>Posts from <?php echo $label; ?></h3>
		</div>
		
		<?php echo do_shortcode('[ajax_load_more id="news-archive" theme_repeater="news.php" post_type="post" ' . $alm_query . ' posts_per_page="10" scroll="false" no_results_text="<aside>Sorry, nothing found.</aside>" button_label="Load More"]'); ?>

	</section>

<?php get_footer(); ?>